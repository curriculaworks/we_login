﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CW.Tools
{
    public static partial class CWTools
    {
        // for dev
        public static bool CWTrue = true;  // note: never change this value; we avoid using const to avoid compiler warnings


        #region Shuffle
        /// <summary>
        /// Shuffle
        /// </summary>
        /// <param name="itemct"></param>
        /// <returns></returns>
        public static int[] Shuffle(int itemct)
        {
            List<int> origItems = new List<int>(itemct);
            int[] shufflev = new int[itemct];

            Random rand = new Random();

            for (int i = 0; i < itemct; i++)
                origItems.Add(i);

            for (int i = 0; i < itemct; i++)
            {
                // pull out origItems[rand] and add to shuffledItems
                int idx = rand.Next(0, origItems.Count);
                shufflev[i] = origItems[idx];
                origItems.RemoveAt(idx);
            }

            return shufflev;
        }

        public static List<object> Shuffle(List<object> items)
        {
            List<object> origItems = new List<object>(items);
            List<object> shuffledItems = new List<object>();

            Random rand = new Random();

            while (origItems.Count > 0)
            {
                // pull out origItems[rand] and add to shuffledItems
                int idx = rand.Next(0, origItems.Count);
                shuffledItems.Add(origItems[idx]);
                origItems.RemoveAt(idx);
            }

            return shuffledItems;
        }

        #endregion Shuffle


        #region Strings: split, tokenize etc

        /// <summary>
        /// Tokenizes a string (record) on a set of field separators (single-char delimiters), trimming surrounding whitespace.
        /// Returns a string array that contains the trimmed substrings in the argument string /record/ that are 
        /// delimited by occurences of the characters in the argument string /delims/.
        /// Example:
        ///   CWTools.CWSplit("hello:world,  today",":,")
        ///   returns {"hello","world","today"}
        /// </summary>
        /// <param name="record">string to tokenize</param>
        /// <param name="delims">string of single-char delimiters</param>
        /// <returns>array of tokens</returns>
        public static string[] CWSplit(string record, string delims)
        {
            return CWSplit(record, delims, true);
        }

        /// <summary>
        /// Tokenizes a string (record) on a set of field separators (single-char delimiters).
        /// Returns a string array that contains the substrings in the argument string /record/ that are 
        /// delimited by occurences of the characters in the argument string /delims/.
        /// Example:
        ///   CWTools.CWSplit("hello:world,  today",":,",true)
        ///   returns {"hello","world","today"}
        /// </summary>
        /// <param name="record">string to tokenize</param>
        /// <param name="delims">string of single-char delimiters</param>
        /// <param name="trimWhitespace">true==remove any leading and trailing whitespace from each token using String.Trim()</param>
        /// <returns>array of tokens</returns>
        public static string[] CWSplit(string record, string delims, bool trimWhitespace)
        {
            string[] tokv = record.Split(delims.ToCharArray());

            if (!trimWhitespace)
                return tokv;

            //
            // create new string[] containing the trimmed tokens
            //
            string[] trimmedtokv = new string[tokv.Length];
            for (int i = 0; i < tokv.Length; i++)
                trimmedtokv[i] = tokv[i].Trim();

            return trimmedtokv;
        }

        #region Tokenize() cw-method
        /// <summary>
        /// Originally created to tokenize one line of a content file (UDF, TDF, unit table).
        /// 
        /// Assumes record is formatted as
        /// [chr recordID][chr delim][str tokens]
        /// 
        /// where 
        ///  - recordID is single-char record-type identifier e.g. 'p' for "pic name"
        ///  - delim is field separator
        ///  - tokens is a delim-sepatared list of tokens, possibly bracketed by any number of ' ' and '\t'
        ///  
        /// Example:
        /// w; hilltop; hill; top
        /// 
        /// 
        /// Delimiter: any "normal"(*) character that does not occur elsewhere in the record can be used;
        ///   Tokenize detects the delim character and uses it to invoke String.Split()
        ///   
        /// Returns:
        ///  string[] of tokens;
        ///  If arg record.Length .lt. 2 returned [] contains one element consisting of arg record: new string[] { record }
        /// 
        /// </summary>
        /// <param name="record">string to be tokenized</param>
        /// <returns></returns>
        public static string[] Tokenize(string record)
        {
            if (record.Length < 2) return new string[] { record };

            //Char delim = record.Substring(1, 1).ToCharArray()[0];
            //string[] tokv = record.Split(new Char[] { delim });

            // tokenize record
            string[] tokv = record.Split(new Char[] { record.Substring(1, 1).ToCharArray()[0] });

            // remove leading/trailing whitespace from each token
            for (int i = 0; i < tokv.Length; i++)
            {
                tokv[i] = tokv[i].Trim(new Char[] { ' ', '\t' });
                //tr.tr("  /", token.Trim(new Char[] { ' ', '\t' }), "/");
            }

            return tokv;
        }
        #endregion Tokenize() cw-method
        #endregion Strings: split, tokenize etc


        #region time, timestamp
        public static string Now
        {
            get { return (DateTime.Now - DateTime.Today).ToString(); }

            private set { }
        }

        //tr.tr("  timestampStart : ", timestampStart.ToString("d") + " " + (timestampStart - timestampStart.Date));
        /// <summary>
        /// convert DateTime object to string formatted with precision of Timespan.ToString()
        /// </summary>
        /// <param name="dtToFormat"></param>
        /// <returns></returns>
        public static string DT(DateTime dtToFormat)
        {
            return DT(dtToFormat, false);
        }
        public static string DT(DateTime dtToFormat, bool includeDate)
        {
            string timestr = (dtToFormat - dtToFormat.Date).ToString();
            if (includeDate)
                return dtToFormat.ToString("d") + " " + timestr;
            else
                return timestr;
        }

        #endregion time, timestamp


    }  // public partial class CWTools: General
}

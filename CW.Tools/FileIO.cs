﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CW.Tools
{
    public static partial class FileIO
    {
        //static bool isVerbose = false;  // true

        static bool cwtrue = true;
        static Random rand = new Random();

        private static string filepathConfig = "passwordmakerconf.txt";
        private static void getConfig(ref string filepathIn, ref string filepathOut)
        {
            filepathIn = "";
            filepathOut = "";

            StreamReader sr = OpenRead(filepathConfig);
            if (sr != null)
            {
                string dataLine = "";
                while (true)
                {
                    dataLine = sr.ReadLine();
                    if (dataLine == null) break;

                    if (dataLine.StartsWith("i") && filepathIn == "")
                        filepathIn = dataLine.Substring(2).Trim();
                    else if (dataLine.StartsWith("o") && filepathOut == "")
                        filepathOut = dataLine.Substring(2).Trim();

                    // stop reading if we've found what we're looking for
                    if (filepathIn != "" && filepathOut != "")
                        break;
                }
                sr.Close();
            }

            if (filepathIn == "")
            {
                Console.Write("input file >");
                filepathIn = Console.ReadLine();
            }

            if (filepathOut == "")
            {
                Console.Write("output file >");
                filepathOut = Console.ReadLine();
            }
        }


        /// <summary>
        /// open file for reading
        /// </summary>
        /// <param name="filepathIn"></param>
        /// <returns>StreamReader instance or null if openerr</returns>
        public static StreamReader OpenRead(string filepathIn)
        {
            StreamReader sr = null;
            try
            {
                //Pass the file path and file name to the StreamReader constructor
                sr = new StreamReader(filepathIn);
            }
            catch (Exception e)
            {
                //Console.WriteLine("Exception: " + e.Message);
                sr = null;
            }
            finally
            {
                //Console.WriteLine("Executing finally block.");
            }

            return sr;
        }

        /// <summary>
        /// open file for writing. If file exists, it will be overwritten.
        /// </summary>
        /// <param name="filepathOut"></param>
        /// <returns>StreamWriter instance or null if openerr</returns>
        public static StreamWriter OpenWrite(string filepathOut)
        {
            #region [commented] "exists" check leftover from console app
            //if (File.Exists(filepathOut))
            //{
            //    Console.Write("Overwrite " + filepathOut + " ? (y/n) >");
            //    string doOverwrite = Console.ReadLine();
            //    if (doOverwrite.Length == 0 || "yes".IndexOf(doOverwrite.ToLower()) != 0)
            //    {
            //        Console.WriteLine("canceled...");
            //        return null;
            //    }

            //    Console.Write("Overwriting output file...");
            //}
            //else
            //    Console.Write("Creating output file...");
            #endregion


            StreamWriter sw = null;

            try
            {
                sw = new StreamWriter(filepathOut);
            }
            catch (Exception e)
            {
                //Console.WriteLine("Exception: " + e.Message);
                sw = null;
            }
            finally
            {
                //Console.WriteLine("Executing finally block.");
            }

            return sw;
        }

        /// <summary>
        /// Read txt file into list of records;
        ///  - filter out comments and empty lines;
        ///  - trim leading,trailing whitespace;
        /// </summary>
        /// <param name="filepathIn"></param>
        /// <param name="filepathOut"></param>
        /// <returns></returns>
        public static List<string> LoadDataLines(string filepathIn)
        {
            List<string> datalines = new List<string>();

            StreamReader sr = OpenRead(filepathIn);
            if (sr != null)
            {
                string dataLine = "";
                while (true)
                {
                    dataLine = sr.ReadLine();
                    if (dataLine == null) break;

                    // preprocess: trim leading,trailing whitespace; omit meta-lines
                    dataLine.Trim();
                    if (
                        dataLine.Length < 2 ||
                        dataLine.StartsWith("#") ||
                        dataLine.StartsWith("//")
                        ) continue;

                    datalines.Add(dataLine);
                }
                sr.Close();
            }

            return datalines;
        }  // LoadDataLines()


    }  // class FileIO
}  // ns CW.Tools

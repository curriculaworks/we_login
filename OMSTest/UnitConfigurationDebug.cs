﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CW.Tools;
using OMS.Tools;

namespace OMSTest
{

    /// <summary>
    /// datastructures for json-encoding (for transport via web srvc)
    /// </summary>
    public class Json
    {

        #region datastructures for json-encoding for n-back

        #region ALERT regarding instance names:
        // instance names in the below structures correspond to the public data-transport spec and will break 
        // frontend/backend comms if changed without notice
        #endregion ALERT


        public class NBackBasePaths
        {
            public string a = "kidsoms.com/assets/cnt/s/";
            public string v = "kidsoms.com/assets/cnt/p/";
        }

        public class NBackStimulus
        {
            public bool IsTrg = false;
            public string AssetRelPath = "";
        }

        public class NBackStimulusPair
        {
            public NBackStimulus a = new NBackStimulus();
            public NBackStimulus v = new NBackStimulus();
        }

        public class NBackUnitConfig
        {
            public string unit_id = "L001G002N011";
            public int difficulty = 1;  // difficulty level (== game level)
            public int recall = 1;      // AV-Recall value
            public int stimulus_duration = 1800;  // in milliseconds; interval between onsets

            public NBackBasePaths base_paths = new NBackBasePaths();

            public NBackStimulusPair[] stimuli = new NBackStimulusPair[1];
        }

        public class NBackUnitConfigWrapper
        {
            public NBackUnitConfig unit = new NBackUnitConfig();
        }

        #endregion datastructures for json-encoding for n-back

        public NBackUnitConfig nbackConfig = new NBackUnitConfig();
    }


    /// <summary>
    /// Descriptor for dynamically loaded external assets, in particular filesystem/web location.
    /// </summary>
    internal class AssetDescriptor
    {
        public string RelPath { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
    }

    /// <summary>
    /// Descriptor for n-back stimulus, incl external asset and isTarget
    /// </summary>
    internal class Stimulus
    {
        public AssetDescriptor AssetDescr = new AssetDescriptor();

        public bool IsTrg = false;
    }



    class UnitConfigurationDebug
    {
        //internal static string pathOMSConfig = @"D:\cwconfig\omsconfig.txt";
        ////internal static string pathOMSConfig = @"C:\cwconfig\omsconfig.txt";

        static StreamReader openRead(string filepathIn)
        {
            StreamReader sr = null;
            try
            {
                //Pass the file path and file name to the StreamReader constructor
                sr = new StreamReader(filepathIn);
            }
            catch (Exception e)
            {
                //Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                //Console.WriteLine("Executing finally block.");
            }

            return sr;
        }

        /// <summary>
        /// Determine path to UDF, read in as filtered list of records.
        /// </summary>
        /// <param name="gameLevel"></param>
        /// <param name="unitID"></param>
        /// <returns></returns>
        private static List<string> GetDataLinesIn(int gameLevel, string unitID)
        {
            #region [old] UDF identifiers
            //int levelIdx = gameLevel;
            //string devLevel = "";  // ""  sample
            //string levelID = "L" + devLevel + gameLevel.ToString("D02");
            ////levelID = "sample";  // DEV
            ////unitID = "sampleUDF_v01";
            #endregion

            #region get filepath for level,unit

            string basePathUDF = OMSGameConfig.NBack.Navy.BasePathUDF;
            string levelPrefix = OMSGameConfig.NBack.Navy.LevelPrefix;

            string levelID = levelPrefix + gameLevel.ToString("D02");

            string pathUDF = basePathUDF + levelID + "/" + unitID + ".txt";
            //string pathUDF = @"D:\cwassets\cnt\u\sampleUDF_v01.txt";
            //string pathUDF = @"D:\cwassets\cnt\u\L01\L001G001N001.txt";
            #endregion get filepath for uid  (unitID)

            List<string> datalinesIn;
            datalinesIn = new List<string>();

            #region open UDF for read
            StreamReader sr = openRead(pathUDF);
            //if (sr == null) { promptFinish(); return; }  // TODO: error handling
            #endregion

            #region read in file contents as List<string>, preprocessing to remove comment- and empty lines
            {
                string dataLine = "";
                while (true)
                {
                    dataLine = sr.ReadLine();
                    if (dataLine == null) break;

                    // preprocess: trim leading,trailing whitespace; omit meta-lines
                    dataLine.Trim();
                    if (
                        dataLine.Length < 2 ||
                        dataLine.StartsWith("#") ||
                        dataLine.StartsWith("//")
                        ) continue;

                    datalinesIn.Add(dataLine);

                    #region sample n-back unit data (incomplete)
                    //n; 1
                    //#c; streamlen:30

                    //i; p:school/SAMPLE
                    //i; i:apple.jpgSAMPLESAMPLESAMPLE
                    //i; i:stapler.jpgSAMPLESAMPLESAMPLESAMPLE
                    //i; i:pencils.jpg
                    //i; i:pencils.jpg
                    //i; i:crayons.jpg
                    //i; i:tape.jpg
                    #endregion

                }  // while (dataline)

                sr.Close();
            }
            #endregion read in file

            return datalinesIn;
        }  // GetDataLinesIn()

        /// <summary>
        /// store global config data read in from external file
        /// </summary>
        internal static class OMSGameConfig
        {
            public static string PathConfigFile = @"C:\cwconfig\omsconfig.txt";

            public static class NBack
            {
                public static class Navy
                {
                    // path to UDF's; note: UDF's read locally by server-side code
                    public static string BasePathUDF = "";  // @"D:\cwassets\cnt\u\";
                    public static string LevelPrefix = "L";  // prefix for UDF subfolders; ex: ~/cwassets/u/Navy/ + L01/ + L001G001N368.txt

                    // path to assets; note: assets http-downloaded by client-side code and require full URL
                    public static string BasePathAud = "http://kidsoms.com/assets/navy/snds/";
                    public static string BasePathVis = "http://kidsoms.com/assets/navy/imgs/";
                }
            }
        }





        internal class UnitConfig
        {
            public string UnitID = "";  // ex. "L001G002N011"
            public int GameLevel = 1;
            public int AVRecallValue = 1;
            public int StimulusDuration = 1800;  // onset interval in milliseconds; optional UDF param; valid if >0 else revert to default

            // TODO: read in base paths from external config file
            public string BasePathAud = "cnt/s/kids/";
            public string BasePathVis = "cnt/p/kids/";

            public int StreamLen = 0;
            public List<Stimulus> Pics = new List<Stimulus>();
            public List<Stimulus> Snds = new List<Stimulus>();
        }


        /// <summary>
        /// Loads external config data for unitID and returns data as instance of UnitConfig
        /// </summary>
        /// <param name="gameLevel"></param>
        /// <param name="unitID"></param>
        /// <returns></returns>
        public static UnitConfig getUnitConfig(int gameLevel, string unitID)
        {
            UnitConfig unitConfig = new UnitConfig();

            #region input global config data from external config file: asset base paths etc


            //string basePathUDF = "";  // @"D:\cwassets\cnt\u\";
            //string levelPrefix = "L";

            // TODO: input snd-file extension from ext. config file 
            string sndFileExtension = ".mp3";

            {
                // note: usually PathConfigFile == @"C:\cwconfig\omsconfig.txt";
                StreamReader srr = openRead(OMSGameConfig.PathConfigFile);  // TODO: errhandling
                string dataLine = "";
                while (true)
                {
                    dataLine = srr.ReadLine();
                    if (dataLine == null) break;

                    // preprocess: trim leading,trailing whitespace; omit meta-lines
                    dataLine.Trim();
                    if (
                        dataLine.Length < 2 ||
                        dataLine.StartsWith("#") ||
                        dataLine.StartsWith("//")
                        ) continue;


                    string[] tokv = CWTools.CWSplit(record: dataLine, delims: "=", trimWhitespace: true);
                    #region store token values in OMSConfig
                    for (int i = 0; i < tokv.Length; i++)
                    {
                        if (false) { }

                        else if
                        (
                                tokv[i] == "basePathUDF"
                            || tokv[i] == "basePathNBackUDF"
                            || tokv[i] == "basePathNBackNavyUDF"
                        )
                            OMSGameConfig.NBack.Navy.BasePathUDF = tokv[1];  // TODO: make this more robust
                        else if
                        (
                                tokv[i] == "UDFLevelPrefix"
                            || tokv[i] == "NBackUDFLevelPrefix"
                            || tokv[i] == "NBackNavyUDFLevelPrefix"
                        )
                            OMSGameConfig.NBack.Navy.LevelPrefix = tokv[1];  // TODO: make this more robust

                        else if
                        (
                                tokv[i] == "basePathAud"
                            || tokv[i] == "basePathNBackAud"
                            || tokv[i] == "basePathNBackNavyAud"
                        )
                            OMSGameConfig.NBack.Navy.BasePathAud = tokv[1];  // TODO: make this more robust
                        else if
                        (
                                tokv[i] == "basePathVis"
                            || tokv[i] == "basePathNBackVis"
                            || tokv[i] == "basePathNBackNavyVis"
                        )
                            OMSGameConfig.NBack.Navy.BasePathVis = tokv[1];  // TODO: make this more robust
                    }  // for i<tokc
                    #endregion store tokens
                }  // while (pathConfig:dataline)

                srr.Close();
            }  // local code block: import omsconfig

            #endregion input global config data

            #region init local datastructures and load udf
            List<string> datalinesIn;
            List<string> datalinesOut;

            datalinesOut = new List<string>();
            datalinesIn = GetDataLinesIn(gameLevel: gameLevel, unitID: unitID);

            datalinesOut.Add(Environment.CurrentDirectory);  // for dev
            datalinesOut.Add("hello july 17");  // for dev
            #endregion init local datastructures

            #region parse and analyze loaded content (stored in datalinesIn)

            // determining streamlen:
            //   config  : from optional explicit record in UDF; has priority if present
            //   counted : fallback if no config: MIN(pics.len, snds.len)
            //   net     : resulting value for returns;
            int streamLenConfig = -1;
            int streamLenCounted = 0;
            int streamLenNet = 0;

            // store dirpath and filename for each asset file
            List<AssetDescriptor> pics = new List<AssetDescriptor>();
            List<AssetDescriptor> snds = new List<AssetDescriptor>();

            int avRecallValue = 1;
            int stimulusOnsetInterval = 0;  // milliseconds; optional UDF param; valid if >0 else revert to default


            #region parse algorithm:
            // 
            // - iterate through datalinesIn, storing pics and snds in separate lists;
            // - determine streamlenNet

            // build output:
            //for (int i = 0; i < streamLenNet; i++)
            //{
            //    // check isNeeded outputLine for updating relpath(s)
            //    // build outputLine for stimulus-pair: "s; "+ snds[i].filename + "; " + pics[i].filename
            //}
            #endregion parse algorithm

            #region parse datalinesIn
            string relPathSnd = "";
            string relPathImg = "";

            foreach (string dataline in datalinesIn)
            {

                string[] tokv = CWTools.Tokenize(dataline);

                string recordKey = tokv[0];

                if (recordKey == "")
                {
                }
                else if (recordKey == "n")
                {
                    avRecallValue = int.Parse(tokv[1]);
                }
                #region config settings
                else if (recordKey == "c")
                {
                    // TODO: clean up tokenizing
                    for (int i = 1; i < tokv.Length; i++)
                    {
                        if (tokv[i] == "")
                        {
                        }
                        else if (tokv[i].ToLower().StartsWith("displayduration:"))
                        {
                            stimulusOnsetInterval = int.Parse(tokv[i].Substring(16));
                            //if (trt) tr.tr(this, ":loadGameDataset() displayduration from UDF: ", displayDuration);
                        }
                        else if (tokv[i].ToLower().StartsWith("streamlen:"))
                        {
                            streamLenConfig = int.Parse(tokv[i].Substring(10));
                        }
                        else if (tokv[i] == "sync")
                        {
                        }
                    }
                }
                #endregion config settings
                #region i; visual item: image
                else if (recordKey == "i")
                {
                    //if (trt) tr.tr("");
                    //if (trt) tr.tr("CWGNBack.loadGameDataset(): dataline: ", dataline);

                    //// visual items (images (img) and skins (dictpath,style)
                    //// iMAGE; lABEL; iMAGEFILE; pATH_OF_FOLDER; fULL_PATH_OF_IMAGE_FILE
                    ////  where :
                    ////    - each 'i' record describes exactly one image file (== one vis. stimulus);
                    ////    - 'p' sets default folder (aka dir) for this and subsequent images;
                    ////    - 'i' is filename+extension; overridden if 'f' present;
                    ////    - 'f' overrides default folder path for this image only, without affecting default dir path;
                    ////    - each 'i' record should contain exactly one 'f' or 'i'; if both are present, whicnever is last takes priority;
                    //datalinesFull.Add("i; l:B2 Stealth Bomber; p:../Images/CoverFlow/; i:OMS_B2StealthBomber_3.jpg; ");
                    //datalinesFull.Add("i; l:London; i:London_2.jpg");

                    //string label = "";
                    //string imgPath = "";
                    string imgFilename = "";
                    string imgPathFull = "";

                    for (int i = 1; i < tokv.Length; i++)
                    {
                        if (tokv[i] == "") continue;
                        // tokenize on ':'
                        string[] ttokv = CWTools.Tokenize(tokv[i]);
                        if (ttokv.Length < 2) continue;  // errchk

                        if (ttokv[0] == "")
                        {
                        }
                        else if (ttokv[0] == "l")
                        {
                            //label = ttokv[1];   // commented to disable labels
                        }
                        else if (ttokv[0] == "i")
                        {
                            imgFilename = ttokv[1];
                        }
                        else if (ttokv[0] == "f")
                        {
                            imgPathFull = ttokv[1];
                        }
                        else if (ttokv[0] == "p")
                        {
                            relPathImg = ttokv[1];
                            if (!(relPathImg == "") && !relPathImg.EndsWith("/"))
                                relPathImg += "/";
                        }
                    }
                    // if dataline contained img ref, add it to img's list
                    if (imgFilename != "")
                    {
                        pics.Add(new AssetDescriptor() { RelPath = relPathImg, FileName = imgFilename });
                        //itemImageDescrs.Add(new ImageDescr(imagePath: imgPath, imageLabel: label));
                        //itemImageIDs.Add(imgPath);  // for preloading image files
                        //ImageDescr imgDscr = new ImageDescr(imagePath: imgPath, imageLabel: label);
                        //if (trt) tr.tr("   imgc=", itemImageIDs.Count, "; imagePath: ", imgPath, "; imageLabel: ", label);
                    }
                }
                #endregion visual items
                #region s; audio item: sound
                else if (recordKey == "s")
                {
                    ////if (trt) tr.tr("");
                    ////if (trt) tr.tr("CWGNBack.loadGameDataset(): dataline: ", dataline);

                    ////// sOUND; sOUNDFILE; pATH_OF_FOLDER; fULL_PATH_OF_SOUND_FILE_WITHOUT_EXTENSION
                    //////  where 'p' sets default folder 
                    ////datalinesFull.Add("s; p:soundeffects/; s:opAddition");
                    ////datalinesFull.Add("s; s:opSubtraction");
                    ////datalinesFull.Add("s; f:math/voice0/70");

                    string sndFilename = "";
                    string sndPathFull = "";

                    for (int i = 1; i < tokv.Length; i++)
                    {
                        if (tokv[i] == "") continue;
                        // tokenize on ':'
                        string[] ttokv = CWTools.Tokenize(tokv[i]);
                        if (ttokv.Length < 2) continue;  // errchk

                        if (ttokv[0] == "")
                        {
                        }
                        else if (ttokv[0] == "s")
                        {
                            sndFilename = ttokv[1];
                        }
                        else if (ttokv[0] == "f")
                        {
                            sndPathFull = ttokv[1];
                        }
                        else if (ttokv[0] == "p")
                        {
                            relPathSnd = ttokv[1];
                            if (!(relPathSnd == "") && !relPathSnd.EndsWith("/"))
                                relPathSnd += "/";
                        }
                    }
                    // if dataline contained snd ref, add it to snd's list
                    if (sndFilename != "")
                    {
                        snds.Add(new AssetDescriptor() { RelPath = relPathSnd, FileName = sndFilename, FileExtension = sndFileExtension });
                        //itemImageDescrs.Add(new ImageDescr(imagePath: sndPath, imageLabel: label));
                        //itemImageIDs.Add(sndPath);  // for preloading image files
                        //ImageDescr sndDscr = new ImageDescr(imagePath: sndPath, imageLabel: label);
                        //if (trt) tr.tr("   sndc=", itemImageIDs.Count, "; imagePath: ", sndPath, "; imageLabel: ", label);
                    }
                }  // else if 's'
                #endregion audio item

            }  // foreach (string dataline in datalines)
            #endregion parse datalines

            #region analyze loaded content

            #region prepend buffer to pics,snds to gracefully handle edges
            {
                List<AssetDescriptor> empties = new List<AssetDescriptor>();
                for (int i = 0; i < avRecallValue; i++)
                {
                    empties.Add(new AssetDescriptor() { RelPath = relPathSnd, FileName = "" });
                    //snds.Add(new AssetDescriptor() { RelPath = relPathSnd, FileName = "" });
                    //pics.Add(new AssetDescriptor() { RelPath = relPathImg, FileName = "" });
                }
                snds.InsertRange(0, empties);
                pics.InsertRange(0, empties);
            }
            #endregion



            streamLenCounted = Math.Min(snds.Count, pics.Count);
            if (streamLenConfig > -1)
                streamLenNet = Math.Min(streamLenConfig, streamLenCounted);
            else
                streamLenNet = streamLenCounted;

            #endregion analyze loaded content

            #endregion parse and analyze loaded content

            #region build (unit-specific section of) UnitConfig

            // meta udf-values: unitID, level,avRecall, onsetInterval,  streamlen
            unitConfig.UnitID = unitID;
            unitConfig.GameLevel = gameLevel;
            unitConfig.AVRecallValue = avRecallValue;
            if (stimulusOnsetInterval > 0)
                unitConfig.StimulusDuration = stimulusOnsetInterval;  // amount of time (milliseconds) between consecutive stimulus-pair onsets

            unitConfig.BasePathAud = OMSGameConfig.NBack.Navy.BasePathAud;
            unitConfig.BasePathVis = OMSGameConfig.NBack.Navy.BasePathVis;

            #region build UnitConfig stream data
            for (int i = avRecallValue; i < streamLenNet; i++)  // first elts are dummies to handle edges gracefully
            {

                #region determine matches (targets)
                // TODO: refine target tests to include path
                bool isAudTarget = snds[i].FileName == snds[i - avRecallValue].FileName;
                bool isVidTarget = pics[i].FileName == pics[i - avRecallValue].FileName;

                #endregion determine match code

                unitConfig.Pics.Add(new Stimulus() { AssetDescr = pics[i], IsTrg = isVidTarget });
                unitConfig.Snds.Add(new Stimulus() { AssetDescr = snds[i], IsTrg = isAudTarget });
            }  // for i in stimuli
            #endregion build UnitConfig stream data

            unitConfig.StreamLen = unitConfig.Pics.Count;  // == Snds.Count == streamLenNet - avRecallValue

            #endregion build stream

            return unitConfig;
        }  // getUnitConfig()
    }
}  // OMSTest

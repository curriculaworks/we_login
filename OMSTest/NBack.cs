﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OMSTest
{
    // created to document algorithm for calculating accuracy of n-back user-responses. See also 
    //   - D:\poc\nback-accuracy
    //   ~/Dropbox\OMS\Technical Design\nback_accuracy


    class NBack_fullannotated
    {

//Four
//types of response are obtained from this task: 
        // [1] hits             (signal //is present), 
        // [2] miss             (signal is present, but the participant//incorrectly indicates that there is no signal), 
        // [3] false alarms     //(participant indicates that signal is present when it is not),
        // [4] correct negative (participant correctly indicates that//there is no signal).

//We used the sensitivity index d¢ to
//gather information regarding how a participant is able to
//discriminate targets from nontargets. d¢ is calculated using
//the formula: d¢ = ZHit – ZFA (Macmillan & Creelman,
//1990), where Hit represents the proportion of hits when a
//signal is present (hits/(hits + misses)), also known as the
//hit rate, and FA represents the proportion of false alarms
//when a signal is absent (false alarms/(false alarms + correct
//negative)), the false-alarm rate.
//However, d¢ is not simply Hit minus FA; more specifically,
//it is the difference between the Z transforms of
//these two rates. The Z transformation is done using the
//statistical formula NORMSINV(Hit) – NORMSINV(FA)
//in a Microsoft Excel spreadsheet. Perfect scores were
//adjusted using these formulas: 1 − 1/(2n) for perfect hits,

//and 1/(2n) for zero false alarms, where n was number of
//total hits or false alarms (20 or 80; Macmillan & Creelman,
//1991). Using this formula the highest obtained d¢ was
//calculated to be +4.46 (20 hits, 0 FA), likewise the lowest
//d¢ was −4.46 (0 hits, 80 FA). Reported is number of hits,
//number of misses (false alarms), net score (hits – false
        //alarms), and d¢.


        #region Terminology:
        //
        // --------------------------
        // streamlen: number of stimuli in n-back stream
        //
        // --------------------------
        // target: stimulus that is a match (to the stimulus n stimuli back)
        // 
        // --------------------------
        // User-response types (4):
        //   hit        : [correct] :  shoot at target     AKA respond 'no'  to match
        //   miss       : [wrong]   : !shoot at target     AKA respond 'yes' to match
        //   false alarm: [wrong]   :  shoot at non-target AKA respond 'yes' to non-match
        //   correct neg: [correct] : !shoot at non-target AKA respond 'no'  to non-match 
        //
        //   User-response types (4) chart (same info as above, different presentation):
        //   
        //   name         |isCorrect || isTrg | Rsp |
        //   -------------|----------||-------|-----|
        //   hit          | T        || T     | N   |
        //   miss         | F        || T     | Y   |
        //   false alarm  | F        || F     | Y   |
        //   correct neg  | T        || F     | N   |
        //
        #endregion Terminology:

        /// <summary>
        /// Calculate accuracy of user responses to one n-back stream (either aud or vis).
        /// Call this method once each for each constituent stream of the dual stream.
        /// </summary>
        /// <returns></returns>
        private double calculateAccuracy()
        {
            int streamlen;  // get value from unit-data
            int targetc;    // number of targets; game should count targets and store value
            int nontargetc = streamlen - targetc;  // number of non-targets

            int hitc;        // game should count hits ('YES' responses which are correct AKA 'YES's to targets) and store value
            int falseAlarmc; // game should count falseAlarmc ('YES' responses which are incorrect AKA 'YES's to non-targets) and store value

            double hitRate = hitc / targetc;                   // ratio of corrects on targets
            double falseAlarmRate = falseAlarmc / nontargetc;  // ratio of wrongs on non-targets

            // adjust for zero-values (to avoid exceeding edges of stats model)
            if (hitc == 0) hitRate = 0.01;
            if (falseAlarmc == 0) falseAlarmRate = 0.01;

            double dPrimeShift = 4.66;
            double dPrimeSpan = 9.32;  // == dPrimeMax * 2

            double dPrime = NormsInv(hitRate) - NormsInv(falseAlarmRate);  // value is between -4.66 and +4.66
            double accuracy = (dPrime + dPrimeShift) / dPrimeSpan;

            return accuracy;
        }

        // see ./NormsInv.js
        public double NormsInv(double p)
        {
            return 0.0;
        }
    
    }  // class NBack_fullannotated

    class NBack
    {

        #region Terminology:
        //
        // --------------------------
        // streamlen: number of stimuli in n-back stream
        //
        // --------------------------
        // target: stimulus that is a match (to the stimulus n stimuli back)
        // 
        // --------------------------
        // User-response types (4):
        //   hit        : [correct] :  shoot at target     AKA respond 'no'  to match
        //   miss       : [wrong]   : !shoot at target     AKA respond 'yes' to match
        //   false alarm: [wrong]   :  shoot at non-target AKA respond 'yes' to non-match
        //   correct neg: [correct] : !shoot at non-target AKA respond 'no'  to non-match 
        //
        //   User-response types (4) chart (same info as above, different presentation):
        //   
        //   name         |isCorrect || isTrg | Rsp |
        //   -------------|----------||-------|-----|
        //   hit          | T        || T     | N   |
        //   miss         | F        || T     | Y   |
        //   false alarm  | F        || F     | Y   |
        //   correct neg  | T        || F     | N   |
        //
        #endregion Terminology:

        private double getAccuracyOverall()
        {

            // note: the meanings of the args are explained below in getAccuracyForSingleStream() in the comments
            double accuracy_aud = getAccuracyForSingleStream(streamlen: streamlen, targetc: audTargetc, hitc: audHitc, falseAlarmc: audFalseAlarmc);
            double accuracy_vis = getAccuracyForSingleStream(streamlen: streamlen, targetc: visTargetc, hitc: visHitc, falseAlarmc: visFalseAlarmc);

            return (accuracy_aud + accuracy_aud) / 2.0;
        }

        /// <summary>
        /// Calculate accuracy of user responses to one n-back stream (either aud or vis).
        /// Call this method once each for each constituent stream of the dual stream.
        /// </summary>
        /// <returns></returns>
        private double getAccuracyForSingleStream(int streamlen, int targetc, int hitc, int falseAlarmc)
        {
            //int streamlen;  // get value from unit-data
            //int targetc;    // number of targets; game should count targets and store value
            int nontargetc = streamlen - targetc;  // number of non-targets

            //int hitc;        // game should count hits ('YES' responses which are correct AKA 'YES's to targets) and store value
            //int falseAlarmc; // game should count falseAlarmc ('YES' responses which are incorrect AKA 'YES's to non-targets) and store value

            double hitRate = hitc / targetc;                   // ratio of corrects on targets
            double falseAlarmRate = falseAlarmc / nontargetc;  // ratio of wrongs on non-targets

            // adjust for zero-values (to avoid exceeding edges of stats model)
            if (hitc == 0) hitRate = 0.01;
            if (falseAlarmc == 0) falseAlarmRate = 0.01;

            double dPrimeShift = 4.66;
            double dPrimeSpan = 9.32;  // == dPrimeMax * 2

            double dPrime = NormsInv(hitRate) - NormsInv(falseAlarmRate);  // value is between -4.66 and +4.66
            double accuracy = (dPrime + dPrimeShift) / dPrimeSpan;

            return accuracy;
        }

        // see ./NormsInv.js  (this here is a placeholder)
        public double NormsInv(double p)
        {
            return 0.0;
        }

    }  // class NBack

}

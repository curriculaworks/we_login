﻿<%@ Page Language="C#" AutoEventWireup="true" Title="OMS Login" MasterPageFile="~/CurriculaWorks.Master" CodeBehind="login.aspx.cs" Inherits="OMS.WebApp.login1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="scripts/scripts.js"></script>
    <style type="text/css">
        .style1
        {
            width: 445px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <%--<tr><td class="lb rb">
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="796" height="28" id="FlashID">
  <param name="movie" value="<%= CWHomePageMenu %>" />
  <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
  <!--[if !IE]>-->
    <object type="application/x-shockwave-flash" data="<%= CWHomePageMenu %>" width="796" height="28">
  <!--<![endif]-->
    <param name="quality" value="high" />
    <param name="wmode" value="transparent" />
    <param name="swfversion" value="9.0.45.0" />
    <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
    <param name="expressinstall" value="scripts/expressInstall.swf" />
    <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
    <div>
      <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
      <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>
    </div>
    <!--[if !IE]>-->
    </object>
  <!--<![endif]-->
  </object>
</td></tr>--%>

<tr>
<td >  <%-- some quick-and-dirty left-spacing since centering is broken :P --%>
    <table width="200"  bgcolor="Black" ><tr><td></td></tr></table>
</td>

<td class="lb rb" bgcolor="Black" align="center">
<br />
    <div align="center"  >
        <asp:Image ID="Image5" runat="server" Height="80px" 
            ImageUrl="~/images/login/OMS_login_banner2.jpg" Width="788px" /> </div> <br />   <%-- 786px --%>
    <br /><br /><br /><br /><br /><br />


	
<div align="center">
<table cellspacing="0" width="200" cellpadding="0" bgcolor="Black" frame="box" 
        style="border: thick groove #3366FF"> 
  <tr>
      <td>
            <div><asp:Image ID="Image1" runat="server" Height="96px" ImageUrl="~/images/login/restricted_access4.jpg" Width="412px" BackColor="Black" /></div> 
            <br />
	        <table align="center" width="430" bgcolor="Black">
            <tr>
                <td>
                    <div style="float:left">
                        <asp:Image ID="Image2" runat="server" Height="25px" ImageUrl="~/images/login/username4.jpg" Width="154px" />        
                        <asp:Image ID="Image3" runat="server" Height="25px" ImageUrl="~/images/login/password4.jpg" Width="154px" />
                    </div>  
                </td>
  
                <td>
                    <div style="float:left"><asp:TextBox ID="Username" runat="server" Width="201px" BorderStyle="Ridge"></asp:TextBox></div>
                    <div style="float:left"><asp:TextBox ID="Password" runat="server" Width="201px" BorderStyle="Ridge" TextMode="Password"></asp:TextBox></div>
                </td>
            </tr>
            </table>
            <br /><br/><br /><br /><br/>

	        <table align="center" width="430" bgcolor="Black">
		        <tr bgcolor="Black">
			        <td align="center">
				        <asp:Button id="Login" onclick="Login_Click" runat="server" Text="Sign in" ForeColor="#000066"></asp:Button>&nbsp;
                            <%--<a href="javascript:popUp('forgetpassword.aspx')"><span id="fgot" runat="server" style="color: #FFFFFF"> Forgot your password?</span></a>--%>
				            <br/>
				        <%--<asp:HiddenField ID="SOURCE" runat="server" Value="CW" />--%>
				        <br/>
				        <asp:Label id="logcheck" runat="server" ForeColor="white"></asp:Label>
                    </td>
			       <%-- <td align="left" bgcolor="Black"></td>--%>
		        </tr>

                <tr>        <%--temporary making this invisible so that "report" field is working correctly for SBIR --%>
		          <td colspan="2" align="center">
			        <table width="100%">
			            <tr><td valign="top"><asp:CheckBox ID="report" Text="View Reports and Account Settings" Runat="server" ForeColor="White" Visible="False" /></td></tr>
                    </table>				      
		          </td>	
            			    
		          <%--<td align="left" bgcolor="Black"></td>--%>
	            </tr>
           </table>
      </td>
   </tr>
</table>		
</div>
<br /><br /><br /><br />		
			
</td></tr>
</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Tools;

namespace OMS.WebApp
{
    public partial class login1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void Login_Click(Object sender, EventArgs e)
        {
            // input values from our markup (web form)
            string username = Username.Text.ToLower();  // "oms2"; //  
            string password = Password.Text;  // "test2"; // 

            #region query db
            //string sessionlink = "session.aspx?id=";
            //string enc_pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(pwd, "SHA1");

            SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd = new SqlCommand("oms_login", SQLConn);       //cw_login
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter Param;
            Param = cmd.Parameters.Add("@un", SqlDbType.VarChar, 25);
            Param.Value = username;
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@pw", SqlDbType.VarChar, 25);
            Param.Value = password;
            Param.Direction = ParameterDirection.Input;

            Param = cmd.Parameters.Add("@id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@role_id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@fn", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@ln", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@tid", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@role_dscr", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@expdays", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@active", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@retval", SqlDbType.Int);
            Param.Direction = ParameterDirection.ReturnValue;

            SQLConn.Open();
            cmd.ExecuteNonQuery();
            SQLConn.Close();
            #endregion query db

            #region config session
            if ((int)cmd.Parameters["@retval"].Value == 0)
            {

            //    //------------------------
            //    //Response.Redirect("http://operationmentalscorpion.com//cwsa/omsk12/dev/omstrainer/CW.SA.DemoTestPage.html?tid=" + traineeID);
            //    //Response.Redirect("http://209.196.155.44//cwsa/omsk12/dev/omstrainer/CW.SA.DemoTestPage.html?tid=" + traineeID);

                #region load settings
                string urlOMSTraineeApp = "";
                OMS.Tools.OMSConfig.GetOMSConfig();
                string userGrpMotivatorsOFF = "alpha";  // alpha  oms4  // TODO: put this in config file
                urlOMSTraineeApp = username.StartsWith(userGrpMotivatorsOFF) ? OMS.Tools.OMSConfig.URLMotivatorsOFF : OMS.Tools.OMSConfig.URLMotivatorsONN;

                if (!CWTools.CWTrue) { }
                else if (OMS.Tools.OMSConfig.IsUserInSet(username: username, usersubsets: OMS.Tools.OMSConfig.MOFFUsers))
                    urlOMSTraineeApp = OMS.Tools.OMSConfig.URLMotivatorsOFF;

                else if (OMS.Tools.OMSConfig.IsUserInSet(username: username, usersubsets: OMS.Tools.OMSConfig.MONNUsers))
                    urlOMSTraineeApp = OMS.Tools.OMSConfig.URLMotivatorsONN;

                else if (OMS.Tools.OMSConfig.IsUserInSet(username: username, usersubsets: OMS.Tools.OMSConfig.MSELUsers))
                    urlOMSTraineeApp = OMS.Tools.OMSConfig.URLMotivatorsSEL;

                else
                    urlOMSTraineeApp = OMS.Tools.OMSConfig.URLMotivatorsSEL;


                #endregion load settings

                int traineeID = (int)cmd.Parameters["@tid"].Value;
                //Response.Redirect("oms_select_group.htm?tid=" + cmd.Parameters["@tid"].Value);
                //Response.Redirect("oms_select_group.htm?tid=" + traineeID);
                Response.Redirect(urlOMSTraineeApp+"?tid=" + traineeID);
            }
            else
            {
                string loginerr = "Invalid Login! Check your password and try again.";
                logcheck.Text = "<font color=\"FireBrick\">" + loginerr + "</font>";  //Invalid Login! Check your password and try again.   
            }
            #endregion config session


        }  // Login_Click()

    }
}
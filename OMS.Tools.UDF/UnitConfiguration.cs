﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CW.Tools;
using OMS.Tools;

namespace OMS.Tools.UDF
{

    /// <summary>
    /// datastructures for json-encoding (for transport via web srvc)
    /// </summary>
    public class Json
    {

        #region datastructures for json-encoding for n-back

        #region ALERT regarding instance names:
        // instance names in the below structures correspond to the public data-transport spec and will break 
        // frontend/backend comms if changed without notice
        #endregion ALERT


        public class NBackBasePaths
        {
            public string a = "kidsoms.com/assets/cnt/s/";
            public string v = "kidsoms.com/assets/cnt/p/";
        }

        public class NBackStimulus
        {
            public bool IsTrg = false;
            public string AssetRelPath = "";
        }

        public class NBackStimulusPair
        {
            public NBackStimulus a = new NBackStimulus();
            public NBackStimulus v = new NBackStimulus();
        }

        public class NBackUnitConfig
        {
            public string unit_id = "L001G002N011";
            public int difficulty = 1;  // difficulty level (== game level)
            public int recall = 1;      // AV-Recall value
            public int stimulus_duration = 1800;  // in milliseconds; interval between onsets

            public NBackBasePaths base_paths = new NBackBasePaths();

            public NBackStimulusPair[] stimuli = new NBackStimulusPair[1];
        }

        public class NBackUnitConfigWrapper
        {
            public NBackUnitConfig unit = new NBackUnitConfig();
        }

        #endregion datastructures for json-encoding for n-back

        public NBackUnitConfig nbackConfig = new NBackUnitConfig();
    }


    /// <summary>
    /// Descriptor for dynamically loaded external assets, in particular filesystem/web location.
    /// </summary>
    internal class AssetDescriptor
    {
        public string RelPath { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
    }

    /// <summary>
    /// Descriptor for n-back stimulus, incl external asset and isTarget
    /// </summary>
    internal class Stimulus
    {
        public AssetDescriptor AssetDescr = new AssetDescriptor();

        public bool IsTrg = false;
    }


    /// <summary>
    /// Manage stored UDF data, generate output for web services etc.
    /// ALERT: Assumes config file exists: C:\cwconfig\omsconfig.txt
    /// 
    /// Currently geared solely for OMS n-back game; TODO: generalize to other games.
    /// </summary>
    public class UnitConfiguration
    {
        //internal static string pathOMSConfig = @"D:\cwconfig\omsconfig.txt";
        ////internal static string pathOMSConfig = @"C:\cwconfig\omsconfig.txt";

        public enum GameIDs
        {
            Default,
            NBack,
            NBackNavy,
            NBackKids,
            Last
        }

        static StreamReader openRead(string filepathIn)
        {
            StreamReader sr = null;
            try
            {
                //Pass the file path and file name to the StreamReader constructor
                sr = new StreamReader(filepathIn);
            }
            catch (Exception e)
            {
                //Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                //Console.WriteLine("Executing finally block.");
            }

            return sr;
        }

        /// <summary>
        /// Determine path to UDF, read in as filtered list of records.
        /// </summary>
        /// <param name="gameLevel"></param>
        /// <param name="unitID"></param>
        /// <returns></returns>
        private static List<string> GetDataLinesIn(int gameLevel, string unitID)
        {
            #region [old] UDF identifiers
            //int levelIdx = gameLevel;
            //string devLevel = "";  // ""  sample
            //string levelID = "L" + devLevel + gameLevel.ToString("D02");
            ////levelID = "sample";  // DEV
            ////unitID = "sampleUDF_v01";
            #endregion

            #region get filepath for level,unit

            string basePathUDF = OMSGameConfig.NBack.Navy.BasePathUDF;
            string levelPrefix = OMSGameConfig.NBack.Navy.LevelPrefix;

            string levelID = levelPrefix + gameLevel.ToString("D02");

            string pathUDF = basePathUDF + levelID + "/" + unitID + ".txt";
            //string pathUDF = @"D:\cwassets\cnt\u\sampleUDF_v01.txt";
            //string pathUDF = @"D:\cwassets\cnt\u\L01\L001G001N001.txt";
            #endregion get filepath for uid  (unitID)

            List<string> datalinesIn;
            datalinesIn = new List<string>();

            #region open UDF for read
            StreamReader sr = openRead(pathUDF);
            //if (sr == null) { promptFinish(); return; }  // TODO: error handling
            #endregion

            #region read in file contents as List<string>, preprocessing to remove comment- and empty lines
            {
                string dataLine = "";
                while (true)
                {
                    dataLine = sr.ReadLine();
                    if (dataLine == null) break;

                    // preprocess: trim leading,trailing whitespace; omit meta-lines
                    dataLine.Trim();
                    if (
                        dataLine.Length < 2 ||
                        dataLine.StartsWith("#") ||
                        dataLine.StartsWith("//")
                        ) continue;

                    datalinesIn.Add(dataLine);

                    #region sample n-back unit data (incomplete)
                    //n; 1
                    //#c; streamlen:30

                    //i; p:school/SAMPLE
                    //i; i:apple.jpgSAMPLESAMPLESAMPLE
                    //i; i:stapler.jpgSAMPLESAMPLESAMPLESAMPLE
                    //i; i:pencils.jpg
                    //i; i:pencils.jpg
                    //i; i:crayons.jpg
                    //i; i:tape.jpg
                    #endregion

                }  // while (dataline)

                sr.Close();
            }
            #endregion read in file

            return datalinesIn;
        }  // GetDataLinesIn()

        internal static class OMSConfig
        {
            public static string BaseConfig = @"C:\cwconfig\";
            public static string PathConfigFile = BaseConfig + @"omsconfig.txt";
        }

        /// <summary>
        /// store global config data read in from external file
        /// </summary>
        internal static class OMSGameConfig
        {
            public static string PathConfigFile = OMSConfig.BaseConfig;

            public static class NBack
            {
                public static class Navy
                {
                    // path to UDF's; note: UDF's read locally by server-side code
                    public static string BasePathUDF = "";  // @"D:\cwassets\cnt\u\";
                    public static string LevelPrefix = "L";  // prefix for UDF subfolders; ex: ~/cwassets/u/Navy/ + L01/ + L001G001N368.txt

                    // path to assets; note: assets http-downloaded by client-side code and require full URL
                    public static string BasePathAud = "http://kidsoms.com/assets/navy/snds/";
                    public static string BasePathVis = "http://kidsoms.com/assets/navy/imgs/";
                }
            }
        }


        


        internal class UnitConfig
        {
            public string UnitID = "";  // ex. "L001G002N011"
            public int GameLevel = 1;
            public int AVRecallValue = 1;
            public int StimulusDuration = 1800;  // onset interval in milliseconds; optional UDF param; valid if >0 else revert to default

            // TODO: read in base paths from external config file
            public string BasePathAud = "cnt/s/kids/";
            public string BasePathVis = "cnt/p/kids/";

            public int StreamLen = 0;
            public List<Stimulus> Pics = new List<Stimulus>();
            public List<Stimulus> Snds = new List<Stimulus>();
        }


        /// <summary>
        /// Loads external config data for unitID and returns data as instance of UnitConfig
        /// </summary>
        /// <param name="gameLevel"></param>
        /// <param name="unitID">full unitID e.g. "L001G001N100"</param>
        /// <returns></returns>
        internal static UnitConfig getUnitConfig(GameIDs gameID, int gameLevel, string unitID)
        {
            UnitConfig unitConfig = new UnitConfig();

            #region input global config data from external config file: asset base paths etc


            //string basePathUDF = "";  // @"D:\cwassets\cnt\u\";
            //string levelPrefix = "L";

            // TODO: input snd-file extension from ext. config file 
            string sndFileExtension = ".mp3";

            #region load game-path config
            {
                // note: usually PathConfigFile == @"C:\cwconfig\omsconfig.txt";
                StreamReader srr = openRead(OMSConfig.PathConfigFile);  // TODO: errhandling
                string dataLine = "";
                while (true)
                {
                    dataLine = srr.ReadLine();
                    if (dataLine == null) break;

                    // preprocess: trim leading,trailing whitespace; omit meta-lines
                    dataLine.Trim();
                    if (
                        dataLine.Length < 2 ||
                        dataLine.StartsWith("#") ||
                        dataLine.StartsWith("//")
                        ) continue;


                    string[] tokv = CWTools.CWSplit(record: dataLine, delims: "=", trimWhitespace: true);
                    string key = tokv[0];
                    string val = tokv[1];
                    #region store token values in OMSConfig
                    if (false) { }
                    else if
                    (
                           gameID == GameIDs.NBackKids
                        && key == "pathconfignbackkids"
                    )
                        OMSGameConfig.PathConfigFile = OMSConfig.BaseConfig + val;  // TODO: make this more robust

                    else if
                    (
                           gameID == GameIDs.NBackNavy
                        && key == "pathconfignbacknavy"
                    )
                        OMSGameConfig.PathConfigFile = OMSConfig.BaseConfig + val;  // TODO: make this more robust

                    else if
                    (
                           gameID == GameIDs.NBack
                        && key == "pathconfignback"
                    )
                        OMSGameConfig.PathConfigFile = OMSConfig.BaseConfig + val;  // TODO: make this more robust

                    #endregion store tokens
                }  // while (pathConfig:dataline)

                srr.Close();
            }  // local code block: import omsconfig
            #endregion load game-path config

            #region load game config
            {
                // note:  PathConfigFile assigned in 'load game-path config'
                StreamReader srr = openRead(OMSGameConfig.PathConfigFile);  // TODO: errhandling
                string dataLine = "";
                while (true)
                {
                    dataLine = srr.ReadLine();
                    if (dataLine == null) break;

                    // preprocess: trim leading,trailing whitespace; omit meta-lines
                    dataLine.Trim();
                    if (
                        dataLine.Length < 2 ||
                        dataLine.StartsWith("#") ||
                        dataLine.StartsWith("//")
                        ) continue;


                    string[] tokv = CWTools.CWSplit(record: dataLine, delims: "=", trimWhitespace: true);
                    #region store token values in OMSConfig  TODO: fix this for-loop (e.g. see above)
                    for (int i = 0; i < 1; i++)
                    {
                        if (false) { }

                        else if
                        (
                                tokv[i] == "basePathUDF"
                            || tokv[i] == "basePathNBackUDF"
                            || tokv[i] == "basePathNBackNavyUDF"
                        )
                            OMSGameConfig.NBack.Navy.BasePathUDF = tokv[1];  // TODO: make this more robust
                        else if
                        (
                                tokv[i] == "UDFLevelPrefix"
                            || tokv[i] == "NBackUDFLevelPrefix"
                            || tokv[i] == "NBackNavyUDFLevelPrefix"
                        )
                            OMSGameConfig.NBack.Navy.LevelPrefix = tokv[1];  // TODO: make this more robust

                        else if
                        (
                                tokv[i] == "basePathAud"
                            || tokv[i] == "basePathNBackAud"
                            || tokv[i] == "basePathNBackNavyAud"
                        )
                            OMSGameConfig.NBack.Navy.BasePathAud = tokv[1];  // TODO: make this more robust
                        else if
                        (
                                tokv[i] == "basePathVis"
                            || tokv[i] == "basePathNBackVis"
                            || tokv[i] == "basePathNBackNavyVis"
                        )
                            OMSGameConfig.NBack.Navy.BasePathVis = tokv[1];  // TODO: make this more robust
                    }  // for i<tokc
                    #endregion store tokens
                }  // while (pathConfig:dataline)

                srr.Close();
            }  // local code block: import omsconfig
            #endregion load game config

            #endregion input global config data

            #region init local datastructures and load udf
            List<string> datalinesIn;
            List<string> datalinesOut;

            datalinesOut = new List<string>();
            datalinesIn = GetDataLinesIn(gameLevel: gameLevel, unitID: unitID);

            datalinesOut.Add(Environment.CurrentDirectory);  // for dev
            datalinesOut.Add("hello july 17");  // for dev
            #endregion init local datastructures

            #region parse and analyze loaded content (stored in datalinesIn)

            // determining streamlen:
            //   config  : from optional explicit record in UDF; has priority if present
            //   counted : fallback if no config: MIN(pics.len, snds.len)
            //   net     : resulting value for returns;
            int streamLenConfig = -1;
            int streamLenCounted = 0;
            int streamLenNet = 0;

            // store dirpath and filename for each asset file
            List<AssetDescriptor> pics = new List<AssetDescriptor>();
            List<AssetDescriptor> snds = new List<AssetDescriptor>();

            int avRecallValue = -1;         // init to invalid value to catch problems with input val
            int stimulusOnsetInterval = 0;  // milliseconds; optional UDF param; valid if >0 else revert to default


            #region parse algorithm:
            // 
            // - iterate through datalinesIn, storing pics and snds in separate lists;
            // - determine streamlenNet

            // build output:
            //for (int i = 0; i < streamLenNet; i++)
            //{
            //    // check isNeeded outputLine for updating relpath(s)
            //    // build outputLine for stimulus-pair: "s; "+ snds[i].filename + "; " + pics[i].filename
            //}
            #endregion parse algorithm

            #region parse datalinesIn
            string relPathSnd = "";
            string relPathImg = "";

            foreach (string dataline in datalinesIn)
            {

                string[] tokv = CWTools.Tokenize(dataline);

                string recordKey = tokv[0];

                if (recordKey == "")
                {
                }
                else if (recordKey == "n")
                {
                    avRecallValue = int.Parse(tokv[1]);
                }
                #region config settings
                else if (recordKey == "c")
                {
                    // TODO: clean up tokenizing
                    for (int i = 1; i < tokv.Length; i++)
                    {
                        if (tokv[i] == "")
                        {
                        }
                        else if (tokv[i].ToLower().StartsWith("displayduration:"))
                        {
                            stimulusOnsetInterval = int.Parse(tokv[i].Substring(16));
                            //if (trt) tr.tr(this, ":loadGameDataset() displayduration from UDF: ", displayDuration);
                        }
                        else if (tokv[i].ToLower().StartsWith("streamlen:"))
                        {
                            streamLenConfig = int.Parse(tokv[i].Substring(10));
                        }
                        else if (tokv[i] == "sync")
                        {
                        }
                    }
                }
                #endregion config settings
                #region i; visual item: image
                else if (recordKey == "i")
                {
                    //if (trt) tr.tr("");
                    //if (trt) tr.tr("CWGNBack.loadGameDataset(): dataline: ", dataline);

                    //// visual items (images (img) and skins (dictpath,style)
                    //// iMAGE; lABEL; iMAGEFILE; pATH_OF_FOLDER; fULL_PATH_OF_IMAGE_FILE
                    ////  where :
                    ////    - each 'i' record describes exactly one image file (== one vis. stimulus);
                    ////    - 'p' sets default folder (aka dir) for this and subsequent images;
                    ////    - 'i' is filename+extension; overridden if 'f' present;
                    ////    - 'f' overrides default folder path for this image only, without affecting default dir path;
                    ////    - each 'i' record should contain exactly one 'f' or 'i'; if both are present, whicnever is last takes priority;
                    //datalinesFull.Add("i; l:B2 Stealth Bomber; p:../Images/CoverFlow/; i:OMS_B2StealthBomber_3.jpg; ");
                    //datalinesFull.Add("i; l:London; i:London_2.jpg");

                    //string label = "";
                    //string imgPath = "";
                    string imgFilename = "";
                    string imgPathFull = "";

                    for (int i = 1; i < tokv.Length; i++)
                    {
                        if (tokv[i] == "") continue;
                        // tokenize on ':'
                        string[] ttokv = CWTools.Tokenize(tokv[i]);
                        if (ttokv.Length < 2) continue;  // errchk

                        if (ttokv[0] == "")
                        {
                        }
                        else if (ttokv[0] == "l")
                        {
                            //label = ttokv[1];   // commented to disable labels
                        }
                        else if (ttokv[0] == "i")
                        {
                            imgFilename = ttokv[1];
                        }
                        else if (ttokv[0] == "f")
                        {
                            imgPathFull = ttokv[1];
                        }
                        else if (ttokv[0] == "p")
                        {
                            relPathImg = ttokv[1];
                            if (!(relPathImg == "") && !relPathImg.EndsWith("/"))
                                relPathImg += "/";
                        }
                    }
                    // if dataline contained img ref, add it to img's list
                    if (imgFilename != "")
                    {
                        pics.Add(new AssetDescriptor() { RelPath = relPathImg, FileName = imgFilename });
                        //itemImageDescrs.Add(new ImageDescr(imagePath: imgPath, imageLabel: label));
                        //itemImageIDs.Add(imgPath);  // for preloading image files
                        //ImageDescr imgDscr = new ImageDescr(imagePath: imgPath, imageLabel: label);
                        //if (trt) tr.tr("   imgc=", itemImageIDs.Count, "; imagePath: ", imgPath, "; imageLabel: ", label);
                    }
                }
                #endregion visual items
                #region s; audio item: sound
                else if (recordKey == "s")
                {
                    ////if (trt) tr.tr("");
                    ////if (trt) tr.tr("CWGNBack.loadGameDataset(): dataline: ", dataline);

                    ////// sOUND; sOUNDFILE; pATH_OF_FOLDER; fULL_PATH_OF_SOUND_FILE_WITHOUT_EXTENSION
                    //////  where 'p' sets default folder 
                    ////datalinesFull.Add("s; p:soundeffects/; s:opAddition");
                    ////datalinesFull.Add("s; s:opSubtraction");
                    ////datalinesFull.Add("s; f:math/voice0/70");

                    string sndFilename = "";
                    string sndPathFull = "";

                    for (int i = 1; i < tokv.Length; i++)
                    {
                        if (tokv[i] == "") continue;
                        // tokenize on ':'
                        string[] ttokv = CWTools.Tokenize(tokv[i]);
                        if (ttokv.Length < 2) continue;  // errchk

                        if (ttokv[0] == "")
                        {
                        }
                        else if (ttokv[0] == "s")
                        {
                            sndFilename = ttokv[1];
                        }
                        else if (ttokv[0] == "f")
                        {
                            sndPathFull = ttokv[1];
                        }
                        else if (ttokv[0] == "p")
                        {
                            relPathSnd = ttokv[1];
                            if (relPathSnd == "/") relPathSnd = "";  // safety catch
                            if ((relPathSnd != "") && !relPathSnd.EndsWith("/"))
                                relPathSnd += "/";
                        }
                    }
                    // if dataline contained snd ref, add it to snd's list
                    if (sndFilename != "")
                    {
                        snds.Add(new AssetDescriptor() { RelPath = relPathSnd, FileName = sndFilename, FileExtension = sndFileExtension });
                        //itemImageDescrs.Add(new ImageDescr(imagePath: sndPath, imageLabel: label));
                        //itemImageIDs.Add(sndPath);  // for preloading image files
                        //ImageDescr sndDscr = new ImageDescr(imagePath: sndPath, imageLabel: label);
                        //if (trt) tr.tr("   sndc=", itemImageIDs.Count, "; imagePath: ", sndPath, "; imageLabel: ", label);
                    }
                }  // else if 's'
                #endregion audio item

            }  // foreach (string dataline in datalines)
            #endregion parse datalines

            #region analyze loaded content

            #region prepend buffer to pics,snds to gracefully handle edges
            {
                List<AssetDescriptor> empties = new List<AssetDescriptor>();
                for (int i = 0; i < avRecallValue; i++)
                {
                    empties.Add(new AssetDescriptor() { RelPath = relPathSnd, FileName = "" });
                }
                snds.InsertRange(0, empties);
                pics.InsertRange(0, empties);
            }
            #endregion



            streamLenCounted = Math.Min(snds.Count, pics.Count);
            if (streamLenConfig > -1)
                streamLenNet = Math.Min(streamLenConfig, streamLenCounted);
            else
                streamLenNet = streamLenCounted;

            #endregion analyze loaded content

            #endregion parse and analyze loaded content

            #region build (unit-specific section of) UnitConfig

            // meta udf-values: unitID, level,avRecall, onsetInterval,  streamlen
            unitConfig.UnitID = unitID;
            unitConfig.GameLevel = gameLevel;
            unitConfig.AVRecallValue = avRecallValue;
            if (stimulusOnsetInterval > 0)
                unitConfig.StimulusDuration = stimulusOnsetInterval;  // amount of time (milliseconds) between consecutive stimulus-pair onsets

            unitConfig.BasePathAud = OMSGameConfig.NBack.Navy.BasePathAud;
            unitConfig.BasePathVis = OMSGameConfig.NBack.Navy.BasePathVis;

            #region build UnitConfig stream data
            for (int i = avRecallValue; i < streamLenNet; i++)  // first elts are dummies to handle edges gracefully
            {
                #region determine matches (targets)
                // TODO: refine target tests to include path
                bool isAudTarget = snds[i].FileName == snds[i - avRecallValue].FileName;
                bool isVidTarget = pics[i].FileName == pics[i - avRecallValue].FileName;

                #endregion determine match code

                unitConfig.Pics.Add(new Stimulus() { AssetDescr = pics[i], IsTrg = isVidTarget });
                unitConfig.Snds.Add(new Stimulus() { AssetDescr = snds[i], IsTrg = isAudTarget });
            }  // for i in stimuli
            #endregion build UnitConfig stream data

            unitConfig.StreamLen = unitConfig.Pics.Count;  // == Snds.Count == streamLenNet - avRecallValue

            #endregion build stream

            return unitConfig;
        }  // getUnitConfig()

        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitID">unitID of unit-data to retrieve. Ex: "L001G002N011"</param>
        /// <returns></returns>
        public static string GetDataAsJSON(GameIDs gameID, int gameLevel, string unitID)
        {
            // retrieve external unit config data
            UnitConfig unitConfig = getUnitConfig(gameID: gameID, gameLevel: gameLevel, unitID: unitID);


            #region transfer unitConfig to json structure Json.NBackUnitConfig
            Json json = new Json();  // json structure for unit-data

            Json.NBackUnitConfig jsUnitConfig = json.nbackConfig;

            jsUnitConfig.unit_id = unitConfig.UnitID;
            jsUnitConfig.difficulty = unitConfig.GameLevel;
            jsUnitConfig.recall = unitConfig.AVRecallValue;
            jsUnitConfig.stimulus_duration = unitConfig.StimulusDuration;

            jsUnitConfig.base_paths.a = unitConfig.BasePathAud;
            jsUnitConfig.base_paths.v = unitConfig.BasePathVis;

            List<Json.NBackStimulusPair> stimulist = new List<Json.NBackStimulusPair>();
            for (int i = 0; i < unitConfig.StreamLen; i++)
            {
                Stimulus a = unitConfig.Snds[i];
                Stimulus v = unitConfig.Pics[i];

                AssetDescriptor assetDescr;
                assetDescr = a.AssetDescr;
                string sndRelPath = assetDescr.RelPath + assetDescr.FileName + assetDescr.FileExtension;
                assetDescr = v.AssetDescr;
                string picRelPath = assetDescr.RelPath + assetDescr.FileName + assetDescr.FileExtension;

                stimulist.Add(new Json.NBackStimulusPair()
                {
                    a = new Json.NBackStimulus() { IsTrg = a.IsTrg, AssetRelPath = sndRelPath },
                    v = new Json.NBackStimulus() { IsTrg = v.IsTrg, AssetRelPath = picRelPath }
                });
            }

            #region [commented] sample list of stimuli (hardcoded values)
            //// dev: hardcoded sample data
            //stimulist.Add(new Json.NBackStimulusPair()
            //{
            //    a = new Json.NBackStimulus() { IsTrg = true, AssetRelPath = "c.mp3" },
            //    v = new Json.NBackStimulus() { IsTrg = false, AssetRelPath = "dog.jpg" }
            //});
            //stimulist.Add(new Json.NBackStimulusPair()
            //{
            //    a = new Json.NBackStimulus() { IsTrg = false, AssetRelPath = "altLetters/r.mp3" },
            //    v = new Json.NBackStimulus() { IsTrg = true, AssetRelPath = "altPics/car.png" }
            //});
            //stimulist.Add(new Json.NBackStimulusPair()
            //{
            //    a = new Json.NBackStimulus() { IsTrg = true, AssetRelPath = "altLetters/d.mp3" },
            //    v = new Json.NBackStimulus() { IsTrg = true, AssetRelPath = "zebra.jpg" }
            //});
            //stimulist.Add(new Json.NBackStimulusPair()
            //{
            //    a = new Json.NBackStimulus() { IsTrg = false, AssetRelPath = "instruments/oboe.mp3" },
            //    v = new Json.NBackStimulus() { IsTrg = false, AssetRelPath = "battleship.jpg" }
            //});
            #endregion [commented] sample

            jsUnitConfig.stimuli = stimulist.ToArray();
            #endregion transfer unitConfig to json structure Json.NBackUnitConfig

            string jsonstr = System.Web.Helpers.Json.Encode(new Json.NBackUnitConfigWrapper() { unit = jsUnitConfig });
            return jsonstr;
        }  // GetDataAsJSON()




        #region old stuff

        /// <summary>
        /// Retrieves unit data for unit /uid/.
        /// Assumes config file exists: C:\cwconfig\omsconfig.txt
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static List<string> GetData(int gameLevel, string unitID)
        {
            #region init local datastructures
            List<string> datalinesIn;
            List<string> datalinesOut;

            datalinesOut = new List<string>();
            datalinesIn = GetDataLinesIn(gameLevel: gameLevel, unitID: unitID);

            datalinesOut.Add(Environment.CurrentDirectory);  // for dev
            datalinesOut.Add("hello july 17");  // for dev
            #endregion init local datastructures

            #region parse and analyze loaded content (stored in datalinesIn)

            // determining streamlen:
            //   config  : from optional explicit record in UDF; has priority if present
            //   counted : fallback if no config: MIN(pics.len, snds.len)
            //   net     : resulting value for returns;
            int streamLenConfig = -1;
            int streamLenCounted = 0;
            int streamLenNet = 0;

            // store dirpath and filename for each asset file
            List<AssetDescriptor> pics = new List<AssetDescriptor>();
            List<AssetDescriptor> snds = new List<AssetDescriptor>();

            int avRecallValue = 1;
            int displayDuration = 0;  // milliseconds; optional UDF param; valid if >0 else revert to default

            // TODO: read in base paths from external config file
            string basePathAud = "cnt/s/kids/";
            string basePathVid = "cnt/p/kids/";

            #region parse algorithm:
            // 
            // - iterate through datalinesIn, storing pics and snds in separate lists;
            // - determine streamlenNet

            // build output:
            //for (int i = 0; i < streamLenNet; i++)
            //{
            //    // check isNeeded outputLine for updating relpath(s)
            //    // build outputLine for stimulus-pair: "s; "+ snds[i].filename + "; " + pics[i].filename
            //}
            #endregion parse algorithm

            #region parse datalinesIn
            string relPathSnd = "";
            string relPathImg = "";

            // prepend buffer to pics,snds to gracefully handle edges
            for (int i = 0; i < Math.Min(1, avRecallValue); i++)
            {
                snds.Add(new AssetDescriptor() { RelPath = relPathSnd, FileName = "" });
                pics.Add(new AssetDescriptor() { RelPath = relPathImg, FileName = "" });
            }

            foreach (string dataline in datalinesIn)
            {

                string[] tokv = CWTools.Tokenize(dataline);

                string recordKey = tokv[0];

                if (recordKey == "")
                {
                }
                else if (recordKey == "n")
                {
                    avRecallValue = int.Parse(tokv[1]);
                }
                #region config settings
                else if (recordKey == "c")
                {
                    // TODO: clean up tokenizing
                    for (int i = 1; i < tokv.Length; i++)
                    {
                        if (tokv[i] == "")
                        {
                        }
                        else if (tokv[i].ToLower().StartsWith("displayduration:"))
                        {
                            displayDuration = int.Parse(tokv[i].Substring(16));
                            //if (trt) tr.tr(this, ":loadGameDataset() displayduration from UDF: ", displayDuration);
                        }
                        else if (tokv[i].ToLower().StartsWith("streamlen:"))
                        {
                            streamLenConfig = int.Parse(tokv[i].Substring(10));
                        }
                        else if (tokv[i] == "sync")
                        {
                        }
                    }
                }
                #endregion config settings
                #region i; visual item: image
                else if (recordKey == "i")
                {
                    //if (trt) tr.tr("");
                    //if (trt) tr.tr("CWGNBack.loadGameDataset(): dataline: ", dataline);

                    //// visual items (images (img) and skins (dictpath,style)
                    //// iMAGE; lABEL; iMAGEFILE; pATH_OF_FOLDER; fULL_PATH_OF_IMAGE_FILE
                    ////  where :
                    ////    - each 'i' record describes exactly one image file (== one vis. stimulus);
                    ////    - 'p' sets default folder (aka dir) for this and subsequent images;
                    ////    - 'i' is filename+extension; overridden if 'f' present;
                    ////    - 'f' overrides default folder path for this image only, without affecting default dir path;
                    ////    - each 'i' record should contain exactly one 'f' or 'i'; if both are present, whicnever is last takes priority;
                    //datalinesFull.Add("i; l:B2 Stealth Bomber; p:../Images/CoverFlow/; i:OMS_B2StealthBomber_3.jpg; ");
                    //datalinesFull.Add("i; l:London; i:London_2.jpg");

                    //string label = "";
                    //string imgPath = "";
                    string imgFilename = "";
                    string imgPathFull = "";

                    for (int i = 1; i < tokv.Length; i++)
                    {
                        if (tokv[i] == "") continue;
                        // tokenize on ':'
                        string[] ttokv = CWTools.Tokenize(tokv[i]);
                        if (ttokv.Length < 2) continue;  // errchk

                        if (ttokv[0] == "")
                        {
                        }
                        else if (ttokv[0] == "l")
                        {
                            //label = ttokv[1];   // commented to disable labels
                        }
                        else if (ttokv[0] == "i")
                        {
                            imgFilename = ttokv[1];
                        }
                        else if (ttokv[0] == "f")
                        {
                            imgPathFull = ttokv[1];
                        }
                        else if (ttokv[0] == "p")
                        {
                            relPathImg = ttokv[1];
                            if (!(relPathImg == "") && !relPathImg.EndsWith("/"))
                                relPathImg += "/";
                        }
                    }
                    // if dataline contained img ref, add it to img's list
                    if (imgFilename != "")
                    {
                        pics.Add(new AssetDescriptor() { RelPath = relPathImg, FileName = imgFilename });
                        //itemImageDescrs.Add(new ImageDescr(imagePath: imgPath, imageLabel: label));
                        //itemImageIDs.Add(imgPath);  // for preloading image files
                        //ImageDescr imgDscr = new ImageDescr(imagePath: imgPath, imageLabel: label);
                        //if (trt) tr.tr("   imgc=", itemImageIDs.Count, "; imagePath: ", imgPath, "; imageLabel: ", label);
                    }
                }
                #endregion visual items
                #region s; audio item: sound
                else if (recordKey == "s")
                {
                    ////if (trt) tr.tr("");
                    ////if (trt) tr.tr("CWGNBack.loadGameDataset(): dataline: ", dataline);

                    ////// sOUND; sOUNDFILE; pATH_OF_FOLDER; fULL_PATH_OF_SOUND_FILE_WITHOUT_EXTENSION
                    //////  where 'p' sets default folder 
                    ////datalinesFull.Add("s; p:soundeffects/; s:opAddition");
                    ////datalinesFull.Add("s; s:opSubtraction");
                    ////datalinesFull.Add("s; f:math/voice0/70");

                    string sndFilename = "";
                    string sndPathFull = "";

                    for (int i = 1; i < tokv.Length; i++)
                    {
                        if (tokv[i] == "") continue;
                        // tokenize on ':'
                        string[] ttokv = CWTools.Tokenize(tokv[i]);
                        if (ttokv.Length < 2) continue;  // errchk

                        if (ttokv[0] == "")
                        {
                        }
                        else if (ttokv[0] == "s")
                        {
                            sndFilename = ttokv[1];
                        }
                        else if (ttokv[0] == "f")
                        {
                            sndPathFull = ttokv[1];
                        }
                        else if (ttokv[0] == "p")
                        {
                            relPathSnd = ttokv[1];
                            if (!(relPathSnd == "") && !relPathSnd.EndsWith("/"))
                                relPathSnd += "/";
                        }
                    }
                    // if dataline contained snd ref, add it to snd's list
                    if (sndFilename != "")
                    {
                        snds.Add(new AssetDescriptor() { RelPath = relPathSnd, FileName = sndFilename });
                        //itemImageDescrs.Add(new ImageDescr(imagePath: sndPath, imageLabel: label));
                        //itemImageIDs.Add(sndPath);  // for preloading image files
                        //ImageDescr sndDscr = new ImageDescr(imagePath: sndPath, imageLabel: label);
                        //if (trt) tr.tr("   sndc=", itemImageIDs.Count, "; imagePath: ", sndPath, "; imageLabel: ", label);
                    }
                }  // else if 's'
                #endregion audio item

            }  // foreach (string dataline in datalines)
            #endregion parse datalines

            #region analyze loaded content
            streamLenCounted = Math.Min(snds.Count, pics.Count);
            if (streamLenConfig > -1)
                streamLenNet = Math.Min(streamLenConfig, streamLenCounted);
            else
                streamLenNet = streamLenCounted;

            #endregion analyze loaded content

            #endregion parse and analyze loaded content




            #region output as needed by h5-oms

            // tmp: copy a few lines from UDF
            //datalinesOut.Add("------------- datalinesIn (excerpt) ---------------------------------------------");
            //int c = Math.Min(25, datalinesIn.Count);
            //for (int i = 0; i < c; i++)
            //    datalinesOut.Add(datalinesIn[i]);
            datalinesOut.Add("------------- generated output --------------------------------------------");

            // note: The number of stimuli (aka the stream length) is dynamic and known only at runtime, but there are a few meta parameters 
            // which are deterministic. The deterministic values are placed at fixed positions in the output list, and
            // are intended to be retrieved by the caller in a position-based manner.
            // The (dynamic) stimuli and related items follow the deterministic values and are intended to be retrieved in a 
            // record-key-based manner.

            // deterministic, positional udf-values:
            datalinesOut.Add(avRecallValue.ToString());
            datalinesOut.Add(basePathAud);
            datalinesOut.Add(basePathVid);


            // non-deterministic records start here (record-key based, non-positional) 
            // record-keys used:
            //   s: stimulus items audio, visual
            //   r: rel. paths for subsequent audio and/or visual items


            for (int i = avRecallValue; i < streamLenNet; i++)  // first elts are dummies to handle edges gracefully
            {
                #region check for updates to rel. path(s)
                string relPathLineOut = "r";  // init

                // check for updated rel. paths
                //if (pics[i - 1].RelPath != pics[i - 1].RelPath) newPathVid = pics[i].RelPath;
                if (pics[i].RelPath != pics[i - 1].RelPath) relPathLineOut += "; v:" + pics[i].RelPath;
                if (snds[i].RelPath != snds[i - 1].RelPath) relPathLineOut += "; a:" + snds[i].RelPath;

                if (relPathLineOut.Length > 1)  // if more than init
                    datalinesOut.Add(relPathLineOut);
                #endregion check for updates to rel. path(s)

                #region always: output line for stimulus pair

                #region determine match code
                // TODO: refine target tests to include path
                bool isAudTarget = snds[i].FileName == snds[i - avRecallValue].FileName;
                bool isVidTarget = pics[i].FileName == pics[i - avRecallValue].FileName;

                string matchCode = "null";
                if (isAudTarget && isVidTarget)
                    matchCode = "av";
                else if (isAudTarget)
                    matchCode = "a";
                else if (isVidTarget)
                    matchCode = "v";
                #endregion determine match code

                //datalinesOut.Add("s; " + "samplesndfile" + "; " + pics[i].FileName + "; " + matchCode);
                datalinesOut.Add("s; " + snds[i].FileName + "; " + pics[i].FileName + "; " + matchCode);

                #endregion always: output line for stimulus pair
            }


            #region [commented] sample hardcoded output lines
            //datalinesOut.Add("s; c; apple; null");     // cnt/s/kids/c.mp3 cnt/p/kids/apple.jpg

            //datalinesOut.Add("r; a:altLetters/");      // _rel path for subsequent audio
            //datalinesOut.Add("s; c; golfclub; a");     // cnt/s/kids/altLetters/c.mp3 cnt/p/kids/golfclub.jpg
            //datalinesOut.Add("s; c; dog; a");          // cnt/s/kids/altLetters/c.mp3 cnt/p/kids/dog.jpg

            //datalinesOut.Add("r; a:otherLetters/");    // _rel path for subsequent audio
            //datalinesOut.Add("s; t; golfclub; v");     // cnt/s/kids/otherLetters/t.mp3 cnt/p/kids/golfclub.jpg

            //datalinesOut.Add("r; a:");                 // reset audio path with empty rel path
            //datalinesOut.Add("s; r; elephant; null");  // cnt/s/kids/r.mp3 cnt/p/kids/elephant.jpg

            //datalinesOut.Add("r; a:altLetters/; v:altPics/");      // _rel paths for both audio and visual
            //datalinesOut.Add("s; e; sportscar; null");  // cnt/s/kids/altLetters/e.mp3 cnt/p/kids/altPics/sportscar.jpg

            //datalinesOut.Add("r; v:");  // reset visual path (but not audio path)
            //datalinesOut.Add("s; f; locomotive; null");  // cnt/s/kids/altLetters/f.mp3 cnt/p/kids/locomotive.jpg
            #endregion sample hardcoded output lines


            #endregion output as needed by h5-oms




            #region [2012-07-04] doc for CAlvin: syntax of retvals[]

            //#region output as needed by h5-oms
            //// note: The number of stimuli (aka the stream length) is dynamic and known only at runtime, but there are a few meta parameters 
            //// which are deterministic. The deterministic values are placed at fixed positions in the output list, and
            //// are intended to be retrieved by the caller in a position-based manner.
            //// The (dynamic) stimuli and related items follow the deterministic values and are intended to be retrieved in a 
            //// record-key-based manner.

            //// deterministic, positional udf-values:
            // 
            // 2                                // av-recall value (aka n-value)
            // cnt/s/kids/                      // base path for snd files (audio)
            // cnt/p/kids/                      // base path for img files (visual)

            //// non-deterministic records start here (record-key based, non-positional) 
            //// record-keys used:
            ////   s: stimulus items audio, visual
            ////      format: positional;
            ////              s; <snd-file-name>; <img-file-name>; match-code
            ////              match-codes: {null,a,v,av}     
            ////   r: rel. paths for subsequent audio and/or visual items
            ////      format: semi-colon separated list of non-positional keyed tokens, all optional:
            ////              a:<rel-path for snd files>
            ////              v:<rel-path for img files>
            //

            // s; c; apple; null                // cnt/s/kids/c.mp3 cnt/p/kids/apple.jpg

            // r; a:altLetters/                 // _rel path for subsequent audio
            // s; c; golfclub; a                // cnt/s/kids/altLetters/c.mp3 cnt/p/kids/golfclub.jpg
            // s; c; dog; a                     // cnt/s/kids/altLetters/c.mp3 cnt/p/kids/dog.jpg

            // r; a:otherLetters/               // _rel path for subsequent audio
            // s; t; golfclub; v                // cnt/s/kids/otherLetters/t.mp3 cnt/p/kids/golfclub.jpg

            // r; a:                            // reset audio path with empty rel path
            // s; r; elephant; null             // cnt/s/kids/r.mp3 cnt/p/kids/elephant.jpg

            // r; a:altLetters/; v:altPics/     // _rel paths for both audio and visual
            // s; e; sportscar; null            // cnt/s/kids/altLetters/e.mp3 cnt/p/kids/altPics/sportscar.jpg

            // r; v:                            // reset visual path (but not audio path)
            // s; f; locomotive; null           // cnt/s/kids/altLetters/f.mp3 cnt/p/kids/locomotive.jpg


            #endregion

            return datalinesOut;
        }  // GetData()

        public static string GetDataAsJSON_v00(int gameLevel, string unitID)
        {
            // ALERT: for now we use class defaults and hard-coded sample values. This is a POC for using json for the 
            // websvrc payload format. See below GetData(int uid) for more-complete algorithm which reads in UDF etc.

            #region algorithm:
            // - determine path to UDF (use gameLevel, unitID)
            // - re-use code from GetData() to read in file, but now store it in instance of UnitConfig
            // - transfer data from unitConfig to json structure (instance of NBackUnitConfig)
            #endregion

            Json json = new Json();  // json structure for unit-data

            Json.NBackUnitConfig unitConfig = json.nbackConfig;


            #region build list of stimuli (for now hardcoded values)
            // dev: hardcoded sample data
            List<Json.NBackStimulusPair> stimulist = new List<Json.NBackStimulusPair>();



            stimulist.Add(new Json.NBackStimulusPair()
            {
                a = new Json.NBackStimulus() { IsTrg = true, AssetRelPath = "c.mp3" },
                v = new Json.NBackStimulus() { IsTrg = false, AssetRelPath = "dog.jpg" }
            });
            stimulist.Add(new Json.NBackStimulusPair()
            {
                a = new Json.NBackStimulus() { IsTrg = false, AssetRelPath = "altLetters/r.mp3" },
                v = new Json.NBackStimulus() { IsTrg = true, AssetRelPath = "altPics/car.png" }
            });
            stimulist.Add(new Json.NBackStimulusPair()
            {
                a = new Json.NBackStimulus() { IsTrg = true, AssetRelPath = "altLetters/d.mp3" },
                v = new Json.NBackStimulus() { IsTrg = true, AssetRelPath = "zebra.jpg" }
            });
            stimulist.Add(new Json.NBackStimulusPair()
            {
                a = new Json.NBackStimulus() { IsTrg = false, AssetRelPath = "instruments/oboe.mp3" },
                v = new Json.NBackStimulus() { IsTrg = false, AssetRelPath = "battleship.jpg" }
            });
            #endregion build list of stimuli (for now hardcoded values)

            unitConfig.stimuli = stimulist.ToArray();

            string jsonstr = System.Web.Helpers.Json.Encode(new Json.NBackUnitConfigWrapper() { unit = unitConfig });

            return jsonstr;
        }

        #endregion old stuff

    }  // class UnitConfiguration

}

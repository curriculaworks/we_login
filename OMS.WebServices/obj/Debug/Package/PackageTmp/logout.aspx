﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CurriculaWorks.Master" AutoEventWireup="true" CodeBehind="logout.aspx.cs" Inherits="OMS.WebServices.logout1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<tr>
    <td>
	        <table align="center" width="430" bgcolor="Black">
		        <tr bgcolor="Black" > 
			        <td align="center" >
				        <br/>
				        <asp:Label id="loggedout" runat="server" ForeColor="FireBrick" 
                            Text="You are now logged out. Thank you for using OMS."></asp:Label>
                    </td>
			       <%-- <td align="left" bgcolor="Black"></td>--%>
		        </tr>
            </table>
    </td>
</tr>
</asp:Content>

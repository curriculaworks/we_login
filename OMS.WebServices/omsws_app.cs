﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using CW.Tools;
using OMS.Tools;

namespace OMS.WebServices
{
    public partial class oms_ws : System.Web.Services.WebService
    {
        #region WebMethods > prod > oms-app ===========================================================================================

        #region OMSInit() =======================================================================================

        [WebMethod]  // omsws
        public string OMSInit(int tid)
        {
            GetWSConfig();

            string formatLogData = "";
            OMS_utils omswsLog = new OMS_utils();
            #region log
            if (isLogEnabled)
            {
                //** Write to log               
                formatLogData = "OMSInit Input Values:  \r\n";
                formatLogData += "tid = " + tid;
                omswsLog.log(formatLogData);
            }
            #endregion log

            if (!isCallDB)
            {
                return System.Web.Helpers.Json.Encode(new OMSSessionWrapper() { oms_session = new OMSSession() });
            }

            #region retrieve session data (user settings and stored state) from db
            string[] retvals = new string[26];

            SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd = new SqlCommand("oms_cw_auth", SQLConn);
            cmd.CommandType = CommandType.StoredProcedure;

            #region set up SqlParameters and execute query
            SqlParameter Param;
            Param = cmd.Parameters.Add("@tidinput", SqlDbType.Int);
            Param.Value = tid;
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@tid", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@f_name", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@l_name", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@currentBadge_id", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@qtyBadge0", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@qtyBadge1", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@qtyBadge2", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@qtyBadge3", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@level_idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@group_idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@unit_idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@unit_id", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@intel_id", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@top_Badge_Cnt", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@cur_Mission_Node_Idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@mapPoint_id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@missionKey", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@storyline_id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@timestampOfBadge", SqlDbType.DateTime);
            Param.Direction = ParameterDirection.Output;
            //
            Param = cmd.Parameters.Add("@training_power", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@highest_n", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@login_time", SqlDbType.DateTime);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@app_starttime", SqlDbType.DateTime);   // note: sql procedure oms_cw_auth records current timestamp and stores in current table
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@recentUnit_ts", SqlDbType.DateTime);
            Param.Direction = ParameterDirection.Output;

            SQLConn.Open();
            cmd.ExecuteNonQuery();
            #endregion set up Sqlparams

            #region [old badge stuff]
            String currBadge = cmd.Parameters["@currentBadge_id"].Value.ToString();
            String qtyB0 = cmd.Parameters["@qtyBadge0"].Value.ToString();
            String qtyB1 = cmd.Parameters["@qtyBadge1"].Value.ToString();
            String qtyB2 = cmd.Parameters["@qtyBadge2"].Value.ToString();
            String qtyB3 = cmd.Parameters["@qtyBadge3"].Value.ToString();
            #endregion [old badge stuff]

            #region [commented] test1
            //DateTime tsOfBadge = Convert.ToDateTime(cmd.Parameters["@timestampOfBadge"].Value);
            //DateTime tsOfBadgeDate = tsOfBadge.Date;   //** get only date portion of the tsOfBadge
            //DateTime tsNow = DateTime.Now.Date;        //** get only date portion of current time
            //int compareTSResult = DateTime.Compare(tsOfBadgeDate, tsNow);

            //if (compareTSResult != 0)     //** if (badges expired)
            //{
            //    currBadge = "-1";         //** Set currentBadgeID = -1 and all earned qty's to 0;
            //    qtyB0 = "0";
            //    qtyB1 = "0";
            //    qtyB2 = "0";
            //    qtyB3 = "0";
            //}
            #endregion test1


            #region [update: OMS_utils now avail.] [commented because we don't have OMS_utils available] calculate applicable param values
            //** Determine Training Level  
            int trainingLevel;

            if (cmd.Parameters["@recentUnit_ts"].Value.ToString() != "")
            {
                DateTime l_recentUnit_ts = (DateTime)cmd.Parameters["@recentUnit_ts"].Value;
                OMS_utils omsTrainingLevel = new OMS_utils();
                omsTrainingLevel.determineTrainingLevel(l_recentUnit_ts);
                trainingLevel = omsTrainingLevel.trainLevel;
            }
            else
                trainingLevel = 0;

            //** Calculate Average Accuracy
            double avgAccuracy;
            OMS_utils omsCalAvgAccuracy = new OMS_utils();
            omsCalAvgAccuracy.calculateAvgAccuracy(tid);
            avgAccuracy = omsCalAvgAccuracy.avgAccuracy;

            //** Calculate Total Training Time
            int totalTrainingTime;
            OMS_utils omsTotalTrainingTime = new OMS_utils();
            omsTotalTrainingTime.calculateTotalTrainingTime(tid);
            totalTrainingTime = omsTotalTrainingTime.totalTrainingTime;

            //** Calculate Average Training Time
            int avgTrainingTime;
            OMS_utils omsAvgTrainingTime = new OMS_utils();
            omsAvgTrainingTime.calculateAvgTrainingTime(tid);
            avgTrainingTime = omsAvgTrainingTime.avgTrainingTime;
            //------------------
            #endregion [commented because we don't have OMS_utils available] calculate applicable param values


            #region output to intermediate datastructure dbValues
            OMS.Tools.NamedObjects dbValues = new Tools.NamedObjects();

            dbValues.Add("userdisplayname_first", cmd.Parameters["@f_name"].Value.ToString());
            dbValues.Add("userdisplayname_last", cmd.Parameters["@l_name"].Value.ToString());

            #region badges

            #region test exception
            //string strval = "";
            //int intval = 987654;

            //dbValues.Add("intval", intval);
            //strval = cmd.Parameters["@currentBadge_id"].Value.ToString();  // = "-1"
            //intval = int.Parse(cmd.Parameters["@currentBadge_id"].Value.ToString());  // works
            ////intval = (int)cmd.Parameters["@currentBadge_id"].Value;  // throws exception (although casting to int works elsewhere; does it not like neg. vals?? [update: yep, apparently not :P]

            //dbValues.Add("strcurrent_badge_id", strval);
            //dbValues.Add("intcurrent_badge_id", intval);
            #endregion test exception

            dbValues.Add("current_badge_id", int.Parse(cmd.Parameters["@currentBadge_id"].Value.ToString()));  // see above "test"

            int badgeTypec = 4;
            int[] earnedBadgeCounts = new int[badgeTypec];
            for (int i = 0; i < badgeTypec; i++)
            {
                string badgeCounterNameDB = "@qtyBadge" + i;
                string badgeCounterNameJSON = "badge_qty_" + i;
                dbValues.Add(badgeCounterNameJSON, int.Parse(cmd.Parameters[badgeCounterNameDB].Value.ToString()));
            }
            #endregion

            dbValues.Add("way_point_idx", int.Parse(cmd.Parameters["@cur_Mission_Node_Idx"].Value.ToString()));

            #region user-performance to date
            dbValues.Add("perf_training_power", (int)cmd.Parameters["@training_power"].Value);
            dbValues.Add("perf_training_level", trainingLevel);  // int

            dbValues.Add("perf_total_training_time", totalTrainingTime);  // int
            dbValues.Add("perf_avg_training_time", avgTrainingTime);  // int

            dbValues.Add("perf_avg_accuracy", avgAccuracy);  // double

            dbValues.Add("perf_highest_n", (int)cmd.Parameters["@highest_n"].Value);
            dbValues.Add("perf_current_game_level", (int)cmd.Parameters["@level_idx"].Value);
            #endregion user-performance to date


            DateTime appStartTime = (DateTime)cmd.Parameters["@app_starttime"].Value;
            //DateTime loginTime = (DateTime)cmd.Parameters["@login_time"].Value;
            brkpnt = 123;  // OMSInit()

            #endregion output to intermediate datastructure dbValues

            #region [obsolete] output to retvals[]: values still to-be-added to class OMSSession (see below) <-- TODO
            #region
            #endregion
            retvals[0] = cmd.Parameters["@tid"].Value.ToString();                       // omitted in dbValues
            retvals[1] = cmd.Parameters["@f_name"].Value.ToString();
            retvals[2] = cmd.Parameters["@l_name"].Value.ToString();
            retvals[3] = currBadge;
            retvals[4] = qtyB0;
            retvals[5] = qtyB1;
            retvals[6] = qtyB2;
            retvals[7] = qtyB3;
            retvals[8] = cmd.Parameters["@level_idx"].Value.ToString();
            retvals[9] = cmd.Parameters["@group_idx"].Value.ToString();                 // omitted in dbValues
            retvals[10] = cmd.Parameters["@unit_idx"].Value.ToString();                 // omitted in dbValues
            retvals[11] = cmd.Parameters["@unit_id"].Value.ToString();                  // omitted in dbValues
            retvals[12] = cmd.Parameters["@intel_id"].Value.ToString();                 // omitted in dbValues
            retvals[13] = cmd.Parameters["@top_Badge_Cnt"].Value.ToString();            // omitted in dbValues
            retvals[14] = cmd.Parameters["@cur_Mission_Node_Idx"].Value.ToString();
            retvals[15] = cmd.Parameters["@mapPoint_id"].Value.ToString();              // omitted in dbValues
            retvals[16] = cmd.Parameters["@missionKey"].Value.ToString();               // omitted in dbValues
            retvals[17] = cmd.Parameters["@storyline_id"].Value.ToString();             // omitted in dbValues
            retvals[18] = cmd.Parameters["@training_power"].Value.ToString();
            retvals[19] = Convert.ToString(trainingLevel);
            retvals[20] = cmd.Parameters["@highest_n"].Value.ToString();
            retvals[21] = Convert.ToString(avgAccuracy);
            retvals[22] = Convert.ToString(totalTrainingTime);
            retvals[23] = Convert.ToString(avgTrainingTime);
            retvals[24] = cmd.Parameters["@app_starttime"].Value.ToString();            // omitted in dbValues
            retvals[25] = cmd.Parameters["@recentUnit_ts"].Value.ToString();            // omitted in dbValues
            #endregion output to retvals[]

            SQLConn.Close();
            #endregion retrieve session data

            #region enter retrieved user data into json data structure
            OMSSession session = new OMSSession();

            session.user_name.first = dbValues["userdisplayname_first"] as string;
            session.user_name.last = dbValues["userdisplayname_last"] as string;

            #region session.earned_badges
            session.earned_badges.current_badge_id = (int)dbValues["current_badge_id"];
            {
                int j = 0;
                session.earned_badges.badge_qty_0 = (int)dbValues["badge_qty_" + j++];
                session.earned_badges.badge_qty_1 = (int)dbValues["badge_qty_" + j++];
                session.earned_badges.badge_qty_2 = (int)dbValues["badge_qty_" + j++];
                session.earned_badges.badge_qty_3 = (int)dbValues["badge_qty_" + j++];
            }
            #endregion session.earned_badges

            session.mission.way_point_id = (int)dbValues["way_point_idx"];

            #region session.performance
            session.performance.training_power = (int)dbValues["perf_training_power"];
            session.performance.training_level = (int)dbValues["perf_training_level"];

            session.performance.total_training_time = (int)dbValues["perf_total_training_time"];
            session.performance.avg_training_time = (int)dbValues["perf_avg_training_time"];

            session.performance.avg_accuracy = (double)dbValues["perf_avg_accuracy"];

            session.performance.highest_n = (int)dbValues["perf_highest_n"];
            session.performance.current_game_level = (int)dbValues["perf_current_game_level"];

            #endregion session.performance

            #endregion enter retrieved user data into json data structure

            string jsonstr = System.Web.Helpers.Json.Encode(new OMSSessionWrapper() { oms_session = session });

            #region log
            if (isLogEnabled)
            {
                //** Write to log               
                formatLogData = "OMSInit Return:  \r\n";
                formatLogData += jsonstr;
                omswsLog.log(formatLogData);
            }
            #endregion log

            return jsonstr;
        }  // OMSInit()
        #endregion OMSInit() =======================================================================================


        #region OMSGetUnitData() =======================================================================================

        [WebMethod]  // omsws
        public string OMSGetUnitData(int tid)
        {
            GetWSConfig();

            string formatLogData = "";
            OMS_utils omswsLog = new OMS_utils();
            #region log
            if (isLogEnabled)
            {
                //** Write to log               
                formatLogData = "OMSGetUnitData Input Values:  \r\n";
                formatLogData += "tid = " + tid;
                omswsLog.log(formatLogData);
            }
            #endregion log


            #region if (!isCallDB)
            if (!isCallDB)
            {
                //// for dev: hardcoded values
                Random rand = new Random();
                OMS.Tools.UDF.UnitConfiguration.GameIDs devgameID = Tools.UDF.UnitConfiguration.GameIDs.NBackKids;
                int devgameLevel = rand.Next(1, 7); // corresponds to available subset of level-folders used for dev
                int devnode = rand.Next(1, 101);  // currently using all nodes for subset of levels
                string devunitID = "L001G001N011";
                devunitID = "L" + devgameLevel.ToString("D03") + "G001N" + devnode.ToString("D03");
                return OMS.Tools.UDF.UnitConfiguration.GetDataAsJSON(gameID: devgameID, gameLevel: devgameLevel, unitID: devunitID);
            }
            #endregion


            #region retrieve session data (user settings and stored state) from db
            // TODO: for now this is copied from OMSInit(). It needs to be refactored into a separate module.

            SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd = new SqlCommand("oms_cw_auth", SQLConn);
            cmd.CommandType = CommandType.StoredProcedure;

            #region set up SqlParameters and execute query
            SqlParameter Param;
            Param = cmd.Parameters.Add("@tidinput", SqlDbType.Int);
            Param.Value = tid;
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@tid", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@f_name", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@l_name", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@currentBadge_id", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@qtyBadge0", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@qtyBadge1", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@qtyBadge2", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@qtyBadge3", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@level_idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@group_idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@unit_idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@unit_id", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@intel_id", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@top_Badge_Cnt", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@cur_Mission_Node_Idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@mapPoint_id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@missionKey", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@storyline_id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@timestampOfBadge", SqlDbType.DateTime);
            Param.Direction = ParameterDirection.Output;
            //
            Param = cmd.Parameters.Add("@training_power", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@highest_n", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@login_time", SqlDbType.DateTime);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@app_starttime", SqlDbType.DateTime);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@recentUnit_ts", SqlDbType.DateTime);
            Param.Direction = ParameterDirection.Output;

            SQLConn.Open();
            cmd.ExecuteNonQuery();
            #endregion set up Sqlparams

            #region output to intermediate datastructure dbValues
            //OMS.Tools.NamedObjects dbValues = new Tools.NamedObjects();

            #region user's current location in curriculum (n-back) (determines UDF file name)
            //dbValues.Add("current_game_level", (int)cmd.Parameters["@level_idx"].Value);

            int gameLevel = (int)cmd.Parameters["@level_idx"].Value;
            int gameGroup = (int)cmd.Parameters["@group_idx"].Value;
            int gameNode = (int)cmd.Parameters["@unit_idx"].Value;
            #endregion

            #endregion output to intermediate datastructure dbValues

            SQLConn.Close();
            #endregion retrieve session data

            #region determine gameID for user  TODO: get range from external config
            #region [commented, tmp for dev]
            //OMS.Tools.UDF.UnitConfiguration.GameIDs gameID = Tools.UDF.UnitConfiguration.GameIDs.Default;
            //// note: tid's 104/5 == kids00{0,1};
            ////       tid's 306-356 arbitrarily reserved for future kids accounts (omega199==305)
            //// ALERT/TODO: this is temporary; should come from external config
            //if
            //(
            //       (tid >= 104 && tid <= 105)
            //    || (tid >= 306 && tid <= 356)
            //)
            //    gameID = Tools.UDF.UnitConfiguration.GameIDs.NBackKids;
            //else
            //    gameID = Tools.UDF.UnitConfiguration.GameIDs.NBackNavy;
            #endregion

            OMSConfig.OMSUserDescr user = OMSConfig.GetUserDescr(tid: tid);
            OMS.Tools.UDF.UnitConfiguration.GameIDs gameID = Tools.UDF.UnitConfiguration.GameIDs.Default;
            if (!cwtrue) { }
            else if (user.NBackTheme == 0) gameID = Tools.UDF.UnitConfiguration.GameIDs.NBackNavy;
            else if (user.NBackTheme == 1) gameID = Tools.UDF.UnitConfiguration.GameIDs.NBackKids;
            //brkpnt = 368;
            #endregion determine gameID for user



            string unitID = "L001G001N001";  // example
            unitID =
                "L" + gameLevel.ToString("D03") +
                "G" + gameGroup.ToString("D03") +
                "N" + gameNode.ToString("D03");



            // - read in udf for unit-id
            // - parse and process to build client-conformant output
            //return OMS.Tools.UDF.UnitConfiguration.GetDataAsJSON(gameLevel: gameLevel, unitID: unitID);

            //DEV: uncomment for debug (and comment above return statement)
            string jsonstr = OMS.Tools.UDF.UnitConfiguration.GetDataAsJSON(gameID: gameID, gameLevel: gameLevel, unitID: unitID);

            #region log
            if (isLogEnabled)
            {
                //** Write to log               
                formatLogData = "OMSGetUnitData Return:  \r\n";
                formatLogData += jsonstr;
                omswsLog.log(formatLogData);
            }
            #endregion log

            return jsonstr;

        }  // OMSGetUnitData()
        #endregion OMSGetUnitData() =======================================================================================


        #region OMSProcessUserPerformanceData() =======================================================================================
        [WebMethod]  // omsws
        public string OMSProcessUserPerformanceData(string json_performance)
        {
            GetWSConfig();

            // preprocess arg string to remove layers of encoding: html (str) > json (str) > internal json-ready datastructure

            // note: arg string is http-escaped by SOAP client; html-decode reveals the underlying json-encoded string (which we json-decode below)
            string jsonstrhtmlescaped = json_performance;
            //string jsonstrhtmlescaped = @"{&quot;accuracy_aud&quot;: 0.664341516834833, &quot;reaction_time_overall&quot;: 700, &quot;accuracy_overall&quot;: 0.64363677151003906, &quot;tid&quot;: 2, &quot;reaction_time_vis&quot;: 700, &quot;accuracy_vis&quot;: 0.62293202618524501, &quot;unit_id&quot;: &quot;L004G001N067&quot;, &quot;reaction_time_aud&quot;: 700}";
            // sample for dev (paste into oms_ws.asmx)
            //  {&quot;tid&quot;: 2, &quot;accuracy_aud&quot;: 0.664341516834833, &quot;reaction_time_overall&quot;: 700, &quot;accuracy_overall&quot;: 0.64363677151003906, &quot;reaction_time_vis&quot;: 700, &quot;accuracy_vis&quot;: 0.62293202618524501, &quot;unit_id&quot;: &quot;L004G001N067&quot;, &quot;reaction_time_aud&quot;: 700}
            string json_performance_unescaped = HttpUtility.HtmlDecode(jsonstrhtmlescaped);

            string formatLogData = "";
            OMS_utils omswsLog = new OMS_utils();
            #region log
            if (isLogEnabled)
            {
                //** Write to log               
                formatLogData = "OMSProcessUserPerformanceData Input Values:  \r\n";
                formatLogData += "|" + json_performance + "|";
                formatLogData += "\r\n\r\n arg str unescaped:\r\n";
                formatLogData += "|" + json_performance_unescaped + "|"; ;
                //formatLogData += "level_idx = " + objunit.level_idx + ", group_idx = " + objunit.group_idx + ", unit_idx = " + objunit.unit_idx;
                //formatLogData += ", unit_id = " + objunit.unit_id;
                omswsLog.log(formatLogData);
            }
            #endregion log


            #region if (!isCallDB)
            if (!isCallDB)
            {
                // ALERT: [2013-03-27: this is now obsolete; see def of MSUserProgressReport and production return (with db)

                //// for dev: hardcoded values
                Random rand = new Random();
                int gameLevel = rand.Next(1, 7); // corresponds to available subset of level-folders used for dev
                int node = rand.Next(1, 101);  // currently using all nodes for subset of levels
                string devunitID = "L001G001N011";
                devunitID = "L" + gameLevel.ToString("D03") + "G001N" + node.ToString("D03");

                return System.Web.Helpers.Json.Encode(new OMSUserProgressReport() { oms_rewards = new OMSNewRewards() });  // sym. for dev
            }
            #endregion

            #region [commented] poc Json.Decode
            //UserPerformanceOnLastNBackWrapper perfWrap = new UserPerformanceOnLastNBackWrapper();
            //string jstr = System.Web.Helpers.Json.Encode(perfWrap);
            //var varPerfWrap = System.Web.Helpers.Json.Decode<UserPerformanceOnLastNBackWrapper>(jstr);
            //varPerfWrap = System.Web.Helpers.Json.Decode(jstr, typeof(UserPerformanceOnLastNBackWrapper));
            ////var varPerfWrap = System.Web.Helpers.Json.Decode(jstr);
            //UserPerformanceOnLastNBack perf = varPerfWrap.performance;
            ////perfWrap = System.Web.Helpers.Json.Decode(jstr);
            ////perfWrap = System.Web.Helpers.Json.Decode(jstr) as UserPerformanceOnLastNBackWrapper;
            ////perfWrap = (UserPerformanceOnLastNBackWrapper)System.Web.Helpers.Json.Decode(jstr);
            //brkpnt++;
            #endregion [commented] poc Json.Decode


            #region decode arg
            // TODO: update error-checking
            UserPerformanceOnLastNBack performance = System.Web.Helpers.Json.Decode<UserPerformanceOnLastNBack>(json_performance_unescaped);
            if (performance == null)
            {
                // handle error
            }
            brkpnt = 0;
            #endregion decode arg

            #region [old] decode arg
            //// TODO: update error-checking
            //UserPerformanceOnLastNBackWrapper perfWrapper = System.Web.Helpers.Json.Decode<UserPerformanceOnLastNBackWrapper>(json_performance);
            //UserPerformanceOnLastNBack performance = new UserPerformanceOnLastNBack();  // init to keep compiler happy

            //if (perfWrapper == null)
            //{
            //    // handle error
            //}
            //else
            //{
            //    performance = perfWrapper.performance;
            //}
            //brkpnt++;
            #endregion [old] decode arg

            #region assign local vars
            //{
            //    public int tid = -1;  // OMS user id; values \in [1,*]
            //    public string unit_id = "";  // ex. "L001G001N011"
            //
            //    public double accuracy_overall = 0.0;  // value \in [0.0,1.0] ex.: 0.72,  
            //    public double accuracy_aud = 0.0;      // ex.  0.74,      
            //    public double accuracy_vis = 0.0;      // ex.  0.68,      

            //    public int reaction_time_overall = 0;  // in milliseconds, ex.: 500
            //    public int reaction_time_aud = 0;      // ex.  530  
            //    public int reaction_time_vis = 0;      // ex.  470
            //}
            int tid = performance.tid;
            string unitID = performance.unit_id;   // ex. "L001G002N011"
            //      0123456789_1
            int gameLevelIdx, gameGroupIdx = 1, gameNodeIdx;  // note: for now group == 1 always

            gameLevelIdx = int.Parse(unitID.Substring(1, 3));
            gameNodeIdx = int.Parse(unitID.Substring(1, 3));

            // note: obviously this is a complete cheat at this point; we're using stimulus_onset_interval * avg_streamlen
            // TODO: fix it:
            //   - maybe store interal and streamlen (and other unit params) in current_table
            //   - AND/OR include gameDuration in class UserPerformanceOnLastNBack 
            // UPDATE: even worse, onset value now actually comes from config file
            int gameDuration = 3000 * 60;   // 1800


            bool isWayPointEarned = false;

            #endregion assign local vars


            #region perform evaluation, update user session in db
            OMSUserProgressReport omsProgressRpt = new OMSUserProgressReport();
            OMSNewRewards oms_rewards = omsProgressRpt.oms_rewards;

            #region try  // talk to db
            try  // talk to db
            {
                #region init otuput variables
                double overallAccuracy = performance.accuracy_overall;

                int nextLevel_IDx = 0;
                int nextGroupIDx = 0;
                int nextUnitIDx = 0;
                string nextUnitID = string.Empty;

                string earnedBadgeID = "-1";
                int qtyBadge0 = 0;
                int qtyBadge1 = 0;
                int qtyBadge2 = 0;
                int qtyBadge3 = 0;
                #endregion

                #region assign earned badge [i.e. exactly one of the above counters is set to 1 to indicate which badge is being earned]
                //**   New earnedBadge logic from Dee Dee 02/2/2012
                //**    1 =     < 65%
                //**    2 =     >= 66% to 74%
                //**    3 =     >= 75% to 84%
                //**    4 =     >= 85%

                //** Assigning Earned Badges
                if (overallAccuracy >= 0.85)
                {
                    earnedBadgeID = "3";
                    qtyBadge3 = 1;
                }
                else if ((overallAccuracy >= 0.75) && (overallAccuracy < 0.85))
                {
                    earnedBadgeID = "2";
                    qtyBadge2 = 1;
                }
                else if ((overallAccuracy >= 0.65) && (overallAccuracy < 0.74))
                {
                    earnedBadgeID = "1";
                    qtyBadge1 = 1;
                }
                else if (overallAccuracy < 0.65)
                {
                    earnedBadgeID = "0";
                    qtyBadge0 = 1;
                }
                #endregion

                //** Call branching logic using passed-in parameter "Overall Accuracy"
                #region branching logic
                string branching_SP;

                if (overallAccuracy >= 0.85)
                {
                    //** Call stored procedure  oms_cw_Branch_Up_A_Level
                    branching_SP = "oms_cw_Branch_Up_A_Level";

                    OMS_utils omsBranching = new OMS_utils();
                    omsBranching.branchingLogic(gameLevelIdx, gameGroupIdx, gameNodeIdx, unitID, branching_SP);

                    nextLevel_IDx = omsBranching.nLevel_idx;
                    nextGroupIDx = omsBranching.nGroupIDx;
                    nextUnitIDx = omsBranching.nUnitIDx;
                    nextUnitID = omsBranching.nUnitID;
                }
                else if (overallAccuracy >= 0.75)
                {
                    //** Call stored procedure  oms_cw_Branch_Current_Level_Next_Group
                    branching_SP = "oms_cw_Branch_Current_Level_Next_Group";

                    OMS_utils omsBranching = new OMS_utils();
                    omsBranching.branchingLogic(gameLevelIdx, gameGroupIdx, gameNodeIdx, unitID, branching_SP);

                    nextLevel_IDx = omsBranching.nLevel_idx;
                    nextGroupIDx = omsBranching.nGroupIDx;
                    nextUnitIDx = omsBranching.nUnitIDx;
                    nextUnitID = omsBranching.nUnitID;
                }
                else if (overallAccuracy >= 0.65)
                {
                    //** Call stored procedure  oms_cw_Branch_Current_Level_Group_Next_Unit
                    branching_SP = "oms_cw_Branch_Current_Level_Group_Next_Unit";

                    OMS_utils omsBranching = new OMS_utils();
                    omsBranching.branchingLogic(gameLevelIdx, gameGroupIdx, gameNodeIdx, unitID, branching_SP);

                    nextLevel_IDx = omsBranching.nLevel_idx;
                    nextGroupIDx = omsBranching.nGroupIDx;
                    nextUnitIDx = omsBranching.nUnitIDx;
                    nextUnitID = omsBranching.nUnitID;
                }
                else if (overallAccuracy < 0.65)
                {
                    //** Call stored procedure  oms_cw_Branch_Back-A_Level
                    branching_SP = "oms_cw_Branch_Back_A_Level";

                    OMS_utils omsBranching = new OMS_utils();
                    omsBranching.branchingLogic(gameLevelIdx, gameGroupIdx, gameNodeIdx, unitID, branching_SP);

                    nextLevel_IDx = omsBranching.nLevel_idx;
                    nextGroupIDx = omsBranching.nGroupIDx;
                    nextUnitIDx = omsBranching.nUnitIDx;
                    nextUnitID = omsBranching.nUnitID;
                }
                #endregion  invoke omsBranching.branchingLogic()

                //**---------------------------------- Call oms_retrieveCurrentTable ----------------------------------
                #region oms_retrieveCurrentTable

                SqlConnection SQLConn5 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
                SqlCommand cmd5 = new SqlCommand("oms_retrieveCurrentTable", SQLConn5);
                cmd5.CommandType = CommandType.StoredProcedure;

                SqlParameter Param5;
                Param5 = cmd5.Parameters.Add("@tid", SqlDbType.Int);
                Param5.Value = tid;
                Param5.Direction = ParameterDirection.Input;
                Param5 = cmd5.Parameters.Add("@top_Badge_Cnt", SqlDbType.Int);
                Param5.Direction = ParameterDirection.Output;
                Param5 = cmd5.Parameters.Add("@cur_Mission_Node_Idx", SqlDbType.Int);
                Param5.Direction = ParameterDirection.Output;
                Param5 = cmd5.Parameters.Add("@app_starttime", SqlDbType.DateTime);
                Param5.Direction = ParameterDirection.Output;
                Param5 = cmd5.Parameters.Add("@highest_n", SqlDbType.Int);
                Param5.Direction = ParameterDirection.Output;
                Param5 = cmd5.Parameters.Add("@training_duration", SqlDbType.Int);
                Param5.Direction = ParameterDirection.Output;
                Param5 = cmd5.Parameters.Add("@recentUnit_ts", SqlDbType.DateTime);
                Param5.Direction = ParameterDirection.Output;

                SQLConn5.Open();
                cmd5.ExecuteNonQuery();

                // store retrieved db values in local datastructures (with some minor data processing)
                int topBadgeCnt = (int)cmd5.Parameters["@top_Badge_Cnt"].Value;
                int curMissioNode_Idx = (int)cmd5.Parameters["@cur_Mission_Node_Idx"].Value;
                // TODO: Q: why does appSTartTime seem to be set to currenttime on every call to processuserperformance? 
                //          It should only be set once - in OMSInit...
                //       A: because GetUnitData calls sp:cwauth
                //       Solution: revise mgmt of appStartTime in cwauth e.g. with flag isUpdateStartTime 
                DateTime appStartTime = new DateTime();
                try { appStartTime = (DateTime)cmd5.Parameters["@app_starttime"].Value; }
                catch (Exception e) { }
                int highestN = (int)cmd5.Parameters["@highest_n"].Value;
                int newTrainDuration = (int)cmd5.Parameters["@training_duration"].Value + gameDuration;
                DateTime l_recentUnit_ts = (DateTime)cmd5.Parameters["@recentUnit_ts"].Value;

                SQLConn5.Close();
                #endregion

                bool isConfirmContinue = false;  // special for for research study 2013-04; cf below


                #region ** Determine Training Power (session duration)
                //-------------------------
                int trainingPower, newTrainDuration_minute;

                if (newTrainDuration != 0)
                    newTrainDuration_minute = (int)Math.Ceiling(newTrainDuration / 60.0);
                else
                    newTrainDuration_minute = 0;

                if (newTrainDuration_minute < 15)
                    trainingPower = 0;                  //weak
                else if (newTrainDuration_minute < 25)
                    trainingPower = 1;                  //medium
                else //(newTrainDuration_minute >= 25)
                    trainingPower = 2;                  //strong
                //-------------------------
                #endregion ** Determine Training Power

                #region ** Determine Training Level (frequency)
                //-------------------------
                int trainingLevel;
                OMS_utils omsTrainingLevel = new OMS_utils();
                omsTrainingLevel.determineTrainingLevel(l_recentUnit_ts);
                trainingLevel = omsTrainingLevel.trainLevel;
                //-------------------------
                #endregion ** Determine Training Level

                #region ** Determine Current Mission Node (aka Way Point) AND isConfirmContinue

                #region way-pt logic from: [2012-05-03] ~\Dropbox\OMS\Requirements Docs\Requirements Document - OMS Motivators.docx
                {
                    // logic: (hardcoded for research study 2013-04)
                    //  - award next way-pt for each 3rd unit completed
                    //  - after 8 units, study is complete: user gets prompts, then is logged out
                    // ALERT: we missappropriate topBadgeCnt as our counter for unitscompleted
                    int wayPointInterval = 3;
                    int maxUnitsForSession = 8;  // and after one session, account is de facto blocked; align with login.aspx.cs
                    int WAYPTMAX = 100;  // safety catch to avoid integer overflow [TODO: consider getting current actual max. from a config file or db]
                    if (++topBadgeCnt <= maxUnitsForSession)
                    {
                        if ((topBadgeCnt) % wayPointInterval == 0)
                        {
                            isWayPointEarned = true;

                            // award next way-pt
                            if (curMissioNode_Idx < WAYPTMAX) curMissioNode_Idx++;

                            //// reset countdown
                            //topBadgeCnt = 0;
                        }
                        if (topBadgeCnt == maxUnitsForSession) isConfirmContinue = true;  // tell client app to prompt user
                    }
                    else  // session complete: lockdown; note: we should actually never get here because login.aspx.cs should detect
                    {
                        topBadgeCnt = maxUnitsForSession;
                        isWayPointEarned = false;
                        isConfirmContinue = true;
                    }

                    brkpnt = 368;

                    #region [commented] this was the code before hardcoding for Study
                    //int COUNTDOWNSTART = 3;
                    //int COUNTERMAX = COUNTDOWNSTART;
                    //int WAYPTMAX = 100;  // safety catch to avoid integer overflow [TODO: consider getting current actual max. from a config file or db]
                    //if (++topBadgeCnt >= COUNTERMAX)
                    //{
                    //    isWayPointEarned = true;

                    //    // award next way-pt
                    //    if (curMissioNode_Idx < WAYPTMAX) curMissioNode_Idx++;

                    //    // reset countdown
                    //    topBadgeCnt = 0;
                    //}
                    #endregion
                }
                #endregion way-pt logic from req. doc

                #region [commented] way-pt (aka Mission Node) logic from OMS-Navy
                ////-------------------------
                //if (earnedBadgeID == "3" || earnedBadgeID == "2")   //** 1st or 2nd place on the level
                //{
                //    if (++topBadgeCnt == 10)
                //    {
                //        //curMissioNode_Idx = curMissioNode_Idx < 51 ? curMissioNode_Idx++ : -1;
                //        if (curMissioNode_Idx < 51)
                //        {
                //            curMissioNode_Idx++;
                //            //earnedMissionNode_Idx = curMissioNode_Idx;
                //        }
                //        else
                //            curMissioNode_Idx = -1;

                //        topBadgeCnt = 0;
                //    }
                //}
                ////else
                ////    curMissioNode_Idx = -1;
                ////-------------------------
                #endregion [commented] way-pt (aka Mission Node) logic from OMS-Navy
                #endregion ** Determine Current Mission Node

                #region [commented] ** Determine whether to ask user for continue-confirmation based on session duration
                //// TODO: finish isConfirmContinue
                //// 30min after call to OMSInit we ask the user to confirm whether they want to continue (via modal pop-up in UI)
                //// note: appStartTime is recorded and stored during OMSInit; here fetched from current_table
                //int confirmThreshhold = 30;  // in minutes
                //DateTime now = DateTime.Now;
                //bool isConfirmContinue = (now - appStartTime).Minutes >= confirmThreshhold;
                //int devMinutes = (now - appStartTime).Minutes;
                //brkpnt = 368;
                #endregion ** Determine whether to ask user for continue-confirmation

                #region old oms_logins_log
                //**---------------------------------- Call oms_logins_log to retrieve data ------------------------------
                //**    Also, 1) Calculate total_training_time (in seconds); 2) Calculate avg_training_time (in seconds); 
                //**------------------------------------------------------------------------------------------------------
                //SqlConnection SQLConn6 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
                //SqlCommand cmd6 = new SqlCommand("oms_getLoginsInfo", SQLConn6);
                //cmd6.CommandType = CommandType.StoredProcedure;

                //SqlParameter Param6;
                //Param6 = cmd6.Parameters.Add("@tid", SqlDbType.Int);
                //Param6.Value = objunit.level_idx;
                //Param6 = cmd6.Parameters.Add("@login_time", SqlDbType.DateTime);
                //Param6.Direction = ParameterDirection.Output;

                //SQLConn6.Open();
                //cmd6.ExecuteNonQuery();

                //DateTime loginTime = (DateTime) cmd6.Parameters["@login_time"].Value;

                //DateTime tsOfBadge = Convert.ToDateTime(cmd.Parameters["@timestampOfBadge"].Value);
                //DateTime tsOfBadgeDate = tsOfBadge.Date;   //** get only date portion of the tsOfBadge
                //DateTime tsNow = DateTime.Now.Date;        //** get only date portion of current time
                //int compareTSResult = DateTime.Compare(tsOfBadgeDate, tsNow);

                //** Determine training power
                //---------------------------

                //DateTime tsNow = DateTime.Now;
                ////TimeSpan trainDuration = tsNow.Subtract(loginTime);
                //TimeSpan trainDuration = tsNow.Subtract(appStartTime);
                //int trainDuration_mins = trainDuration.Minutes;
                //int trainingPower;

                //if (trainDuration_mins < 15)
                //    trainingPower = 0;          //weak
                //else if (trainDuration_mins < 25)
                //    trainingPower = 1;          //medium
                //else //(trainDuration_mins > 25)
                //    trainingPower = 2;          //strong

                //SQLConn6.Close();
                #endregion

                //**------------------------------------------------------------------------------------------------------
                #region old branching logic
                //**---------------------------------- Call oms_cw_getnextunit (Branching logic) --------------------------
                //SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
                //SqlCommand cmd = new SqlCommand("oms_cw_getnextunit", SQLConn);
                //cmd.CommandType = CommandType.StoredProcedure;

                //SqlParameter Param;
                //Param = cmd.Parameters.Add("@currentLevel", SqlDbType.Int);
                //Param.Value = objunit.level;
                //Param.Direction = ParameterDirection.Input;
                //Param = cmd.Parameters.Add("@currentGroupId", SqlDbType.Int);
                //Param.Value = objunit.group_id;
                //Param.Direction = ParameterDirection.Input;
                //Param = cmd.Parameters.Add("@currentUnitId", SqlDbType.VarChar, 50);
                //Param.Value = objunit.unit_id;
                //Param.Direction = ParameterDirection.Input;
                //Param = cmd.Parameters.Add("@earnedBadgeID", SqlDbType.Int);
                //Param.Value = earnedBadgeID;
                //Param.Direction = ParameterDirection.Input;
                ////
                //Param = cmd.Parameters.Add("@level", SqlDbType.Int);
                //Param.Direction = ParameterDirection.Output;
                //Param = cmd.Parameters.Add("@group_id", SqlDbType.Int);
                //Param.Direction = ParameterDirection.Output;
                //Param = cmd.Parameters.Add("@unit_id", SqlDbType.VarChar, 50);
                //Param.Direction = ParameterDirection.Output;
                //Param = cmd.Parameters.Add("@qtyBadge0", SqlDbType.Int);
                //Param.Direction = ParameterDirection.Output;
                //Param = cmd.Parameters.Add("@qtyBadge1", SqlDbType.Int);
                //Param.Direction = ParameterDirection.Output;
                //Param = cmd.Parameters.Add("@qtyBadge2", SqlDbType.Int);
                //Param.Direction = ParameterDirection.Output;
                //Param = cmd.Parameters.Add("@qtyBadge3", SqlDbType.Int);
                //Param.Direction = ParameterDirection.Output;

                //SQLConn.Open();
                //cmd.ExecuteNonQuery();


                //int nextLevel = (int)cmd.Parameters["@level"].Value;
                //int nextGroupID = (int)cmd.Parameters["@group_id"].Value;
                //string nextUnitID = cmd.Parameters["@unit_id"].Value.ToString();
                //int qtyBadge0 = (int)cmd.Parameters["@qtyBadge0"].Value;
                //int qtyBadge1 = (int)cmd.Parameters["@qtyBadge1"].Value;
                //int qtyBadge2 = (int)cmd.Parameters["@qtyBadge2"].Value;
                //int qtyBadge3 = (int)cmd.Parameters["@qtyBadge3"].Value;

                //SQLConn.Close();
                #endregion branching

                //**---------------------------------- Call oms_getMissionProgressInfo ----------------------------------
                #region oms_getMissionProgressInfo

                SqlConnection SQLConn2 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
                SqlCommand cmd2 = new SqlCommand("oms_getMissionProgressInfo", SQLConn2);
                cmd2.CommandType = CommandType.StoredProcedure;

                SqlParameter Param2;
                Param2 = cmd2.Parameters.Add("@level_idx", SqlDbType.Int);
                Param2.Value = gameLevelIdx;
                Param2.Direction = ParameterDirection.Input;
                Param2 = cmd2.Parameters.Add("@group_idx", SqlDbType.Int);
                Param2.Value = gameGroupIdx;
                Param2.Direction = ParameterDirection.Input;
                Param2 = cmd2.Parameters.Add("@unit_idx", SqlDbType.Int);
                Param2.Value = gameNodeIdx;
                Param2.Direction = ParameterDirection.Input;
                Param2 = cmd2.Parameters.Add("@unit_id", SqlDbType.VarChar, 50);
                Param2.Value = unitID;
                Param2.Direction = ParameterDirection.Input;
                //
                Param2 = cmd2.Parameters.Add("@intel_id", SqlDbType.VarChar, 50);
                Param2.Direction = ParameterDirection.Output;
                Param2 = cmd2.Parameters.Add("@mapPoint_id", SqlDbType.Int);
                Param2.Direction = ParameterDirection.Output;

                SQLConn2.Open();
                cmd2.ExecuteNonQuery();

                string mapPoint_id = string.Empty;
                if ((cmd2.Parameters["@mapPoint_id"].Value.ToString() != "") && (cmd2.Parameters["@mapPoint_id"].Value.ToString() != null))
                    mapPoint_id = cmd2.Parameters["@mapPoint_id"].Value.ToString();
                else
                    mapPoint_id = "0";

                string intel_id = cmd2.Parameters["@intel_id"].Value.ToString();

                SQLConn2.Close();
                #endregion

                //**---------------------------------- Call oms_updateCurrentTable ----------------------------------
                #region oms_updateCurrentTable

                /*  Determin highest_n */
                int currentN;
                currentN = gameLevelIdx;       //currentN equals to the level of unit just completed

                //if (currentN > highestN)
                //    highestN = currentN;

                //if (currentN > highestN && overallAccuracy >= 0.85)         //** only assign the new "highest n" if passing 85%
                if (currentN > highestN)
                {
                    if (currentN < 4)
                        highestN = 1;
                    else  // convert unit level to n-value (3 unit levels per n-value)
                        highestN = (int)Math.Ceiling(currentN / 3.0);       //** round up to an integral value >= to the decimal number
                }
                //-----------------------

                SqlConnection SQLConn3 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
                SqlCommand cmd3 = new SqlCommand("oms_updateCurrentTable", SQLConn3);
                cmd3.CommandType = CommandType.StoredProcedure;

                SqlParameter Param3;
                Param3 = cmd3.Parameters.Add("@tid", SqlDbType.Int);
                Param3.Value = tid;
                Param3.Direction = ParameterDirection.Input;
                Param3 = cmd3.Parameters.Add("@nextLevel_idx", SqlDbType.Int);
                Param3.Value = nextLevel_IDx;
                Param3.Direction = ParameterDirection.Input;
                Param3 = cmd3.Parameters.Add("@nextGroupIdx", SqlDbType.Int);
                Param3.Value = nextGroupIDx;
                Param3.Direction = ParameterDirection.Input;
                Param3 = cmd3.Parameters.Add("@nextUnitIdx", SqlDbType.Int);
                Param3.Value = nextUnitIDx;
                Param3.Direction = ParameterDirection.Input;
                Param3 = cmd3.Parameters.Add("@nextUnitId", SqlDbType.VarChar, 50);
                Param3.Value = nextUnitID;
                Param3.Direction = ParameterDirection.Input;
                //
                Param3 = cmd3.Parameters.Add("@currentBadge_id", SqlDbType.VarChar, 50);
                Param3.Value = earnedBadgeID;
                Param3.Direction = ParameterDirection.Input;
                Param3 = cmd3.Parameters.Add("@qtyBadge0", SqlDbType.Int);
                Param3.Value = qtyBadge0;
                Param3.Direction = ParameterDirection.Input;
                Param3 = cmd3.Parameters.Add("@qtyBadge1", SqlDbType.Int);
                Param3.Value = qtyBadge1;
                Param3.Direction = ParameterDirection.Input;
                Param3 = cmd3.Parameters.Add("@qtyBadge2", SqlDbType.Int);
                Param3.Value = qtyBadge2;
                Param3.Direction = ParameterDirection.Input;
                Param3 = cmd3.Parameters.Add("@qtyBadge3", SqlDbType.Int);
                Param3.Value = qtyBadge3;
                Param3.Direction = ParameterDirection.Input;
                //
                Param3 = cmd3.Parameters.Add("@top_Badge_Cnt", SqlDbType.Int);
                Param3.Value = topBadgeCnt;
                Param3.Direction = ParameterDirection.Input;
                Param3 = cmd3.Parameters.Add("@cur_Mission_Node_Idx", SqlDbType.Int);
                Param3.Value = curMissioNode_Idx;
                Param3.Direction = ParameterDirection.Input;
                //
                Param3 = cmd3.Parameters.Add("@intel_id", SqlDbType.VarChar, 50);
                Param3.Value = intel_id;
                Param3.Direction = ParameterDirection.Input;
                Param3 = cmd3.Parameters.Add("@mapPoint_id", SqlDbType.Int);
                Param3.Value = Convert.ToInt32(mapPoint_id);
                Param3.Direction = ParameterDirection.Input;
                Param3 = cmd3.Parameters.Add("@missionKey", SqlDbType.Int);
                Param3.Value = 0;
                Param3.Direction = ParameterDirection.Input;
                Param3 = cmd3.Parameters.Add("@storyline_id", SqlDbType.Int);
                Param3.Value = 0;
                Param3.Direction = ParameterDirection.Input;
                //
                Param3 = cmd3.Parameters.Add("@training_power", SqlDbType.Int);
                Param3.Value = trainingPower;
                Param3.Direction = ParameterDirection.Input;
                Param3 = cmd3.Parameters.Add("@highest_n", SqlDbType.Int);
                Param3.Value = highestN;
                Param3.Direction = ParameterDirection.Input;
                Param3 = cmd3.Parameters.Add("@training_duration", SqlDbType.Int);
                Param3.Value = newTrainDuration;
                Param3.Direction = ParameterDirection.Input;

                SQLConn3.Open();
                cmd3.ExecuteNonQuery();

                //** Excepthion handling: if update return code  <> 0, do something?

                SQLConn3.Close();
                #endregion

                //**---------------------------------- Call oms_addPerfData ----------------------------------
                #region call stored procedure oms_addPerfData

                SqlConnection SQLConn4 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
                SqlCommand cmd4 = new SqlCommand("oms_addPerfData", SQLConn4);
                cmd4.CommandType = CommandType.StoredProcedure;

                SqlParameter Param4;
                Param4 = cmd4.Parameters.Add("@tid", SqlDbType.Int);
                Param4.Value = tid;
                Param4.Direction = ParameterDirection.Input;
                Param4 = cmd4.Parameters.Add("@level_idx", SqlDbType.Int);
                Param4.Value = gameLevelIdx;
                Param4.Direction = ParameterDirection.Input;
                Param4 = cmd4.Parameters.Add("@group_idx", SqlDbType.Int);
                Param4.Value = gameGroupIdx;
                Param4.Direction = ParameterDirection.Input;
                Param4 = cmd4.Parameters.Add("@unit_idx", SqlDbType.Int);
                Param4.Value = gameNodeIdx;
                Param4.Direction = ParameterDirection.Input;
                Param4 = cmd4.Parameters.Add("@unit_id", SqlDbType.VarChar, 50);
                Param4.Value = unitID;
                Param4.Direction = ParameterDirection.Input;
                //
                Param4 = cmd4.Parameters.Add("@overallAccuracy", SqlDbType.Float);
                Param4.Value = performance.accuracy_overall;
                Param4.Direction = ParameterDirection.Input;
                Param4 = cmd4.Parameters.Add("@overallReactionTime", SqlDbType.Float);
                Param4.Value = performance.reaction_time_overall;
                Param4.Direction = ParameterDirection.Input;
                Param4 = cmd4.Parameters.Add("@visualAccuracy", SqlDbType.Float);
                Param4.Value = performance.accuracy_vis;
                Param4.Direction = ParameterDirection.Input;
                Param4 = cmd4.Parameters.Add("@audioAccuracy", SqlDbType.Float);
                Param4.Value = performance.accuracy_aud;
                Param4.Direction = ParameterDirection.Input;
                Param4 = cmd4.Parameters.Add("@visualHighContrast", SqlDbType.Float);
                Param4.Value = performance.reaction_time_vis;  // ALERT: we're cheating for now
                Param4.Direction = ParameterDirection.Input;
                Param4 = cmd4.Parameters.Add("@visualLowContrast", SqlDbType.Float);
                Param4.Value = performance.reaction_time_aud;  // ALERT: we're cheating for now
                Param4.Direction = ParameterDirection.Input;
                Param4 = cmd4.Parameters.Add("@gameDuration", SqlDbType.Float);
                Param4.Value = gameDuration;
                Param4.Direction = ParameterDirection.Input;


                // note: performanceKey value is the primary key val. of the new row in the perf. table;
                // we hand it to the client to give back to us in the call to StoreUnit so we can cross-ref the
                // perf. data with the user-response data
                Param4 = cmd4.Parameters.Add("@perf_key", SqlDbType.Int);
                Param4.Direction = ParameterDirection.ReturnValue;

                SQLConn4.Open();
                cmd4.ExecuteNonQuery();

                string perf_key = cmd4.Parameters["@perf_key"].Value.ToString();

                SQLConn4.Close();
                #endregion call stored procedure oms_addPerfData

                #region //** Calculate Average Accuracy
                double avgAccuracy;
                OMS_utils omsCalAvgAccuracy = new OMS_utils();
                omsCalAvgAccuracy.calculateAvgAccuracy(tid);
                avgAccuracy = omsCalAvgAccuracy.avgAccuracy;
                //------------------
                #endregion

                #region //** Calculate Total Training Time
                int totalTrainingTime;
                OMS_utils omsTotalTrainingTime = new OMS_utils();
                omsTotalTrainingTime.calculateTotalTrainingTime(tid);
                totalTrainingTime = omsTotalTrainingTime.totalTrainingTime;
                //------------------
                #endregion

                #region //** Calculate Average Training Time
                int avgTrainingTime;
                OMS_utils omsAvgTrainingTime = new OMS_utils();
                omsAvgTrainingTime.calculateAvgTrainingTime(tid);
                avgTrainingTime = omsAvgTrainingTime.avgTrainingTime;
                #endregion

                #region prepare datastructure for return and log
                //**--------------- Return values to consuming Client for GetNextUnit ------------------------
                //string[] retvals = new string[16];

                //retvals[0] = Convert.ToString(nextLevel_IDx);
                //retvals[1] = Convert.ToString(nextGroupIDx);
                //retvals[2] = Convert.ToString(nextUnitIDx);
                //retvals[3] = nextUnitID;
                //retvals[4] = Convert.ToString(topBadgeCnt);
                //retvals[5] = Convert.ToString(curMissioNode_Idx);
                //retvals[6] = intel_id;
                //retvals[7] = mapPoint_id;   //Convert.ToString(mapPoint_id);
                //retvals[8] = earnedBadgeID;
                //retvals[9] = perf_key;
                //retvals[10] = Convert.ToString(trainingPower);
                //retvals[11] = Convert.ToString(trainingLevel);
                //retvals[12] = Convert.ToString(highestN);
                //retvals[13] = Convert.ToString(avgAccuracy);
                //retvals[14] = Convert.ToString(totalTrainingTime);
                //retvals[15] = Convert.ToString(avgTrainingTime);

                omsProgressRpt.isConfirmContinue = isConfirmContinue;

                oms_rewards.badge_id = int.Parse(earnedBadgeID);
                oms_rewards.mission.way_point_id = isWayPointEarned ? curMissioNode_Idx + 1 : -1;  // note: way_point_id uses 1-based counting
                oms_rewards.performance.training_level = trainingLevel;
                oms_rewards.performance.total_training_time = totalTrainingTime;
                oms_rewards.performance.avg_training_time = avgTrainingTime;
                oms_rewards.performance.highest_n = highestN;
                oms_rewards.performance.avg_accuracy = avgAccuracy;
                oms_rewards.performance.current_game_level = gameLevelIdx;

                #region [commented] log retvals[]
                ////** Write to log
                //formatLogData = "";
                //formatLogData = "OMSGetNextUnit return values: ";
                //for (int i = 0; i < 16; i++)
                //{
                //    formatLogData += string.Format("--{0}", retvals[i]);
                //}
                //omswsLog.log(formatLogData);
                #endregion log retvals[]


                //return retvals;
                #endregion [commented] retvals and log
            }
            #endregion try  // talk to db

            catch (Exception e)
            {
                //formatLogData = "Error Message = " + e.Message;
                //formatLogData += " StackTrace = " + e.StackTrace;
                //omswsLog.log(formatLogData);

                //return null;
            }
            #endregion perform evaluation, update user session in db

            #region encode return
            //string jsonstr = System.Web.Helpers.Json.Encode(new OMSUserProgressReport() { oms_rewards = oms_rewards });
            string jsonstr_0 = jsonstr_0 = System.Web.Helpers.Json.Encode(new OMSUserProgressReport() { 
                isConfirmContinue = omsProgressRpt.isConfirmContinue,
                oms_rewards = oms_rewards });
            string jsonstr = System.Web.Helpers.Json.Encode(omsProgressRpt);
            #endregion encode return

            #region log
            if (isLogEnabled)
            {
                //** Write to log               
                formatLogData = "OMSProcessUserPerformanceData Return:  \r\n";
                formatLogData += jsonstr_0;
                omswsLog.log(formatLogData);
            }
            #endregion log

            return jsonstr_0;
        }  // OMSProcessUserPerformanceData()
        #endregion OMSProcessUserPerformanceData() =======================================================================================
        #endregion WebMethods > prod > oms-app ===========================================================================================
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Tools;
using OMS.Tools;

namespace OMS.WebServices
{
    public partial class login1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region [commented] dev reset logcheck
            logcheck.Text = "logcheck msg goes here";
            //TextBox1.Attributes.Add("onblur", "document.getElementById('" + TextBox1.ClientID + "').innerText = ''");
            TextBox1.Attributes.Add("onFocus", "doClear(document.getElementById('" + logcheck.ClientID + "'))");
            //TextBox1.Attributes.Add("onFocus", "doClear(logcheck)");
            #endregion

        }

        #region [commented] dev reset logcheck
        //public void Text_Changed(Object sender, EventArgs e)
        //{
        //    Username.Text = "";
        //    TextBox1.Text = "";
        //    logcheck.Text = "";
        //}
        #endregion
        public void Login_Click(Object sender, EventArgs e)
        {

            bool isLogEnabled = OMSConfig.GetIsEnabledLogLoginEvents();

            #region [dev, disabled for prod]
            if (!CWTools.CWTrue)
            {
                OMS.Tools.OMSConfig.Toolsfoo();
                #region openread
                {
                    string filepathIn = @"C:\cwconfig\omswsconfig.txt";
                    StreamReader sr = null;
                    try
                    {
                        //Pass the file path and file name to the StreamReader constructor
                        sr = new StreamReader(filepathIn);
                    }
                    catch (Exception ee)
                    {
                        string s = ee.Message;
                        //Console.WriteLine("Exception: " + e.Message);
                    }
                    finally
                    {
                        //Console.WriteLine("Executing finally block.");
                    }
                }
                #endregion openread


                GetWSConfig();
                bool iisLogEnabled = isEnabledLogLoginEvents;
            }
            #endregion
            #region [dev, disabled for prod]
            if (!CWTools.CWTrue)
            {

                #region
                //// TODO: clean this up
                //string urlOMSTraineeApp = "";
                //OMS.Tools.OMSConfig.GetOMSConfig();
                //List<OMS.Tools.OMSConfig.OMSUserDescr> users = OMS.Tools.OMSConfig.GetUsers(pathConfigUsers: OMSConfig.PathConfigUsers);
                //OMSConfig.OMSUserDescr user = OMSConfig.GetUserDescr(tid: traineeID, users: users);

                string devUsername = "userfoo";
                int devTraineeID = 26;

                string formatLogData = "";
                OMS_utils omswsLog = new OMS_utils(logPath: "loginevents");
                #region log
                if (isLogEnabled)
                {
                    OMS.Tools.OMSConfig.GetOMSConfig();
                    List<OMS.Tools.OMSConfig.OMSUserDescr> users = OMS.Tools.OMSConfig.GetUsers(pathConfigUsers: OMSConfig.PathConfigUsers);
                    OMSConfig.OMSUserDescr user = OMSConfig.GetUserDescr(tid: devTraineeID, users: users);
                    if(user==null)user = new OMSConfig.OMSUserDescr() {Username="error retrieving user info"};
                    //** Write to log               
                    formatLogData = "Login:  \r\n";
                    formatLogData += "  un  = " + user.Username;
                    formatLogData += "  tid = " + devTraineeID;
                    formatLogData += "  users.Count = " + users.Count;
                    formatLogData += "  user.OMSRunMode = " + user.OMSRunMode;
                    formatLogData += "  user.NBackTheme = " + user.NBackTheme;
                    omswsLog.log(formatLogData);
                }
                #endregion log

                string urlOMSTraineeApp = OMSConfig.GetApplicationURL(tid: devTraineeID);

                #endregion

                #region [commented] test GetApplicationURL
                ////u,26,lynn100,0,0
                ////u,27,lynn101,1,0
                ////u,28,lynn102,1,0
                ////u,29,lynn103,2,1
                //string urlOMSTraineeApp = OMSConfig.GetApplicationURL(tid: 29);

                //string currentURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                ////  - split off query string
                //string[] tokv = CWTools.CWSplit(record: currentURL, delims: "?", trimWhitespace: true);
                ////
                //string loginURL = tokv[0];
                //string redirectURL = "http://oms-h5/shell/pagefoo/" + "?tid=" + 113 + "&ru=" + loginURL;
                //// "http://oms-h5/shell/select/?tid=2&ru=LOGIN_PAGE_URL

                //Response.Redirect(redirectURL);
                #endregion test GetApplicationURL

                if (sender != null) return;
            }
            #endregion [dev, commented]

            // input values from our markup (web form)
            string username = Username.Text.ToLower();  // "oms2"; //  
            string password = Password.Text;  // "test2"; // 

            #region query db
            //string sessionlink = "session.aspx?id=";
            //string enc_pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(pwd, "SHA1");

            SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd = new SqlCommand("oms_login", SQLConn);       //cw_login
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter Param;
            Param = cmd.Parameters.Add("@un", SqlDbType.VarChar, 25);
            Param.Value = username;
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@pw", SqlDbType.VarChar, 25);
            Param.Value = password;
            Param.Direction = ParameterDirection.Input;

            Param = cmd.Parameters.Add("@id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@role_id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@fn", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@ln", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@tid", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@role_dscr", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@expdays", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@active", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@retval", SqlDbType.Int);
            Param.Direction = ParameterDirection.ReturnValue;

            SQLConn.Open();
            cmd.ExecuteNonQuery();
            SQLConn.Close();
            #endregion query db

            #region config session
            if ((int)cmd.Parameters["@retval"].Value == 0)  // db query successful
            {

                //    //------------------------
                //    //Response.Redirect("http://operationmentalscorpion.com//cwsa/omsk12/dev/omstrainer/CW.SA.DemoTestPage.html?tid=" + traineeID);
                //    //Response.Redirect("http://209.196.155.44//cwsa/omsk12/dev/omstrainer/CW.SA.DemoTestPage.html?tid=" + traineeID);

                #region load settings
                int traineeID = (int)cmd.Parameters["@tid"].Value;

                //// TODO: clean this up
                //string urlOMSTraineeApp = "";
                //OMS.Tools.OMSConfig.GetOMSConfig();
                //List<OMS.Tools.OMSConfig.OMSUserDescr> users = OMS.Tools.OMSConfig.GetUsers(pathConfigUsers: OMSConfig.PathConfigUsers);
                //OMSConfig.OMSUserDescr user = OMSConfig.GetUserDescr(tid: traineeID, users: users);

                string formatLogData = "";
                OMS_utils omswsLog = new OMS_utils(logPath: "loginevents");
                #region log
                if (isLogEnabled)
                {
                    //** Write to log               
                    formatLogData = "Login:  \r\n";
                    formatLogData += "  un  = " + username;
                    formatLogData += "  tid = " + traineeID;
                    omswsLog.log(formatLogData);
                }
                #endregion log


                string urlOMSTraineeApp = OMSConfig.GetApplicationURL(tid: traineeID);

                #region [commented] old stuff
                //string userGrpMotivatorsOFF = "alpha";  // alpha  oms4  // TODO: put this in config file
                //urlOMSTraineeApp = username.StartsWith(userGrpMotivatorsOFF) ? OMS.Tools.OMSConfig.URLMotivatorsOFF : OMS.Tools.OMSConfig.URLMotivatorsONN;

                //if (!CWTools.CWTrue) { }
                //else if (OMS.Tools.OMSConfig.IsUserInSet(username: username, usersubsets: OMS.Tools.OMSConfig.MotvOFFUsers))
                //    urlOMSTraineeApp = OMS.Tools.OMSConfig.URLMotivatorsOFF;

                //else if (OMS.Tools.OMSConfig.IsUserInSet(username: username, usersubsets: OMS.Tools.OMSConfig.MotvONNUsers))
                //    urlOMSTraineeApp = OMS.Tools.OMSConfig.URLMotivatorsONN;

                //else if (OMS.Tools.OMSConfig.IsUserInSet(username: username, usersubsets: OMS.Tools.OMSConfig.ConfigSELUsers))
                //    urlOMSTraineeApp = OMS.Tools.OMSConfig.URLConfigSEL;

                //else
                //    urlOMSTraineeApp = OMS.Tools.OMSConfig.URLConfigSEL;
                #endregion old stuff


                #endregion load settings


                #region determine our URL
                //
                // src of below example: http://forums.asp.net/post/4145669.aspx
                //
                //Response.Redirect("oms_select_group.htm?tid=" + cmd.Parameters["@tid"].Value);
                //Response.Redirect("oms_select_group.htm?tid=" + traineeID);
                //Response.Redirect(urlOMSTraineeApp + "?tid=" + traineeID);

                //Request.Path :	/virtual_dir/webapp/page.aspx
                //Request.PhysicalApplicationPath :	d:\Inetpub\wwwroot\virtual_dir\
                //Request.QueryString :	/virtual_dir/webapp/page.aspx?q=qvalue
                //Request.Url.AbsolutePath :	/virtual_dir/webapp/page.aspx
                //Request.Url.AbsoluteUri :	http://localhost:2000/virtual_dir/webapp/page.aspx?q=qvalue
                //Request.Url.Host :	localhost
                //Request.Url.Authority :	localhost:80
                //Request.Url.LocalPath :	/virtual_dir/webapp/page.aspx
                //Request.Url.PathAndQuery :	/virtual_dir/webapp/page.aspx?q=qvalue
                //Request.Url.Port :	80
                //Request.Url.Query :	?q=qvalue
                //Request.Url.Scheme :	http


                // we split off {Scheme + Authority + LocalPath} aka protocol + host:port + path : 
                //  - start with full URL
                string currentURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                //  - split off query string
                string[] tokv = CWTools.CWSplit(record: currentURL, delims: "?", trimWhitespace: true);
                //
                string loginURL = tokv[0];
                // "http://oms-h5/shell/select/?tid=2&ru=LOGIN_PAGE_URL
                #endregion determine our URL

                OMSConfig.OMSUserDescr user = OMSConfig.GetUserDescr(tid: traineeID);
                //int runmode = user.OMSRunMode;
                //int nbacktheme = user.NBackTheme;
                //string redirectURL = urlOMSTraineeApp + "?tid=" + traineeID + "&ru=" + loginURL + "&rm=" + runmode;
                string redirectURL = urlOMSTraineeApp + "?tid=" + traineeID + "&ru=" + loginURL;
                Response.Redirect(redirectURL);
                //Response.Redirect(urlOMSTraineeApp + "?tid=" + traineeID);



            }
            else
            {
                string loginerr = "Invalid Login! Check your password and try again.";
                logcheck.Text = "<font color=\"FireBrick\">" + loginerr + "</font>";  //Invalid Login! Check your password and try again.   
            }
            #endregion config session

        }  // Login_Click()



        #region config mgmt [TODO: cleanup structures (consolodate duplicate code etc)]
        protected static bool isEnabledLogLoginEvents = false;

        static StreamReader openRead(string filepathIn)
        {
            StreamReader sr = null;
            try
            {
                //Pass the file path and file name to the StreamReader constructor
                sr = new StreamReader(filepathIn);
            }
            catch (Exception e)
            {
                string s = e.Message;
                //Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                //Console.WriteLine("Executing finally block.");
            }

            return sr;
        }
        protected static string BaseConfig = "";  // @"C:\cwconfig\";
        protected static string PathWSConfigFile = BaseConfig + @"omswsconfig.txt";

        protected static string PathNewUsers = BaseConfig + @"omsnewusersconfig.txt";

        protected static void GetWSConfig()
        {
            #region input global ws-config data from external config file: log flag etc

            {
                // TODO: check whether config updated since last read

                // note: usually PathConfigFile == @"C:\cwconfig\omsconfig.txt";
                StreamReader srr = openRead(PathWSConfigFile);  // TODO: errhandling
                if (srr == null) return;
                string dataLine = "";
                while (true)
                {
                    dataLine = srr.ReadLine();
                    if (dataLine == null) break;

                    // preprocess: trim leading,trailing whitespace; omit meta-lines
                    dataLine.Trim();
                    if (
                        dataLine.Length < 2 ||
                        dataLine.StartsWith("#") ||
                        dataLine.StartsWith("//")
                        ) continue;


                    string[] tokv = CWTools.CWSplit(record: dataLine, delims: "=", trimWhitespace: true);
                    #region store token values in OMSConfig
                    // TODO: make this more robust  
                    {
                        if (false) { }

                        else if
                        (
                                tokv[0] == "isEnabledLogLoginEvents"
                        )
                            isEnabledLogLoginEvents = "true".StartsWith(tokv[1]);
                    }  // for i<tokc
                    #endregion store tokens
                }  // while (pathConfig:dataline)

                srr.Close();
            }  // local code block: import omsconfig

            #endregion input global config data

        }  // GetConfig()


        #endregion config mgmt


    }
}
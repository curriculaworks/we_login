﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using CW.Tools;

namespace OMS.WebServices
{
    public partial class oms_ws : System.Web.Services.WebService
    {
        bool cwtrue { get { return CWTools.CWTrue; } set { } }

        // TODO: for OMSInit and OMSProcessUserPerformanceData, refactor code for logic into separate class

        #region notes: build and publish
        // - see oms_ws.asmx.cs for general build/publish notes
        // - add/exclude pages to build as web service (with variants)
        //   - exclude: login*, logout*
        //   - Set as Start Page: oms_ws.asmx
        //   - add/exclude modules omsws_*.cs as needed (and/or {un}comment [WebMethod] directives)
        //   - modules:
        //     - core --> always include
        //     - admin : oms-admin tools (create users etc)
        //     - app : main oms functionality (init, getunitdata etc)
        //     - dev : testing, poc etc
        //
        #region debug
        //debug:
        //  - set break points;
        //  - maybe adjust Startup Project > web service proj
        //  - menu Debug > Start Debugging;
        //  - after app launches, in browser go to oms_ws.asmx (if it is not the start page)
        //  - click a ws method and invoke it using the MS tool;
        //  - breakpts should now be hit;
        #endregion debug
        #endregion notes: build and publish

        #region notes: WebMethod overview
        // OMSInit(int tid); 
        //    return json.Encode(SessionWrapper)
        //
        // OMSGetUnitData(int tid); 
        //    return json.Encode(OMS.Tools.UDF.NBackUnitConfigWrapper);
        //
        // OMSProcessUserPerformanceData(string json.Encode(nback_perf))
        //    return json.Encode(oms_rewards)
        #endregion notes: WebMethod overview


        #region datastructures for json-encoding of session data (incl. rewards) (return from OMS{Init, ProcessUserPerformanceData})

        public class UserDisplayName
        {
            public string first = "Robert";
            public string last = "Young";
        }

        public class EarnedBadges
        {
            // most recently earned badge; value \in {-1, 0,1,2,3}, where -1 == no badges earned yet today
            public int current_badge_id = -1;

            // quantities of each badge type already earned today
            public int badge_qty_0 = 0;
            public int badge_qty_1 = 0;
            public int badge_qty_2 = 0;
            public int badge_qty_3 = 0;
        }

        public class Mission
        {
            // most recently earned (and highest) way point, where -1 == no mission progress yet. 
            // implication: all way points up to this one have also been earned
            public int way_point_id = -1;
        }

        /// <summary>
        /// class for those user-performance which are updated after each unit-completion
        /// </summary>
        public class UserPerformanceStatsImmed
        {
            public int training_level = 0;  // duration of training session; value \in {0,1,2}

            public int total_training_time = 0; // in seconds
            public int avg_training_time = 0;      // in seconds

            public double avg_accuracy = 0.0;         // \in [0,1.0]

            public int highest_n = 1;
            public int current_game_level = 1;
        }

        /// <summary>
        /// class for those user-performance which remain in effect throughout the session
        /// </summary>
        public class UserPerformanceStatsForSession : UserPerformanceStatsImmed
        {
            public int training_power = 0;  // training frequency; value \in {0,1,2}
        }

        #region datastructure for OMSInit return (stored session data)
        public class OMSSession
        {
            public UserDisplayName user_name = new UserDisplayName();

            public EarnedBadges earned_badges = new EarnedBadges();

            public Mission mission = new Mission();

            public UserPerformanceStatsForSession performance = new UserPerformanceStatsForSession();
        }

        public class OMSSessionWrapper
        {
            public OMSSession oms_session = new OMSSession();
        }
        #endregion datastructure for OMSInit return

        #region datastructure for OMSProcessUserPerformanceData return (new reward data)
        public class OMSNewRewards
        {
            public int badge_id = 0;  // value \in {0,1,2,3}

            public Mission mission = new Mission();

            public UserPerformanceStatsImmed performance = new UserPerformanceStatsImmed();
        }

        public class OMSUserProgressReport
        {
            public bool isConfirmContinue = false;  // require user to confirm continue training
            public OMSNewRewards oms_rewards = new OMSNewRewards();
        }

        //public class OMSNewRewardsWrapper
        //{
        //    public OMSNewRewards oms_rewards = new OMSNewRewards();
        //}
        #endregion datastructure for OMSProcessUserPerformanceData return

        #endregion datastructures for json-encoding of session data

        #region datastructures for json-encoding of user-performance data (arg to OMSProcessUserPerformanceData)

        public class UserPerformanceOnLastNBack
        {
            public int tid = -1;  // OMS user id; values \in [1,*]
            public string unit_id = "";  // ex. "L001G002N011"

            public double accuracy_overall = 0.0;  // value \in [0.0,1.0] ex.: 0.72,  
            public double accuracy_aud = 0.0;      // ex.  0.74,      
            public double accuracy_vis = 0.0;      // ex.  0.68,      

            public int reaction_time_overall = 0;  // in milliseconds, ex.: 500
            public int reaction_time_aud = 0;      // ex.  530  
            public int reaction_time_vis = 0;      // ex.  470
        }

        public class UserPerformanceOnLastNBackWrapper
        {
            public UserPerformanceOnLastNBack performance = new UserPerformanceOnLastNBack();
        }
        #endregion datastructures for json-encoding of user-performance data


        #region config mgmt [TODO: cleanup structures (consolodate duplicate code etc)]
        static StreamReader openRead(string filepathIn)
        {
            StreamReader sr = null;
            try
            {
                //Pass the file path and file name to the StreamReader constructor
                sr = new StreamReader(filepathIn);
            }
            catch (Exception e)
            {
                //Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                //Console.WriteLine("Executing finally block.");
            }

            return sr;
        }
        protected static string BaseConfig = @"C:\cwconfig\";
        protected static string PathWSConfigFile = BaseConfig + @"omswsconfig.txt";

        protected static string PathNewUserConfig = BaseConfig + @"omsnewusersconfig.txt";

        protected static void GetWSConfig()
        {
            #region input global ws-config data from external config file: log flag etc

            {
                // TODO: check whether config updated since last read

                // note: usually PathWSConfigFile == @"C:\cwconfig\omswsconfig.txt";
                StreamReader srr = openRead(PathWSConfigFile);  // TODO: errhandling
                if (srr == null) return;
                string dataLine = "";
                while (true)
                {
                    dataLine = srr.ReadLine();
                    if (dataLine == null) break;

                    // preprocess: trim leading,trailing whitespace; omit meta-lines
                    dataLine.Trim();
                    if (
                        dataLine.Length < 2 ||
                        dataLine.StartsWith("#") ||
                        dataLine.StartsWith("//")
                        ) continue;


                    string[] tokv = CWTools.CWSplit(record: dataLine, delims: "=", trimWhitespace: true);
                    #region store token values in OMSConfig
                    // TODO: make this more robust  
                    {
                        if (false) { }

                        else if
                        (
                                tokv[0] == "isLogEnabled"
                        )
                            isLogEnabled = "true".StartsWith(tokv[1]);

                        else if
                        (
                                tokv[0] == "isLogEnabledForAdmin"
                        )
                            isLogEnabledForAdmin = "true".StartsWith(tokv[1]);
                    }  // for i<tokc
                    #endregion store tokens
                }  // while (pathConfig:dataline)

                srr.Close();
            }  // local code block: import omsconfig

            #endregion input global config data

        }  // GetConfig()

        /// <summary>
        /// note: currently not used; see omsws_admin:OMSGenerateUserAccounts()
        /// </summary>
        protected static void GetGenUserConfig()
        {
            #region input "generate useracct" config data from external config file: user info

            List<string> datalines = CW.Tools.FileIO.LoadDataLines(PathNewUserConfig);
            foreach (string dataline in datalines)
            {
                string[] tokv = CWTools.CWSplit(record: dataline, delims: "=", trimWhitespace: true);
                #region store token values in OMSConfig
                // TODO: make this more robust  
                {
                    if (false) { }

                    else if
                    (
                            tokv[0] == "isLogEnabled"
                    )
                        isLogEnabled = "true".StartsWith(tokv[1]);
                }  // for i<tokc
                #endregion
            }  // foreach dataline

            {
                // TODO: check whether config updated since last read

                // note: usually PathConfigFile == @"C:\cwconfig\omsconfig.txt";
                StreamReader srr = openRead(PathNewUserConfig);  // TODO: errhandling
                if (srr == null) return;
                string dataLine = "";
                while (true)
                {
                    dataLine = srr.ReadLine();
                    if (dataLine == null) break;

                    // preprocess: trim leading,trailing whitespace; omit meta-lines
                    dataLine.Trim();
                    if (
                        dataLine.Length < 2 ||
                        dataLine.StartsWith("#") ||
                        dataLine.StartsWith("//")
                        ) continue;


                    string[] tokv = CWTools.CWSplit(record: dataLine, delims: "=", trimWhitespace: true);
                    #region store token values in OMSConfig
                    // TODO: make this more robust  
                    {
                        if (false) { }

                        else if
                        (
                                tokv[0] == "isLogEnabled"
                        )
                            isLogEnabled = "true".StartsWith(tokv[1]);
                    }  // for i<tokc
                    #endregion store tokens
                }  // while (pathConfig:dataline)

                srr.Close();
            }  // local code block: import omsconfig

            #endregion input global config data

        }  // GetConfig()

        #endregion config mgmt


        #region common for WebMethods > prod ===================================================================================================
        protected static bool isCallDB = true;  // true false  [disable db calls during dev]
        protected static bool isLogEnabled = true;  // true false  [log calls to WebMethods (omsws_*) in external txt file]
        protected static bool isLogEnabledForAdmin = true;  // true false  [log calls to WebMethods (omsws_admin) in external txt file]

        protected static int brkpnt = 4711;  // for forcing a particular place in the code to be break-pointable
        #endregion common for WebMethods > prod ===================================================================================================

    }
}
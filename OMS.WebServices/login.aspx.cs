﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Tools;
using OMS.Tools;

using System.Security.Cryptography;
using System.Text;

namespace OMS.WebServices
{
    public partial class login1 : System.Web.UI.Page
    {

        #region notes: build and publish
        // - see oms_ws.asmx.cs
        #endregion notes: build and publish

        /// <summary>
        /// hash secret shared by OMS-H5gen1 Student App
        /// </summary>
        protected string hashSecret
        {
            // TODO: probably should read this in from external file
            get {return "/bpxrDg7B8nedgLBsB0RGexiLkcrjzeL1QWNDM1CmL8ycSsqfR75euYZXAQdxbETyiIpOskhgnv+IUa8McRyZRy/NSOt4oG33A0qhhprGxqoF0+nw0DAFof7Esrn0mK/yoPzMWzJmgpmKzFINhXem4Bx6PAIrgGeegW8gGLkVsfm/D7tWAyoMYXtiVrPdrFzcolpctCkYofFMohLmXJZdorfmPLXlTBxfvS6WI0qujRg7G/oIsN4MpENuVOmMfzhQKcMQkV/GDlRy4JBjR3q57TKtyzOX8nGp3KQJ89a12fqxVQT3Donafg7D2fQeQRSs0D9a78P+J5BmfY8e9PSf98bxMqgXrs6OKiMjILI9uhG97O+SRjPnecoM3QS7rNeSA30YsH+oO6ukH1OfsY5fxW6sS9yROCa3ncSHg/15I0Ji0uw5FRUIa8gQJl4k3UFzmcFEHsWuBvzoIzgqHtncQSchDQ6/qSJbYGW/EqxNuQ3O9EqN/K6YBk3/wH/Ym0MXGZUg4sgIQCheh6MgMe6YlwDk2JlIYwREy/YGR3gKhb76UpPa426R/dleXiVb6NmaeHwyUGUDrQYP2vX8QtnSKDIDOk8zHJTj6SHf1kHEI0duGo2yPliSfKBGvM9SaoNFMOd1sPoX40x3LPh+4uTsjWZlRzV4jIYuq5DjHubt0kKZN/igQ+OAcR6fNR+RQMcQcsVXsEJIF0qeiqecMQAZNeRjWwT8CmfNaB6dGQAKQiT16aRX73ITwsXwbUWctx+Vti0xDiLyMUygkqJ4FJN17AegwJ+452p14jOEraC8DOigGUOs4EhzrphDgrN8qk6bAqa14+WfB5x8Z9Z+4xVFgxEV4ZUqSlVxXHtXvL3/S/eopn0Ox/t7aukonWuXs37h8WW8zMnC5WxxibHhSYep7AuR7RifQBJdJQMZawvehasQFfRM0Fyt0AIB3SyW6JDeUU+xkqejkFhDWZ6x9PvAH6KUAukFMM4AOrP1iEL2svjsorABAY3C6lyLdmYuENN48TIdCjUNi/d1jTsi5Kh0idxKwKSPsZX9uloBtBfMmJc6G5BfqjIo0T38mvt4jHIVxxwP9PUdCvJJHm9PJooMASYP7rHOdpY+YNo0/UXjamQO5RnEZcNyG5Mn4WG8uMrugHiaVjDRq1/f1jx0qe4srd5JooKUAA1PGedPGKJ3hDVHzWJEAFRM8ekNJgcBXRSu4jY3OpIGUXgPWleGwg+1JnE988UOWtwuVDaePa2+Q65Een08AR9BhWl3ysiHwU+osX4ca94tvSMB/GVCpqmTkfhRCMhb7GTlFnY/WyEat/y/1GVKZeeEEqSXrnbgr1OQ9S9aPCOc3zRNsPiqZK3Jf7Sgmx44pWsQvakCjLPWRanTDKgGuStfYtpFqJLD15XKhfT3y5XXroYolMkbrAyVRxXbAfNKJHisQ546Mg8e74PowbSDOcuDX/zM9wkd8Z7zFf7kunqj9Bvzta7OGaZ2UFmpYTGOllQ1z6cc1ZQd8mXKLqg/xhCzu3cgMaFyl7kTuOv7x2yNmSiryW3rrE1kXhpd2sdLWoqCOcMz+1No8aKcm6z9Y6P425LeOO4XNla";}
            set {}
        }

        protected static string HashSHA512(string password, string salt)
        {
            return BitConverter.ToString(new SHA512CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(password + ":" + salt))).Replace("-", "").ToLower();
        }

        protected static string HashSHA256(string password, string salt)
        {
            return BitConverter.ToString(new SHA256CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(password))).Replace("-", "").ToLower();
            //return BitConverter.ToString(new SHA256CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(password + ":" + salt))).Replace("-", "").ToLower();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // wire up reset logcheck
            Username.Attributes.Add("onFocus", "doClear(document.getElementById('" + logcheck.ClientID + "'))");
            Password.Attributes.Add("onFocus", "doClear(document.getElementById('" + logcheck.ClientID + "'))");


            #region try stuff out
            bool cwtrue = true;
            int brkpt = 368;

            if (!cwtrue)
            {
                Response.Redirect("logout.aspx");
            }

            if (!cwtrue)
            {
                string wsAuthority = System.Web.HttpContext.Current.Request.Url.Authority;
                string wsLocalPath = System.Web.HttpContext.Current.Request.Url.LocalPath;

                // find LocalPath without file
                //wsLocalPath = @"/oms/";  // dev only
                string wsLocalPathDir = wsLocalPath.Substring(0, wsLocalPath.LastIndexOf(@"/"));
                if (!wsLocalPathDir.EndsWith(@"/")) wsLocalPathDir += @"/";

                brkpt = 368;
            }

            #region dev stuff: SHA256 encryption hash code
            if (!cwtrue)
            {
                string lHashSecret = hashSecret;
                // precursor = unicode("foo=1/tid=38/wsu=http%3A%2F%2Fkidsoms.com/HASH_SECRET")
                string urlAuthority = "173.167.107.237%2F88/";  // final '/' included for convenience
                string urlQueryString = "foo=1/tid=38/wsu=http%3A%2F%2F" + urlAuthority;
                string encodePrecursor = urlQueryString + lHashSecret;
                string actualHash = BitConverter.ToString(new SHA256CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(encodePrecursor))).Replace("-", "").ToLower();

                string expectedHash = "251cd9fa9eafb7ab6bf09d758694833853a4460fa1a2673ed46ed1d8cb2ca201";

                bool isAsExpected = actualHash == expectedHash;
                brkpt = 368;


                ////string actualHash = System.Security.Cryptography.SHA256.
                ////var sha256 = new System.Security.Cryptography.SHA256Managed();
                ////string actualHash = sha256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(hashPrecursor)).ToString();
                //var sha256 = new System.Security.Cryptography.SHA256Managed();
                ////string actualHash = HashSHA256(urlQueryString, lHashSecret);
                //string actualHash = HashSHA256(urlQueryString, "");
            }

            #endregion current dev stuff

            #region [commented] dev stuff: reset logcheck etc
            //string wsAuthority = HttpContext.Current.Request.Url.Authority;

            //logcheck.Text = "logcheck msg goes here";
            ////TextBox1.Attributes.Add("onblur", "document.getElementById('" + TextBox1.ClientID + "').innerText = ''");
            //TextBox1.Attributes.Add("onFocus", "doClear(document.getElementById('" + logcheck.ClientID + "'))");
            ////TextBox1.Attributes.Add("onFocus", "doClear(logcheck)");
            #endregion
            #endregion try stuff out

        }

        #region [commented] dev reset logcheck
        //public void Text_Changed(Object sender, EventArgs e)
        //{
        //    Username.Text = "";
        //    TextBox1.Text = "";
        //    logcheck.Text = "";
        //}
        #endregion
        public void Login_Click(Object sender, EventArgs e)
        {

            bool isLogEnabled = OMSConfig.GetIsEnabledLogLoginEvents();

            #region [dev, disabled for prod]
            if (!CWTools.CWTrue)
            {
                OMS.Tools.OMSConfig.Toolsfoo();
                #region openread
                {
                    string filepathIn = @"C:\cwconfig\omswsconfig.txt";
                    StreamReader sr = null;
                    try
                    {
                        //Pass the file path and file name to the StreamReader constructor
                        sr = new StreamReader(filepathIn);
                    }
                    catch (Exception ee)
                    {
                        string s = ee.Message;
                        //Console.WriteLine("Exception: " + e.Message);
                    }
                    finally
                    {
                        //Console.WriteLine("Executing finally block.");
                    }
                }
                #endregion openread


                GetWSConfig();
                bool iisLogEnabled = isEnabledLogLoginEvents;
            }
            #endregion
            #region [dev, disabled for prod]
            if (!CWTools.CWTrue)
            {

                #region
                //// TODO: clean this up
                //string urlOMSTraineeApp = "";
                //OMS.Tools.OMSConfig.GetOMSConfig();
                //List<OMS.Tools.OMSConfig.OMSUserDescr> users = OMS.Tools.OMSConfig.GetUsers(pathConfigUsers: OMSConfig.PathConfigUsers);
                //OMSConfig.OMSUserDescr user = OMSConfig.GetUserDescr(tid: traineeID, users: users);

                string devUsername = "userfoo";
                int devTraineeID = 26;

                string formatLogData = "";
                OMS_utils omswsLog = new OMS_utils(logPath: "loginevents");
                #region log
                if (isLogEnabled)
                {
                    OMS.Tools.OMSConfig.GetOMSConfig();
                    List<OMS.Tools.OMSConfig.OMSUserDescr> users = OMS.Tools.OMSConfig.GetUsers(pathConfigUsers: OMSConfig.PathConfigUsers);
                    OMSConfig.OMSUserDescr user = OMSConfig.GetUserDescr(tid: devTraineeID, users: users);
                    if(user==null)user = new OMSConfig.OMSUserDescr() {Username="error retrieving user info"};
                    //** Write to log               
                    formatLogData = "Login:  \r\n";
                    formatLogData += "  un  = " + user.Username;
                    formatLogData += "  tid = " + devTraineeID;
                    formatLogData += "  users.Count = " + users.Count;
                    formatLogData += "  user.OMSRunMode = " + user.OMSRunMode;
                    formatLogData += "  user.NBackTheme = " + user.NBackTheme;
                    omswsLog.log(formatLogData);
                }
                #endregion log

                string urlOMSTraineeApp = OMSConfig.GetApplicationURL(tid: devTraineeID);

                #endregion

                #region [commented] test GetApplicationURL
                ////u,26,lynn100,0,0
                ////u,27,lynn101,1,0
                ////u,28,lynn102,1,0
                ////u,29,lynn103,2,1
                //string urlOMSTraineeApp = OMSConfig.GetApplicationURL(tid: 29);

                //string currentURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                ////  - split off query string
                //string[] tokv = CWTools.CWSplit(record: currentURL, delims: "?", trimWhitespace: true);
                ////
                //string loginURL = tokv[0];
                //string redirectURL = "http://oms-h5/shell/pagefoo/" + "?tid=" + 113 + "&ru=" + loginURL;
                //// "http://oms-h5/shell/select/?tid=2&ru=LOGIN_PAGE_URL

                //Response.Redirect(redirectURL);
                #endregion test GetApplicationURL

                if (sender != null) return;
            }
            #endregion [dev, commented]

            // input values from our markup (web form)
            string username = Username.Text.ToLower();  // "oms2"; //  
            string password = Password.Text;  // "test2"; // 

            #region query db
            #region query db: oms_login
            //string sessionlink = "session.aspx?id=";
            //string enc_pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(pwd, "SHA1");

            SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd = new SqlCommand("oms_login", SQLConn);       //cw_login
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter Param;
            Param = cmd.Parameters.Add("@un", SqlDbType.VarChar, 25);
            Param.Value = username;
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@pw", SqlDbType.VarChar, 25);
            Param.Value = password;
            Param.Direction = ParameterDirection.Input;

            Param = cmd.Parameters.Add("@id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@role_id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@fn", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@ln", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@tid", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@role_dscr", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@expdays", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@active", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@retval", SqlDbType.Int);
            Param.Direction = ParameterDirection.ReturnValue;

            SQLConn.Open();
            cmd.ExecuteNonQuery();
            SQLConn.Close();

            int tid = (int)cmd.Parameters["@tid"].Value;
            int loginRetval = (int)cmd.Parameters["@retval"].Value;
            #endregion query db: oms_login


            //**---------------------------------- Call oms_retrieveCurrentTable ----------------------------------
            #region oms_retrieveCurrentTable

            SqlConnection SQLConn5 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd5 = new SqlCommand("oms_retrieveCurrentTable", SQLConn5);
            cmd5.CommandType = CommandType.StoredProcedure;

            SqlParameter Param5;
            Param5 = cmd5.Parameters.Add("@tid", SqlDbType.Int);
            Param5.Value = tid;
            Param5.Direction = ParameterDirection.Input;
            Param5 = cmd5.Parameters.Add("@top_Badge_Cnt", SqlDbType.Int);
            Param5.Direction = ParameterDirection.Output;
            Param5 = cmd5.Parameters.Add("@cur_Mission_Node_Idx", SqlDbType.Int);
            Param5.Direction = ParameterDirection.Output;
            Param5 = cmd5.Parameters.Add("@app_starttime", SqlDbType.DateTime);
            Param5.Direction = ParameterDirection.Output;
            Param5 = cmd5.Parameters.Add("@highest_n", SqlDbType.Int);
            Param5.Direction = ParameterDirection.Output;
            Param5 = cmd5.Parameters.Add("@training_duration", SqlDbType.Int);
            Param5.Direction = ParameterDirection.Output;
            Param5 = cmd5.Parameters.Add("@recentUnit_ts", SqlDbType.DateTime);
            Param5.Direction = ParameterDirection.Output;

            SQLConn5.Open();
            cmd5.ExecuteNonQuery();

            // store retrieved db values in local datastructures (with some minor data processing)
            int topBadgeCnt = (int)cmd5.Parameters["@top_Badge_Cnt"].Value;
            //int curMissioNode_Idx = (int)cmd5.Parameters["@cur_Mission_Node_Idx"].Value;
            //// TODO: Q: why does appSTartTime seem to be set to currenttime on every call to processuserperformance? 
            ////          It should only be set once - in OMSInit...
            ////       A: because GetUnitData calls sp:cwauth
            ////       Solution: revise mgmt of appStartTime in cwauth e.g. with flag isUpdateStartTime 
            //DateTime appStartTime = new DateTime();
            //try { appStartTime = (DateTime)cmd5.Parameters["@app_starttime"].Value; }
            //catch (Exception e) { }
            //int highestN = (int)cmd5.Parameters["@highest_n"].Value;
            //int newTrainDuration = (int)cmd5.Parameters["@training_duration"].Value + gameDuration;
            //DateTime l_recentUnit_ts = (DateTime)cmd5.Parameters["@recentUnit_ts"].Value;

            SQLConn5.Close();
            #endregion

            #endregion query db

            // ALERT: hardcoded for Research Study 2013-04; align with omsws_app.cs:#region way-pt logic
            // If user authenticates and session completed, we redirect to logout page
            int maxUnitsForSession = 8;  // and after one session, account is de facto blocked; align with omsws_app.cs
            if (loginRetval == 0 && topBadgeCnt >= maxUnitsForSession) Response.Redirect("logout.aspx");

            #region config session
            //if ((int)cmd.Parameters["@retval"].Value == 0)  // db query oms_login successful; user authenticated
            if (loginRetval == 0)  // db query oms_login successful; user authenticated
            {

                //    //------------------------
                //    //Response.Redirect("http://operationmentalscorpion.com//cwsa/omsk12/dev/omstrainer/CW.SA.DemoTestPage.html?tid=" + traineeID);
                //    //Response.Redirect("http://209.196.155.44//cwsa/omsk12/dev/omstrainer/CW.SA.DemoTestPage.html?tid=" + traineeID);

                #region load external user settings 
                int traineeID = (int)cmd.Parameters["@tid"].Value;

                //// TODO: clean this up
                //string urlOMSTraineeApp = "";
                //OMS.Tools.OMSConfig.GetOMSConfig();
                //List<OMS.Tools.OMSConfig.OMSUserDescr> users = OMS.Tools.OMSConfig.GetUsers(pathConfigUsers: OMSConfig.PathConfigUsers);
                //OMSConfig.OMSUserDescr user = OMSConfig.GetUserDescr(tid: traineeID, users: users);

                string formatLogData = "";
                OMS_utils omswsLog = new OMS_utils(logPath: "loginevents");
                #region log
                if (isLogEnabled)
                {
                    //** Write to log               
                    formatLogData = "Login:  \r\n";
                    formatLogData += "  un  = " + username;
                    formatLogData += "  tid = " + traineeID;
                    omswsLog.log(formatLogData);
                }
                #endregion log


                string urlOMSTraineeApp = OMSConfig.GetApplicationURL(tid: traineeID);

                #region [commented] old stuff
                //string userGrpMotivatorsOFF = "alpha";  // alpha  oms4  // TODO: put this in config file
                //urlOMSTraineeApp = username.StartsWith(userGrpMotivatorsOFF) ? OMS.Tools.OMSConfig.URLMotivatorsOFF : OMS.Tools.OMSConfig.URLMotivatorsONN;

                //if (!CWTools.CWTrue) { }
                //else if (OMS.Tools.OMSConfig.IsUserInSet(username: username, usersubsets: OMS.Tools.OMSConfig.MotvOFFUsers))
                //    urlOMSTraineeApp = OMS.Tools.OMSConfig.URLMotivatorsOFF;

                //else if (OMS.Tools.OMSConfig.IsUserInSet(username: username, usersubsets: OMS.Tools.OMSConfig.MotvONNUsers))
                //    urlOMSTraineeApp = OMS.Tools.OMSConfig.URLMotivatorsONN;

                //else if (OMS.Tools.OMSConfig.IsUserInSet(username: username, usersubsets: OMS.Tools.OMSConfig.ConfigSELUsers))
                //    urlOMSTraineeApp = OMS.Tools.OMSConfig.URLConfigSEL;

                //else
                //    urlOMSTraineeApp = OMS.Tools.OMSConfig.URLConfigSEL;
                #endregion old stuff


                #endregion load settings


                #region determine our URL
                //
                // src of below example: http://forums.asp.net/post/4145669.aspx
                //
                //Response.Redirect("oms_select_group.htm?tid=" + cmd.Parameters["@tid"].Value);
                //Response.Redirect("oms_select_group.htm?tid=" + traineeID);
                //Response.Redirect(urlOMSTraineeApp + "?tid=" + traineeID);

                //Request.Path :	/virtual_dir/webapp/page.aspx
                //Request.PhysicalApplicationPath :	d:\Inetpub\wwwroot\virtual_dir\
                //Request.QueryString :	/virtual_dir/webapp/page.aspx?q=qvalue
                //Request.Url.AbsolutePath :	/virtual_dir/webapp/page.aspx
                //Request.Url.AbsoluteUri :	http://localhost:2000/virtual_dir/webapp/page.aspx?q=qvalue
                //Request.Url.Host :	localhost
                //Request.Url.Authority :	localhost:80
                //Request.Url.LocalPath :	/virtual_dir/webapp/page.aspx
                //Request.Url.PathAndQuery :	/virtual_dir/webapp/page.aspx?q=qvalue
                //Request.Url.Port :	80
                //Request.Url.Query :	?q=qvalue
                //Request.Url.Scheme :	http


                // we split off {Scheme + Authority + LocalPath} aka protocol + host:port + path : 
                //  - start with full URL
                string currentURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                //  - split off query string
                string[] tokv = CWTools.CWSplit(record: currentURL, delims: "?", trimWhitespace: true);
                //
                string loginURL = tokv[0];
                // "http://oms-h5/shell/select/?tid=2&ru=LOGIN_PAGE_URL

                // pull off filename at end (login.aspx)
                string webappBase = loginURL.Substring(0, loginURL.LastIndexOf(@"/"));
                string logoutURL = webappBase + @"/logout.aspx";

                string wsAuthority = System.Web.HttpContext.Current.Request.Url.Authority;
                string wsLocalPath = System.Web.HttpContext.Current.Request.Url.LocalPath;

                // find LocalPath without file
                wsLocalPath = @"/oms/";  // dev only
                string wsLocalPathDir = wsLocalPath.Substring(0, wsLocalPath.LastIndexOf(@"/"));
                //string logoutURL = wsLocalPathDir + @"/logout.aspx";

                #endregion determine our URL

                OMSConfig.OMSUserDescr user = OMSConfig.GetUserDescr(tid: traineeID);
                //int runmode = user.OMSRunMode;
                //int nbacktheme = user.NBackTheme;
                //string redirectURL = urlOMSTraineeApp + "?tid=" + traineeID + "&ru=" + loginURL + "&rm=" + runmode;
                //string redirectURL = urlOMSTraineeApp + "?tid=" + traineeID + "&ru=" + loginURL + "&wsu=" + wsAuthority;
                //string redirectURL = urlOMSTraineeApp + "?tid=" + traineeID + "&ru=" + logoutURL + "&wsu=" + wsAuthority;
                string redirectURL = urlOMSTraineeApp + "?tid=" + traineeID + "&ru=" + logoutURL;
                Response.Redirect(redirectURL);
                //Response.Redirect(urlOMSTraineeApp + "?tid=" + traineeID);



            }
            else
            {
                string loginerr = "Invalid Login! Check your password and try again.";
                logcheck.Text = "<font color=\"FireBrick\">" + loginerr + "</font>";  //Invalid Login! Check your password and try again.   
            }
            #endregion config session

        }  // Login_Click()



        #region config mgmt [TODO: cleanup structures (consolodate duplicate code etc)]
        protected static bool isEnabledLogLoginEvents = false;

        static StreamReader openRead(string filepathIn)
        {
            StreamReader sr = null;
            try
            {
                //Pass the file path and file name to the StreamReader constructor
                sr = new StreamReader(filepathIn);
            }
            catch (Exception e)
            {
                string s = e.Message;
                //Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                //Console.WriteLine("Executing finally block.");
            }

            return sr;
        }
        protected static string BaseConfig = "";  // @"C:\cwconfig\";
        protected static string PathWSConfigFile = BaseConfig + @"omswsconfig.txt";

        protected static string PathNewUsers = BaseConfig + @"omsnewusersconfig.txt";

        protected static void GetWSConfig()
        {
            #region input global ws-config data from external config file: log flag etc

            {
                // TODO: check whether config updated since last read

                // note: usually PathConfigFile == @"C:\cwconfig\omsconfig.txt";
                StreamReader srr = openRead(PathWSConfigFile);  // TODO: errhandling
                if (srr == null) return;
                string dataLine = "";
                while (true)
                {
                    dataLine = srr.ReadLine();
                    if (dataLine == null) break;

                    // preprocess: trim leading,trailing whitespace; omit meta-lines
                    dataLine.Trim();
                    if (
                        dataLine.Length < 2 ||
                        dataLine.StartsWith("#") ||
                        dataLine.StartsWith("//")
                        ) continue;


                    string[] tokv = CWTools.CWSplit(record: dataLine, delims: "=", trimWhitespace: true);
                    #region store token values in OMSConfig
                    // TODO: make this more robust  
                    {
                        if (false) { }

                        else if
                        (
                                tokv[0] == "isEnabledLogLoginEvents"
                        )
                            isEnabledLogLoginEvents = "true".StartsWith(tokv[1]);
                    }  // for i<tokc
                    #endregion store tokens
                }  // while (pathConfig:dataline)

                srr.Close();
            }  // local code block: import omsconfig

            #endregion input global config data

        }  // GetConfig()


        #endregion config mgmt


    }
}
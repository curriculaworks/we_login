using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Resources;
using System.Globalization;
using System.Threading;

namespace CurriculaWorks
{
	public partial class forgetpassword : System.Web.UI.Page
	{
        protected System.Web.UI.WebControls.Panel firstmessage;
        protected System.Web.UI.WebControls.TextBox username;
        protected System.Web.UI.WebControls.Label badreply;
        protected System.Web.UI.WebControls.Label goodreply;
        protected System.Web.UI.WebControls.Button submit;
        protected System.Web.UI.WebControls.HiddenField SOURCE;
        protected System.Web.UI.HtmlControls.HtmlGenericControl forgotten;
        protected System.Web.UI.HtmlControls.HtmlGenericControl unme;
        protected System.Web.UI.HtmlControls.HtmlGenericControl help;
        protected System.Web.UI.HtmlControls.HtmlGenericControl assist;
        protected string support;
        protected bool parentsetpass = false;

        private ResourceManager rm;

        SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);


		private void Page_Load(object sender, System.EventArgs e)
		{
            string strCulture;
            string passparm = Request.QueryString["param"];     /* forgetpassword.aspx?param=setpass */

            if (Session["SelectCulture"] != null)
            {
                strCulture = Session["SelectCulture"].ToString();
            }
            else
            {
                Session["SelectCulture"] = "en-US";
                strCulture = Session["SelectCulture"].ToString();
            }
            //string strCulture = Session["SelectCulture"].ToString();
            CultureInfo objCI = new CultureInfo(strCulture);
            Thread.CurrentThread.CurrentCulture = objCI;
            Thread.CurrentThread.CurrentUICulture = objCI;
            //Read the rsources files
            String strResourcesPath = Server.MapPath("bin");
            rm = ResourceManager.CreateFileBasedResourceManager("resource", strResourcesPath, null);            
            forgotten.InnerText = rm.GetString("0114");
            unme.InnerText = rm.GetString("0108");
            help.InnerText = rm.GetString("0115");
            assist.InnerText = rm.GetString("0116");
            submit.Text = rm.GetString("0307");
            //.InnerText = rm.GetString("0014");
            badreply.Text=rm.GetString("0353");
            support = rm.GetString("0336");

            if (passparm == "setpass")    //If it is called from rptStudent.aspx page, send the setting password.
                parentsetpass = true;   
		}

        protected void sendemail(Object sender, EventArgs e)
        {
            string pw, email, name, parentpw;
			int rep_id;
            string SQLStr = "cw_getpw '" + username.Text + "'";			
            SQLConn.Open();
            SqlCommand cmd = new SqlCommand(SQLStr,SQLConn);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                pw = reader.GetString(0);
                email = reader.GetString(1);
				rep_id = reader.GetInt32(2);
				name = reader.GetString(3); 
				parentpw = reader.GetString(4);
                badreply.Visible = false;
                firstmessage.Visible = false;
                MailMessage msg = new MailMessage();
                msg.BodyFormat = MailFormat.Text;

				if (rep_id == 7)
				{
					msg.To = email;
					msg.From = "\"Explode The Code Online\" <ETC_online@epsbooks.com>";
					msg.Subject = "Your Explode The Code Online Password";
					msg.Body = username.Text + ":\n\n Your password for Explode The Code Online is " + pw + ".\n\nTo change your password, please see http://www.explodethecode.com/help/guides/. \n\nIf you believe you received this message in error, please reply to this message immediately.\n\nIf you have any other questions, view online help at http://www.explodethecode.com/help/ or simply reply to this e-mail." +
						"\n------------------------------------------------" +
						"\nExplode The Code Online Customer Service" +
						"\nETC_Online@epsbooks.com | 800.225.5750, ext. 4" +
						"\nhttp://www.explodethecode.com/\n";
					SmtpMail.SmtpServer = "localhost";
					SmtpMail.Send(msg);
                    Response.Redirect("http://www.explodethecode.com/login/password.cfm?sent");
				}
				else
				{
					msg.To = email;
					msg.From = "webmaster@curriculaworks.com";
                    msg.Subject = rm.GetString("0356");


                    //msg.Body = "Greetings, " + username.Text + ". Here is your password, given to you upon your request: \n\n\tPassword: " + 
                    //            pw + "\n\tSettings Password: " + parentpw + ".\nIf you believe you received this message in error or that someone 
                    //            might be trying to steal your password, please email support@curriculaworks.com.";
                    if (parentsetpass)   //** if setting password, send only that
                    {
                        msg.Body = rm.GetString("0348") + ", " + username.Text + ". " + rm.GetString("0349") + "\n\n\t" + 
                                   rm.GetString("0351") + " " + parentpw + "\n\n\t" + rm.GetString("0352");
                        parentsetpass = false;
                    }
                    else            //** otherwise, send user password
                        msg.Body = rm.GetString("0348") + ", " + username.Text + ". " + rm.GetString("0349") + "\n\n\t" + 
                                   rm.GetString("0350") + " " + pw + "\n\n\t" + rm.GetString("0352");

                    SmtpMail.SmtpServer = "localhost";
                    SmtpMail.Send(msg);
				}
               
                //goodreply.Text = "Your password has been sent to <b>" + email + "</b>. (If this email does not arrive in a timely fashion, 
                //make sure you check your bulk/spam folder to make sure it hasn't been sent there.)<br/><br/><a href=\"javascript:window.close();\">Close this window</a>.";
                goodreply.Text = rm.GetString("0354") + " <b>" + email + "</b>. " + rm.GetString("0355");
                goodreply.Visible = true;
            }
            else
            {
                if (SOURCE != null && SOURCE.Value == "EPS")
                {
                    Response.Redirect("http://www.explodethecode.com/login/password.cfm?error");
                }
                else
                {
                    badreply.Visible = true;
                }
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}

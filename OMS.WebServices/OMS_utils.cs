﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace OMS.WebServices
{
    public class OMS_utils
    {
        #region ALERT TODO: refactor this class!
        // inherited from monolithic prev version (oms_kids-2012spring i.e. cw_oms_ws_K.sln)
        // todo:
        //   - isolate db calls (Stored procedures)
        //   - isolate logic for calculating/determining user-state param values (e.g. Training Level)
        // see also:
        //   - ./OMS.WebTools.readme_OMS_utils.cs
        #endregion

        public OMS_utils() { }
        public OMS_utils(string logPath)
        {
            if (logPath != "")
            {
                string lp = logPath;
                if (!lp.EndsWith(".txt")) lp += ".txt";
                LogPath = lp;
            }
        }

        public int nLevel_idx;
        public int nGroupIDx;
        public int nUnitIDx;
        public string nUnitID;
        public double avgAccuracy;
        public int totalTrainingTime;
        public int avgTrainingTime;
        public int trainLevel;

        public void branchingLogic(int level_idx, int group_idx, int unit_idx, string unit_id, string branch_sp)
        {
            SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd;

            //if (earnedBadge == "3")
            //     cmd = new SqlCommand("oms_cw_Branch_Up_A_Level", SQLConn);
            //else if (earnedBadge == "2")
            //    cmd = new SqlCommand("oms_cw_Branch_Current_Level_Next_Group", SQLConn);
            //else if (earnedBadge == "1")
            //    cmd = new SqlCommand("oms_cw_Branch_Current_Level_Group_Next_Unit", SQLConn);
            //else //if (earnedBadge == "0")                                      //** Try and throw error if earnBadge = "-1" ?
            //    cmd = new SqlCommand("oms_cw_Branch_Back_A_Level", SQLConn);

            cmd = new SqlCommand(branch_sp, SQLConn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter Param;
            Param = cmd.Parameters.Add("@currentLevel_idx", SqlDbType.Int);
            Param.Value = level_idx;
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@currentGroupIdx", SqlDbType.Int);
            Param.Value = group_idx;
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@currentUnitIdx", SqlDbType.Int);
            Param.Value = unit_idx;
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@currentUnitId", SqlDbType.VarChar, 50);
            Param.Value = unit_id;
            Param.Direction = ParameterDirection.Input;
            //
            Param = cmd.Parameters.Add("@level_idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@group_idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@unit_idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@unit_id", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;

            SQLConn.Open();
            cmd.ExecuteNonQuery();

            //** Write to log
            //OMS_utils omswsLog = new OMS_utils();

            //string formatLogData = "";
            //formatLogData = "oms_utils,  cmd.Parameters[@level_idx].Value = " + cmd.Parameters["@level_idx"].Value;
            //formatLogData += ", oms_utils,  cmd.Parameters[@group_idx].Value = " + cmd.Parameters["@group_idx"].Value;
            //formatLogData += ", oms_utils,  cmd.Parameters[@unit_idx].Value = " + cmd.Parameters["@unit_idx"].Value;
            //formatLogData += "--------------------------";
            //formatLogData += "Input Values: level_idx= " + level_idx + ", group_idx= " + group_idx;
            //formatLogData += ",  unit_idx= " + unit_idx + ", unit_id= " + unit_id + ", earnedBadge= " + earnedBadge;

            //omswsLog.log(formatLogData);
            //----------------------------
            nLevel_idx = (int)cmd.Parameters["@level_idx"].Value;
            nGroupIDx = (int)cmd.Parameters["@group_idx"].Value;
            nUnitIDx = (int)cmd.Parameters["@unit_idx"].Value;
            //if ((cmd.Parameters["@unit_idx"].Value != null) && (cmd.Parameters["@unit_idx"].Value != " "))
            //    nUnitIDx = (int)cmd.Parameters["@unit_idx"].Value;
            //else
            //    nUnitIDx = 0;

            nUnitID = cmd.Parameters["@unit_id"].Value.ToString();

            SQLConn.Close();
        }


        public void determineTrainingLevel(DateTime recentUnitTS)
        {
            DateTime tsNow = DateTime.Now;

            TimeSpan timeElapse = tsNow.Subtract(recentUnitTS);

            int timeElapse_Hours = timeElapse.Hours;

            if (timeElapse_Hours > 24)
                trainLevel = 0;             //rookie
            else if (timeElapse_Hours > 12)
                trainLevel = 1;             //amateur
            else //(timeElapse_Hours <= 12)
                trainLevel = 2;             //pro
        }


        public void calculateAvgAccuracy(int tid)
        {
            SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd = new SqlCommand("oms_getAvgAccuracy", SQLConn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter Param;
            Param = cmd.Parameters.Add("@tid", SqlDbType.Int);
            Param.Value = tid;
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@avg_accuracy", SqlDbType.Float);
            Param.Direction = ParameterDirection.Output;

            SQLConn.Open();
            cmd.ExecuteNonQuery();

            if (cmd.Parameters["@avg_accuracy"].Value.ToString() != "")
                avgAccuracy = (double)cmd.Parameters["@avg_accuracy"].Value;
            else
                avgAccuracy = 0.0;

            SQLConn.Close();
        }  // calculateAvgAccuracy()


        public void calculateTotalTrainingTime(int tid)
        {
            SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd = new SqlCommand("oms_getTotalTrainingTime", SQLConn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter Param9;
            Param9 = cmd.Parameters.Add("@tid", SqlDbType.Int);
            Param9.Value = tid;
            Param9.Direction = ParameterDirection.Input;
            Param9 = cmd.Parameters.Add("@total_training_time", SqlDbType.Int);
            Param9.Direction = ParameterDirection.Output;

            SQLConn.Open();
            cmd.ExecuteNonQuery();

            if (cmd.Parameters["@total_training_time"].Value.ToString() != "")
                totalTrainingTime = (int)cmd.Parameters["@total_training_time"].Value;
            else
                totalTrainingTime = 0;


            SQLConn.Close();
        }



        public void calculateAvgTrainingTime(int tid)
        {
            SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd = new SqlCommand("oms_getAvgTrainingTime", SQLConn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter Param8;
            Param8 = cmd.Parameters.Add("@tid", SqlDbType.Int);
            Param8.Value = tid;
            Param8.Direction = ParameterDirection.Input;
            Param8 = cmd.Parameters.Add("@avg_training_time", SqlDbType.Int);
            Param8.Direction = ParameterDirection.Output;

            SQLConn.Open();
            cmd.ExecuteNonQuery();

            avgTrainingTime = (int)cmd.Parameters["@avg_training_time"].Value;
            //int avgTrainingTime;
            //if (cmd.Parameters["@avg_training_time"].Value.ToString() != "")
            //    avgTrainingTime = (int)cmd.Parameters["@avg_training_time"].Value;
            //else
            //    avgTrainingTime = 0;

            SQLConn.Close();
        }

        protected string logPath = "logs/web_service_log.txt";
        public string LogPath { get { return logPath; } set { logPath = "logs/" + value; } }
        public void log(string data)
        {
            string fn = HttpContext.Current.Server.MapPath(LogPath);
            //string fn = HttpContext.Current.Server.MapPath("logs/web_service_log.txt");

            StreamWriter sw = File.Exists(fn) ? File.AppendText(fn) : new StreamWriter(fn);

            using (sw)
            {
                sw.WriteLine("\r\n-------------------------------");
                sw.Write("Log Entry : ");
                sw.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                sw.WriteLine("  :");
                sw.WriteLine("  :{0}", data);
                // Update the underlying file.
                sw.Flush();
                sw.Close();
            }
        }
    }
}

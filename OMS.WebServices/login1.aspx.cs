﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMS.WebServices
{
    public partial class login1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void Login_Click(Object sender, EventArgs e)
        {

            //string sessionlink = "session.aspx?id=";
            //string enc_pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(pwd, "SHA1");

            SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd = new SqlCommand("oms_login", SQLConn);       //cw_login
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter Param;
            Param = cmd.Parameters.Add("@un", SqlDbType.VarChar, 25);
            Param.Value = "oms2"; //  Username.Text.ToLower();
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@pw", SqlDbType.VarChar, 25);
            Param.Value = "test2"; // Password.Text;
            Param.Direction = ParameterDirection.Input;

            Param = cmd.Parameters.Add("@id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@role_id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@fn", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@ln", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@tid", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@role_dscr", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@expdays", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@active", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@retval", SqlDbType.Int);
            Param.Direction = ParameterDirection.ReturnValue;

            SQLConn.Open();
            cmd.ExecuteNonQuery();
            SQLConn.Close();

            #region config session
            if ((int)cmd.Parameters["@retval"].Value == 0)
            {
            #region etco stuff
            ////    Session["omsid"] = cmd.Parameters["@id"].Value;
            ////    Session["role"] = cmd.Parameters["@role_id"].Value;
            ////    Session["name"] = Username.Text;
            ////    Session["expdays"] = cmd.Parameters["@expdays"].Value;
            ////    Session["tid"] = cmd.Parameters["@tid"].Value;

            ////    int traineeID = (int)cmd.Parameters["@tid"].Value;

            ////    //sessionlink += Convert.ToString(Session["cwid"]) + "&x=1";  //** for SBIR testing
            ////    //Response.Redirect(sessionlink);                             //** for SBIR testing
            ////    //Response.Redirect("oms_intermediatepage.aspx?s=" + Session["cwid"].ToString());

            ////    //------------------------ Get trainee_id from oms_kids db using the oms_id
            ////    //SqlConnection SQLConn2 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);

            ////    //SqlCommand cmd2 = new SqlCommand("oms_k12_getTID", SQLConn2);
            ////    //cmd2.CommandType = CommandType.StoredProcedure;

            ////    //SqlParameter Param2;
            ////    //Param2 = cmd2.Parameters.Add("@oms_id", SqlDbType.Int);
            ////    //Param2.Value = Convert.ToInt32(Session["omsid"]);
            ////    //Param2.Direction = ParameterDirection.Input;
            ////    //Param2 = cmd2.Parameters.Add("@trainee_id", SqlDbType.Int);
            ////    //Param2.Direction = ParameterDirection.Output;

            ////    //SQLConn2.Open();
            ////    //cmd2.ExecuteNonQuery();

            ////    //int traineeID = (int)cmd2.Parameters["@trainee_id"].Value;
            #endregion etco stuff

            //    //SQLConn2.Close();
            //    //------------------------
            //    //Response.Redirect("http://operationmentalscorpion.com//cwsa/omsk12/dev/omstrainer/CW.SA.DemoTestPage.html?tid=" + traineeID);
            //    //Response.Redirect("http://209.196.155.44//cwsa/omsk12/dev/omstrainer/CW.SA.DemoTestPage.html?tid=" + traineeID);
                int traineeID = (int)cmd.Parameters["@tid"].Value;
                //Response.Redirect("oms_select_group.htm?tid=" + cmd.Parameters["@tid"].Value);
                Response.Redirect("oms_select_group.htm?tid=" + traineeID);
            }
            else
            {
                //logcheck.Text = "<font color=\"FireBrick\">" + loginerr + "</font>";  //Invalid Login! Check your password and try again.   
            }
            #endregion config session


        }  // Login_Click()

    }
}
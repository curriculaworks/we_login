﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using CW.Tools;

namespace OMS.WebServices
{
    public partial class oms_ws : System.Web.Services.WebService
    {
        #region WebMethods, dev/poc ===================================================================================================

        // note: these methods are just for trying stuff out etc, and should be removed from prod web srvc
        // (e.g. by commenting the [WebMethod] attribute)

        #region OMSDev_login() =======================================================================================
        /// <summary>
        /// General-purpose "scratch" method for trying stuff out.
        /// </summary>
        /// <param name="tid"></param>
        /// <returns></returns>
        //[WebMethod]  // omsdev
        public string OMSDev_login(int tid)
        {

            //string sessionlink = "session.aspx?id=";
            //string enc_pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(pwd, "SHA1");

            SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd = new SqlCommand("oms_login", SQLConn);       //cw_login
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter Param;
            Param = cmd.Parameters.Add("@un", SqlDbType.VarChar, 25);
            Param.Value = "oms2"; //  Username.Text.ToLower();
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@pw", SqlDbType.VarChar, 25);
            Param.Value = "test2"; // Password.Text;
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@role_id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@fn", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@ln", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@tid", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@role_dscr", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@expdays", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@active", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@retval", SqlDbType.Int);
            Param.Direction = ParameterDirection.ReturnValue;

            SQLConn.Open();
            cmd.ExecuteNonQuery();
            SQLConn.Close();

            #region config session
            //if ((int)cmd.Parameters["@retval"].Value == 0)
            //{
            //    Session["omsid"] = cmd.Parameters["@id"].Value;
            //    Session["role"] = cmd.Parameters["@role_id"].Value;
            //    Session["name"] = Username.Text;
            //    Session["expdays"] = cmd.Parameters["@expdays"].Value;
            //    Session["tid"] = cmd.Parameters["@tid"].Value;

            //    int traineeID = (int)cmd.Parameters["@tid"].Value;

            //    //sessionlink += Convert.ToString(Session["cwid"]) + "&x=1";  //** for SBIR testing
            //    //Response.Redirect(sessionlink);                             //** for SBIR testing
            //    //Response.Redirect("oms_intermediatepage.aspx?s=" + Session["cwid"].ToString());

            //    //------------------------ Get trainee_id from oms_kids db using the oms_id
            //    //SqlConnection SQLConn2 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);

            //    //SqlCommand cmd2 = new SqlCommand("oms_k12_getTID", SQLConn2);
            //    //cmd2.CommandType = CommandType.StoredProcedure;

            //    //SqlParameter Param2;
            //    //Param2 = cmd2.Parameters.Add("@oms_id", SqlDbType.Int);
            //    //Param2.Value = Convert.ToInt32(Session["omsid"]);
            //    //Param2.Direction = ParameterDirection.Input;
            //    //Param2 = cmd2.Parameters.Add("@trainee_id", SqlDbType.Int);
            //    //Param2.Direction = ParameterDirection.Output;

            //    //SQLConn2.Open();
            //    //cmd2.ExecuteNonQuery();

            //    //int traineeID = (int)cmd2.Parameters["@trainee_id"].Value;

            //    //SQLConn2.Close();
            //    //------------------------
            //    //Response.Redirect("http://operationmentalscorpion.com//cwsa/omsk12/dev/omstrainer/CW.SA.DemoTestPage.html?tid=" + traineeID);
            //    //Response.Redirect("http://209.196.155.44//cwsa/omsk12/dev/omstrainer/CW.SA.DemoTestPage.html?tid=" + traineeID);
            //    Response.Redirect("oms_select_group.htm?tid=" + traineeID);
            //}
            //else
            //{
            //    logcheck.Text = "<font color=\"FireBrick\">" + loginerr + "</font>";  //Invalid Login! Check your password and try again.   
            //}
            #endregion config session

            return "";
        }  // OMSDev()
        #endregion OMSDev_login() =======================================================================================

        #region OMSDev_resetmission() =======================================================================================
        /// <summary>
        /// General-purpose "scratch" method for trying stuff out.
        /// </summary>
        /// <param name="tid"></param>
        /// <returns></returns>
        //[WebMethod]  omsarchived
        //public bool OMSDev_resetmission(int tid)
        //{
        //    GetConfig();

        //    string formatLogData = "";
        //    OMS_utils omswsLog = new OMS_utils();
        //    #region log
        //    if (isLogEnabled)
        //    {
        //        //** Write to log               
        //        formatLogData = "OMSDev_resetmission Input Values:  \r\n";
        //        formatLogData += "tid = " + tid;
        //        omswsLog.log(formatLogData);
        //    }
        //    #endregion log

        //    #region perform evaluation, update user session in db

        //    try  // talk to db
        //    {

        //        //**---------------------------------- Call oms_retrieveCurrentTable ----------------------------------
        //        #region oms_retrieveCurrentTable

        //        SqlConnection SQLConn5 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        //        SqlCommand cmd5 = new SqlCommand("oms_retrieveCurrentTable", SQLConn5);
        //        cmd5.CommandType = CommandType.StoredProcedure;

        //        SqlParameter Param5;
        //        Param5 = cmd5.Parameters.Add("@tid", SqlDbType.Int);
        //        Param5.Value = tid;
        //        Param5.Direction = ParameterDirection.Input;
        //        Param5 = cmd5.Parameters.Add("@top_Badge_Cnt", SqlDbType.Int);
        //        Param5.Direction = ParameterDirection.Output;
        //        Param5 = cmd5.Parameters.Add("@cur_Mission_Node_Idx", SqlDbType.Int);
        //        Param5.Direction = ParameterDirection.Output;
        //        Param5 = cmd5.Parameters.Add("@app_starttime", SqlDbType.DateTime);
        //        Param5.Direction = ParameterDirection.Output;
        //        Param5 = cmd5.Parameters.Add("@highest_n", SqlDbType.Int);
        //        Param5.Direction = ParameterDirection.Output;
        //        Param5 = cmd5.Parameters.Add("@training_duration", SqlDbType.Int);
        //        Param5.Direction = ParameterDirection.Output;
        //        Param5 = cmd5.Parameters.Add("@recentUnit_ts", SqlDbType.DateTime);
        //        Param5.Direction = ParameterDirection.Output;

        //        SQLConn5.Open();
        //        cmd5.ExecuteNonQuery();

        //        //int topBadgeCnt = (int)cmd5.Parameters["@top_Badge_Cnt"].Value;
        //        //int curMissioNode_Idx = (int)cmd5.Parameters["@cur_Mission_Node_Idx"].Value;
        //        //DateTime appStartTime = (DateTime)cmd5.Parameters["@app_starttime"].Value;
        //        //int highestN = (int)cmd5.Parameters["@highest_n"].Value;
        //        //int newTrainDuration = (int)cmd5.Parameters["@training_duration"].Value + gameDuration;
        //        //DateTime l_recentUnit_ts = (DateTime)cmd5.Parameters["@recentUnit_ts"].Value;

        //        SQLConn5.Close();
        //        #endregion



        //        //**---------------------------------- Call oms_updateCurrentTable ----------------------------------
        //        #region oms_updateCurrentTable

        //        SqlConnection SQLConn3 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        //        SqlCommand cmd3 = new SqlCommand("oms_updateCurrentTable", SQLConn3);
        //        cmd3.CommandType = CommandType.StoredProcedure;

        //        SqlParameter Param3;
        //        Param3 = cmd3.Parameters.Add("@tid", SqlDbType.Int);
        //        Param3.Value = tid;
        //        Param3.Direction = ParameterDirection.Input;
        //        Param3 = cmd3.Parameters.Add("@nextLevel_idx", SqlDbType.Int);
        //        Param3.Value = nextLevel_IDx;
        //        Param3.Direction = ParameterDirection.Input;
        //        Param3 = cmd3.Parameters.Add("@nextGroupIdx", SqlDbType.Int);
        //        Param3.Value = nextGroupIDx;
        //        Param3.Direction = ParameterDirection.Input;
        //        Param3 = cmd3.Parameters.Add("@nextUnitIdx", SqlDbType.Int);
        //        Param3.Value = nextUnitIDx;
        //        Param3.Direction = ParameterDirection.Input;
        //        Param3 = cmd3.Parameters.Add("@nextUnitId", SqlDbType.VarChar, 50);
        //        Param3.Value = nextUnitID;
        //        Param3.Direction = ParameterDirection.Input;
        //        //
        //        Param3 = cmd3.Parameters.Add("@currentBadge_id", SqlDbType.VarChar, 50);
        //        Param3.Value = earnedBadgeID;
        //        Param3.Direction = ParameterDirection.Input;
        //        Param3 = cmd3.Parameters.Add("@qtyBadge0", SqlDbType.Int);
        //        Param3.Value = qtyBadge0;
        //        Param3.Direction = ParameterDirection.Input;
        //        Param3 = cmd3.Parameters.Add("@qtyBadge1", SqlDbType.Int);
        //        Param3.Value = qtyBadge1;
        //        Param3.Direction = ParameterDirection.Input;
        //        Param3 = cmd3.Parameters.Add("@qtyBadge2", SqlDbType.Int);
        //        Param3.Value = qtyBadge2;
        //        Param3.Direction = ParameterDirection.Input;
        //        Param3 = cmd3.Parameters.Add("@qtyBadge3", SqlDbType.Int);
        //        Param3.Value = qtyBadge3;
        //        Param3.Direction = ParameterDirection.Input;
        //        //
        //        Param3 = cmd3.Parameters.Add("@top_Badge_Cnt", SqlDbType.Int);
        //        Param3.Value = topBadgeCnt;
        //        Param3.Direction = ParameterDirection.Input;
        //        Param3 = cmd3.Parameters.Add("@cur_Mission_Node_Idx", SqlDbType.Int);
        //        Param3.Value = curMissioNode_Idx;
        //        Param3.Direction = ParameterDirection.Input;
        //        //
        //        Param3 = cmd3.Parameters.Add("@intel_id", SqlDbType.VarChar, 50);
        //        Param3.Value = intel_id;
        //        Param3.Direction = ParameterDirection.Input;
        //        Param3 = cmd3.Parameters.Add("@mapPoint_id", SqlDbType.Int);
        //        Param3.Value = Convert.ToInt32(mapPoint_id);
        //        Param3.Direction = ParameterDirection.Input;
        //        Param3 = cmd3.Parameters.Add("@missionKey", SqlDbType.Int);
        //        Param3.Value = 0;
        //        Param3.Direction = ParameterDirection.Input;
        //        Param3 = cmd3.Parameters.Add("@storyline_id", SqlDbType.Int);
        //        Param3.Value = 0;
        //        Param3.Direction = ParameterDirection.Input;
        //        //
        //        Param3 = cmd3.Parameters.Add("@training_power", SqlDbType.Int);
        //        Param3.Value = trainingPower;
        //        Param3.Direction = ParameterDirection.Input;
        //        Param3 = cmd3.Parameters.Add("@highest_n", SqlDbType.Int);
        //        Param3.Value = highestN;
        //        Param3.Direction = ParameterDirection.Input;
        //        Param3 = cmd3.Parameters.Add("@training_duration", SqlDbType.Int);
        //        Param3.Value = newTrainDuration;
        //        Param3.Direction = ParameterDirection.Input;

        //        SQLConn3.Open();
        //        cmd3.ExecuteNonQuery();

        //        //** Excepthion handling: if update return code  <> 0, do something?

        //        SQLConn3.Close();
        //        #endregion

        //    }
        //    catch (Exception e)
        //    {
        //        //formatLogData = "Error Message = " + e.Message;
        //        //formatLogData += " StackTrace = " + e.StackTrace;
        //        //omswsLog.log(formatLogData);

        //        //return null;
        //    }
        //    #endregion perform evaluation, update user session in db


        //    return true;
        //}  // OMSDev_resetmission()
        #endregion OMSDev_resetmission() =======================================================================================

        #region OMSDev() =======================================================================================
        /// <summary>
        /// General-purpose "scratch" method for trying stuff out.
        /// </summary>
        /// <param name="tid"></param>
        /// <returns></returns>
        //[WebMethod]  // omsdev
        public string[] OMSDev(int tid)
        {

            List<string> retvals = new List<string>();

            string jsonstr = "";


            #region handle SOAP escapes
            {
                string jsonstrhtmlescaped = @"{&quot;accuracy_aud&quot;: 0.664341516834833, &quot;reaction_time_overall&quot;: 700, &quot;accuracy_overall&quot;: 0.64363677151003906, &quot;tid&quot;: 2, &quot;reaction_time_vis&quot;: 700, &quot;accuracy_vis&quot;: 0.62293202618524501, &quot;unit_id&quot;: &quot;L004G001N067&quot;, &quot;reaction_time_aud&quot;: 700}";
                string json_performance = HttpUtility.HtmlDecode(jsonstrhtmlescaped);

                #region decode arg
                // TODO: update error-checking
                UserPerformanceOnLastNBackWrapper perfWrapper = System.Web.Helpers.Json.Decode<UserPerformanceOnLastNBackWrapper>(json_performance);
                UserPerformanceOnLastNBack performance = new UserPerformanceOnLastNBack();  // init to keep compiler happy

                if (perfWrapper == null)
                {
                    // handle error
                }
                else
                {
                    performance = perfWrapper.performance;
                }
                brkpnt++;

                performance = System.Web.Helpers.Json.Decode<UserPerformanceOnLastNBack>(json_performance);
                brkpnt = 0;
                #endregion decode arg

            }
            #endregion handle SOAP escapes

            {
                UserPerformanceOnLastNBack performance = new UserPerformanceOnLastNBack();
                jsonstr = System.Web.Helpers.Json.Encode(new UserPerformanceOnLastNBackWrapper() { performance = performance });
                retvals.Add(jsonstr);
            }




            #region [commented] a few retval-strings leftover from cwauth
            //// perform actions of OMSCWAuth for kidsoms
            ////  --> this gives us the unit-id
            ////  --> for now we just simulate

            //retvals.Add(tid.ToString());
            //retvals.Add("Burton Fallin");
            //retvals.Add("u1-1-1");
            #endregion [commented] a few retval-strings leftover from cwauth


            string[] retvalv = retvals.ToArray();  // intermed. arry solely for debug
            return retvalv;
        }  // OMSDev()
        #endregion OMSDev() =======================================================================================

        #region OMSCWauth() =======================================================================================
        // note: borrowed from cw_oms_wd_K.sln for purpose of POC calling actual oms stored procedure (from Don's oms_kids db) from this web srvc

        //[WebMethod]  // omsdev
        //[TraceExtension]
        public string[] OMSCWauth(int tid)
        {
            ////** Write to log
            //OMS_utils omswsLog = new OMS_utils();

            //string formatLogData = "";
            //formatLogData = "OMSCWauth Input Values:  " + tid;
            //omswsLog.log(formatLogData);
            ////---------------------------------------------------------------------

            string[] retvals = new string[26];

            SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd = new SqlCommand("oms_cw_auth", SQLConn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter Param;
            Param = cmd.Parameters.Add("@tidinput", SqlDbType.Int);
            Param.Value = tid;
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@tid", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@f_name", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@l_name", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@currentBadge_id", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@qtyBadge0", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@qtyBadge1", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@qtyBadge2", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@qtyBadge3", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@level_idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@group_idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@unit_idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@unit_id", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@intel_id", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@top_Badge_Cnt", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@cur_Mission_Node_Idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@mapPoint_id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@missionKey", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@storyline_id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@timestampOfBadge", SqlDbType.DateTime);
            Param.Direction = ParameterDirection.Output;
            //
            Param = cmd.Parameters.Add("@training_power", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@highest_n", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@login_time", SqlDbType.DateTime);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@app_starttime", SqlDbType.DateTime);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@recentUnit_ts", SqlDbType.DateTime);
            Param.Direction = ParameterDirection.Output;

            //SQLConn.Open();
            try
            {
                SQLConn.Open();
            }
            catch (Exception e)
            {
                string errstr = e.ToString();
                errstr += ":brkpt";
            }

            cmd.ExecuteNonQuery();

            String currBadge = cmd.Parameters["@currentBadge_id"].Value.ToString();
            String qtyB0 = cmd.Parameters["@qtyBadge0"].Value.ToString();
            String qtyB1 = cmd.Parameters["@qtyBadge1"].Value.ToString();
            String qtyB2 = cmd.Parameters["@qtyBadge2"].Value.ToString();
            String qtyB3 = cmd.Parameters["@qtyBadge3"].Value.ToString();

            #region test1
            //DateTime tsOfBadge = Convert.ToDateTime(cmd.Parameters["@timestampOfBadge"].Value);
            //DateTime tsOfBadgeDate = tsOfBadge.Date;   //** get only date portion of the tsOfBadge
            //DateTime tsNow = DateTime.Now.Date;        //** get only date portion of current time
            //int compareTSResult = DateTime.Compare(tsOfBadgeDate, tsNow);

            //if (compareTSResult != 0)     //** if (badges expired)
            //{
            //    currBadge = "-1";         //** Set currentBadgeID = -1 and all earned qty's to 0;
            //    qtyB0 = "0";
            //    qtyB1 = "0";
            //    qtyB2 = "0";
            //    qtyB3 = "0";
            //}
            #endregion test1


            #region [update: OMS_utils now avail.] [commented because we don't have OMS_utils available] calculate applicable param values
            //** Determine Training Level  
            int trainingLevel;

            if (cmd.Parameters["@recentUnit_ts"].Value.ToString() != "")
            {
                DateTime l_recentUnit_ts = (DateTime)cmd.Parameters["@recentUnit_ts"].Value;
                OMS_utils omsTrainingLevel = new OMS_utils();
                omsTrainingLevel.determineTrainingLevel(l_recentUnit_ts);
                trainingLevel = omsTrainingLevel.trainLevel;
            }
            else
                trainingLevel = 0;

            //** Calculate Average Accuracy
            double avgAccuracy;
            OMS_utils omsCalAvgAccuracy = new OMS_utils();
            omsCalAvgAccuracy.calculateAvgAccuracy(tid);
            avgAccuracy = omsCalAvgAccuracy.avgAccuracy;

            //** Calculate Total Training Time
            int totalTrainingTime;
            OMS_utils omsTotalTrainingTime = new OMS_utils();
            omsTotalTrainingTime.calculateTotalTrainingTime(tid);
            totalTrainingTime = omsTotalTrainingTime.totalTrainingTime;

            //** Calculate Average Training Time
            int avgTrainingTime;
            OMS_utils omsAvgTrainingTime = new OMS_utils();
            omsAvgTrainingTime.calculateAvgTrainingTime(tid);
            avgTrainingTime = omsAvgTrainingTime.avgTrainingTime;
            //------------------
            #endregion [commented because we don't have OMS_utils available] calculate applicable param values

            retvals[0] = cmd.Parameters["@tid"].Value.ToString();
            retvals[1] = cmd.Parameters["@f_name"].Value.ToString();
            retvals[2] = cmd.Parameters["@l_name"].Value.ToString();
            retvals[3] = currBadge;
            retvals[4] = qtyB0;
            retvals[5] = qtyB1;
            retvals[6] = qtyB2;
            retvals[7] = qtyB3;
            retvals[8] = cmd.Parameters["@level_idx"].Value.ToString();
            retvals[9] = cmd.Parameters["@group_idx"].Value.ToString();
            retvals[10] = cmd.Parameters["@unit_idx"].Value.ToString();
            retvals[11] = cmd.Parameters["@unit_id"].Value.ToString();
            retvals[12] = cmd.Parameters["@intel_id"].Value.ToString();
            retvals[13] = cmd.Parameters["@top_Badge_Cnt"].Value.ToString();
            retvals[14] = cmd.Parameters["@cur_Mission_Node_Idx"].Value.ToString();
            retvals[15] = cmd.Parameters["@mapPoint_id"].Value.ToString();
            retvals[16] = cmd.Parameters["@missionKey"].Value.ToString();
            retvals[17] = cmd.Parameters["@storyline_id"].Value.ToString();
            retvals[18] = cmd.Parameters["@training_power"].Value.ToString();
            retvals[19] = Convert.ToString(trainingLevel);
            retvals[20] = cmd.Parameters["@highest_n"].Value.ToString();
            retvals[21] = Convert.ToString(avgAccuracy);
            retvals[22] = Convert.ToString(totalTrainingTime);
            retvals[23] = Convert.ToString(avgTrainingTime);
            retvals[24] = cmd.Parameters["@app_starttime"].Value.ToString();
            retvals[25] = cmd.Parameters["@recentUnit_ts"].Value.ToString();

            SQLConn.Close();
            return retvals;
        }

        #endregion OMSCWauth() =======================================================================================

        #region CWauth() =======================================================================================
        // note: borrowed from cw_staging.sln for purpose of POC calling a stored procedure from this web srvc
        //[WebMethod]  // omsdev
        //[TraceExtension]
        public string[] CWauth(string un, string pw, int sid)
        {
            string[] retvals = new string[11];

            SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd = new SqlCommand("cw_auth", SQLConn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter Param;
            Param = cmd.Parameters.Add("@un", SqlDbType.VarChar, 25);
            Param.Value = un;
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@pw", SqlDbType.VarChar, 25);
            Param.Value = pw;
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@sidinput", SqlDbType.Int);
            Param.Value = sid;
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@sid", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@unitname", SqlDbType.VarChar, 10);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@username", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@autoreward", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@rw_time", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@rw_andor", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@rw_units", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@rw_days", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@gamelinks", SqlDbType.VarChar, 25);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@reset", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;

            try
            {
                SQLConn.Open();
            }
            catch (Exception e)
            {
                string errstr = e.ToString();
                errstr += ":foo";
            }
            cmd.ExecuteNonQuery();

            retvals[0] = cmd.Parameters["@sid"].Value.ToString();
            retvals[1] = cmd.Parameters["@unitname"].Value.ToString();
            retvals[2] = "0";
            retvals[3] = cmd.Parameters["@username"].Value.ToString();
            retvals[4] = cmd.Parameters["@autoreward"].Value.ToString();
            retvals[5] = cmd.Parameters["@rw_time"].Value.ToString();
            retvals[6] = cmd.Parameters["@rw_andor"].Value.ToString();
            retvals[7] = cmd.Parameters["@rw_units"].Value.ToString();
            retvals[8] = cmd.Parameters["@rw_days"].Value.ToString();
            retvals[9] = cmd.Parameters["@gamelinks"].Value.ToString();
            retvals[10] = cmd.Parameters["@reset"].Value.ToString();
            //@reset=0/1
            SQLConn.Close();

            FormsAuthentication.SetAuthCookie(un.ToLower(), true);
            return retvals;
        }
        #endregion CWauth() =======================================================================================




        //[WebMethod]  // omsdev
        public string[] OMSInit_v00(int tid)
        {

            List<string> retvals = new List<string>();

            // perform actions of OMSCWAuth for kidsoms
            //  --> this gives us the unit-id
            //  --> for now we just simulate

            retvals.Add(tid.ToString());
            retvals.Add("Burton Fallin");
            retvals.Add("u1-1-1");


            string[] retvalv = retvals.ToArray();  // intermed. arry solely for debug
            return retvalv;
        }  // OMSInit()

        #endregion WebMethods, dev ===================================================================================================
    }
}
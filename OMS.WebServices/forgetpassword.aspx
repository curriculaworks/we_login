<%@ Page language="c#" Codebehind="forgetpassword.aspx.cs" AutoEventWireup="true" Inherits="CurriculaWorks.forgetpassword" %>
<html>
  <head>
    <title></title>
    <LINK href="styles/tw.css" type="text/css" rel="stylesheet">
</head>

<body>	
<form id="Form1" method="post" runat="server">
<div align="center" class="padleft padright">
  <table>
    <tr>
      <td>
        <asp:Panel ID="firstmessage" Runat="server" Visible="True" Width="400">
            <span id="forgotten" runat="server">If you have forgotten your password, enter your username here, and your password will be sent to the email account you registered with CurriculaWorks</span>
            <br /><br />
            <span id="unme" runat="server"> Username: </span>:&nbsp;&nbsp;
            <asp:TextBox id="username" runat="server"></asp:TextBox>
            <asp:button Runat="server" ID="submit" OnClick="sendemail" Text="Go"/><br/>
            <asp:HiddenField ID="SOURCE" runat="server" Value="CW" /><br/>
            <span id="help" runat="server">If you need more help, or don't remember your username, email</span>
            <a href="mailto:<%= support %>@curriculaworks.com"><%= support %>@curriculaworks.com</a> 
            <span id="assist" runat="server">for assistance.</span>
        </asp:Panel><br/>

        <asp:Label ID="badreply" Runat="server" Visible="False" ForeColor="FireBrick" Font-Bold="True">That username does not exist. Check for spelling errors.</asp:Label><br />

        <asp:Label ID="goodreply" Runat="server" Visible="False"></asp:Label>
      </td>
    </tr>
  </table>
</div>
</form>	

<script type="text/javascript">var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));</script><script type="text/javascript">var pageTracker = _gat._getTracker("UA-3509160-1");pageTracker._initData();pageTracker._trackPageview();</script></body>
</html>

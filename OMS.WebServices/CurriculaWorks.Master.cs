﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Globalization;
using System.Threading;
using System.Resources;

namespace CurriculaWorks
{
    public partial class cwmaster : System.Web.UI.MasterPage
    {
        public bool morkus = true;
        public string Title = "";
        public string Message = "";
        protected string LogInfo = "";
        public string ExpWarning = "";
        public string help1 = "";   //"images/rollovers/Help1.jpg";
        public string help2 = "";   //"images/rollovers/Help2.jpg";
        public string logout1 = "images/rollovers/LogOut1.jpg";
        public string logout2 = "images/rollovers/LogOut2.jpg";  
        public string purSubEngSpanToggle = "media/LoginPurchase.swf";     //use this toggle to load English/Spanish buttons for Login and Purchase a Subsription
        protected string Year = "2008";
        //public string Icon = "<img alt=\"CurriculaWorks Logo\" src=\"images/cwlogo.gif\" border=\"0\" width=\"130\" height=\"48\">";
        protected string Icon = "<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" width=\"130\" height=\"50\" id=\"FlashID\" title=\"Click here to return to the CurriculaWorks home page.\"><param name=\"movie\" value=\"media/Logo.swf\" /><!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. --><!--[if !IE]>--><object type=\"application/x-shockwave-flash\" data=\"media/Logo.swf\" width=\"130\" height=\"50\"><!--<![endif]--><param name=\"quality\" value=\"best\" /><param name=\"wmode\" value=\"opaque\" /><param name=\"swfversion\" value=\"9.0.45.0\" /><!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. --><param name=\"expressinstall\" value=\"scripts/expressInstall.swf\" /><!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. --><div><h4>Content on this page requires a newer version of Adobe Flash Player.</h4><p><a href=\"http://www.adobe.com/go/getflashplayer\"><img src=\"http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif\" alt=\"Get Adobe Flash player\" width=\"112\" height=\"33\" /></a></p></div><!--[if !IE]>--></object><!--<![endif]--></object>";

        //protected string HelpLink = "http://www.explodethecode.com/help/";
        protected string HelpLink = "http://curriculaworks.com/help/Explode_The_Code_Online_User_Guide.pdf";

        protected string mainmenu = "<a href=\"mainmenu.aspx\" onmouseover=\"document.mm.src='images/rollovers/MainMenu2.jpg'\" onmouseout=\"document.mm.src='images/rollovers/MainMenu1.jpg'\"><img src=\"images/rollovers/MainMenu1.jpg\" width=\"85\" height=\"18\" border=\"0\" name=\"mm\" /></a>";

        protected string studenthome = "\" onmouseover=\"document.mm.src='images/rollovers/StudentHome2.jpg'\" onmouseout=\"document.mm.src='images/rollovers/StudentHome1.jpg'\"><img src=\"images/rollovers/StudentHome1.jpg\" width=\"103\" height=\"18\" border=\"0\" name=\"mm\" /></a>";
        static ResourceManager rm;
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["SelectCulture"] == null) 
                Session["SelectCulture"] = "en-US";

            //Read the resources files
            String strResourcesPath = Server.MapPath("bin");
            rm = ResourceManager.CreateFileBasedResourceManager("resource", strResourcesPath, null);

            //if (Session["banner"] != null && Session["banner"].ToString() != "")
            //{
            //    Banner.Text = Session["banner"].ToString();
            //    pnlBanner.Visible = true;
            //}
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Year = DateTime.Now.Year.ToString();

            if ((Session["name"] != null) && (Session["roledscr"] != null) && (Session["rep_id"] != null))
            {
                /* if  ((Session["SelectCulture"] != null) && (Session["SelectCulture"].ToString() == "en-US"))
                    HELPbutton.Visible = true;                       //**display HELP button only when logged in and language preference is English.
                else
                {                                                       //* 4/7/2011 was width=103, height=17
                    studenthome = "\" onmouseover=\"document.mm.src='images/rollovers/StudentHome-Spanish2.jpg'\" onmouseout=\"document.mm.src='images/rollovers/StudentHome-Spanish1.jpg'\"><img src=\"images/rollovers/StudentHome-Spanish1.jpg\" width=\"103\" height=\"18\" border=\"0\" name=\"mm\" /></a>"; //**display Spanish "Student Home" button
                        
                    logout1 = "images/rollovers/LogOut_Spanish1.jpg";
                    logout2 = "images/rollovers/LogOut_Spanish2.jpg";
                } */

                //LogInfo = "<span class=\"r\">Logged&nbsp;in:&nbsp;<b><font color=\"SteelBlue\">" + Convert.ToString(Session["name"]) + "</font></b></span>&nbsp;<br/>";
                //LogInfo += "<span class=\"r\">Role:&nbsp;<b><font color=\"SteelBlue\">" + Convert.ToString(Session["roledscr"]) + "</font></b></span>&nbsp;";
                LogInfo = "<span class=\"r\">" + rm.GetString("0580") + "&nbsp;<b><font color=\"SteelBlue\">" + Convert.ToString(Session["name"]) + "</font></b></span>&nbsp;<br/>";
                LogInfo += "<span class=\"r\">" + rm.GetString("0581") + "&nbsp;<b><font color=\"SteelBlue\">";
                if (Convert.ToString(Session["roledscr"]) == "Student")
                    LogInfo += rm.GetString("0582");
                else
                    LogInfo += Convert.ToString(Session["roledscr"]);
                LogInfo += "</font></b></span>&nbsp;";

                if (Session["role"].ToString() == "0")
                {
                    LogInfo += "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" class=\"tan\"><tr><td class=\"padtop padbottom\" align=\"right\">" + 
                               "<a href=\"logout.aspx\" onmouseover=\"document.lo.src='" + logout2 + "'\" onmouseout=\"document.lo.src='" + logout1 + 
                               "'\"><img src=\"" + logout1 + "\" width=\"61\" height=\"18\" border=\"0\" name=\"lo\" /></a></td><td>&nbsp;</td></tr></table>";
                }
                else if (Session["role"].ToString() == "1")
                {
                    LogInfo += "<table cellspacing=\"0\" cellpadding=\"0\" class=\"tan\"><tr><td class=\"padtop padbottom\"><a href=\"rptStudent.aspx?s=" + 
                               Session["cwid"].ToString() + studenthome + "</td><td class=\"padtop padbottom\">" + 
                               "<a href=\"logout.aspx\" onmouseover=\"document.lo.src='" + logout2 + 
                               "'\" onmouseout=\"document.lo.src='" + logout1 + "'\"><img src=\"" + logout1 + 
                               "\" width=\"61\" height=\"18\" border=\"0\" name=\"lo\" /></a></td><td>&nbsp;</td></tr></table>";
                }
                else
                    LogInfo += "<table cellspacing=\"0\" cellpadding=\"0\" class=\"tan\"><tr><td class=\"padtop padbottom\">" + mainmenu + 
                               "</td><td class=\"padtop padbottom\">" + "<a href=\"logout.aspx\" onmouseover=\"document.lo.src='" + logout2 + 
                               "'\" onmouseout=\"document.lo.src='" + logout1 + "'\"><img src=\"" + logout1 + 
                               "\" width=\"61\" height=\"18\" border=\"0\" name=\"lo\" /></a></td><td>&nbsp;</td></tr></table>";

                if (Session["rep_id"].ToString() == "7")
                {
                    Icon = "<a href=\"http://www.ExplodeTheCode.com\" class=\"t\"><img alt=\"ETC Online Logo\" src=\"images/etclogo3.jpg\" border=\"0\" width=\"194\" height=\"33\"></a>";
                    HelpLink = "http://www.explodethecode.com/help/";
                }

                //loggedintext.Text = LogInfo;
                //loggedinpanel.Visible = true;
                //flashlogin.Visible = false;


                if (Session["expdays"] != null && Session["expdays"] != DBNull.Value && morkus && (int)Session["role"]<6)
                    if (Convert.ToInt32(Session["expdays"]) < -365)
                    {
                        Response.Redirect("logout.aspx");
                    }
                    else if (Convert.ToInt32(Session["expdays"]) < -30)
                    {
                        warning.Visible = true;
                        ExpWarning = "<div class=\"expr\">Your subscription has now expired. Your students do not have access to Student Activities, but you can still access the Summary Report until " + DateTime.Today.AddDays(365 - Convert.ToInt32(Math.Abs((int)Session["expdays"]))).ToShortDateString() + ". After this date, you will not be able to access the Summary Report information for your students.<br/><br/>To renew, please contact Customer Service at <a href=\"mailto:ExplodeTheCode@schoolspecialty.com\">ExplodeTheCode@schoolspecialty.com</a> or call 800.225.5750.</div>";
                    }
                    else if (Convert.ToInt32(Session["expdays"]) < 0)
                    {
                        warning.Visible = true;
                        if (Session["subscr_type"] != null && (int)Session["subscr_type"] == 3)
                            ExpWarning = "<div class=\"expr\">Your Free Trial has expired. We hope you have found <i>Explode The Code Online's</i> data-driven instruction and robust reporting features helpful to your phonics instruction.<br/>To purchase a license, please contact Customer Service at <a href=\"mailto:ExplodeTheCode@schoolspecialty.com\">ExplodeTheCode@schoolspecialty.com</a> or call 800.225.5750.</div>";
                        else
                            ExpWarning = "<div class=\"expr\">Your subscription has expired. We hope you have found <i>Explode The Code Online</i> useful in preparing your students for reading success. While you are planning your renewal, access for these seats has been extended until " + DateTime.Today.AddDays(30 - Convert.ToInt32(Math.Abs((int)Session["expdays"]))).ToShortDateString() + ". To renew, please contact Customer Service at <a href=\"mailto:ExplodeTheCode@schoolspecialty.com\">ExplodeTheCode@schoolspecialty.com</a> or call 800.225.5750.</div>";
                    }
                    else if ((Convert.ToInt32(Session["expdays"]) < 10) && Session["subscr_type"] != null && (int)Session["subscr_type"] == 3)
                    {
                        ExpWarning = "<div class=\"expr\">Your Free Trial will expire on " + DateTime.Today.AddDays(Convert.ToInt32(Math.Abs((int)Session["expdays"]))).ToShortDateString() + ". To purchase a license, please contact Customer Service at <a href=\"mailto:ExplodeTheCode@schoolspecialty.com\">ExplodeTheCode@schoolspecialty.com</a> or call 800.225.5750.</div>";
                    }
                    else if (Convert.ToInt32(Session["expdays"]) < 61)
                    {
                        warning.Visible = true;
                        ExpWarning = "<div class=\"expr\">Your subscription will expire on " + DateTime.Today.AddDays(Convert.ToInt32(Math.Abs((int)Session["expdays"]))).ToShortDateString() + ". To renew, please contact Customer Service at <a href=\"mailto:ExplodeTheCode@schoolspecialty.com\">ExplodeTheCode@schoolspecialty.com</a> or call 800.225.5750.</div>";
                    }
                /*if ((Session["role"] != DBNull.Value && Session["taken"] != DBNull.Value && Session["seats"] != DBNull.Value) &&
                    ((int)Session["role"] > 1 && (int)Session["taken"] > (int)Session["seats"])
                    )
                {
                    warning.Visible = true;
                    ExpWarning += "<p class=\"expr\">Your license allows <span class=\"redemph\">" + Session["seats"].ToString() + "</span> active students. You have now exceeded that limit. Please purchase additional seats or inactivate student(s). To purchase additional seats, please contact Customer Service at <a href=\"mailto:ExplodeTheCode@schoolspecialty.com\">ExplodeTheCode@schoolspecialty.com</a> or call 800.225.5750.</p>"; 
                }*/

            }
            else
            {
                //flashlogin.Visible = true;
                //loggedinpanel.Visible = false;

                //LogInfo = "<br/><br/><span class=\"r\"><a href=\"login.aspx\">Members<br/>log in here!</a></span>";
            }
        }   //** end "if ((Session["name"] != null) "
    }  //** end Page Load
}

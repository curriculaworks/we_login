﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using CW.Tools;

namespace OMS.WebServices
{
    public partial class oms_ws : System.Web.Services.WebService
    {

        protected bool isAuth(HttpRequest request)
        {
            return isAuth(request, omswsLog: null, wsMethodID: "");
        }
        protected bool isAuth(HttpRequest request, OMS_utils omswsLog, string wsMethodID)
        {
            // forbid remote access
            string urlref = request.UrlReferrer.Host;
            //return request.UrlReferrer.Host == "localhost";
            bool isAuthorized = request.UrlReferrer.Authority.StartsWith("localhost") ||
                request.UrlReferrer.Authority.StartsWith("209.196.155.42");

            #region log unauthorized access attempt
            if (!isAuthorized && omswsLog !=null)
            {
                string formatLogData = "";
                #region log
                if (isLogEnabledForAdmin)
                {
                    //** Write to log               
                    formatLogData = "UNAUTHORIZED ACCESS ATTEMPT on method "+wsMethodID+"  \r\n";
                    omswsLog.log(formatLogData);
                }
                #endregion log
            }
            #endregion

            return isAuthorized;
        }

        #region WebMethods > prod > oms-admin =================================================================================

        #region OMSGenerateUserAccounts() =======================================================================================

        class UserDescriptor
        {
            public string un = "";
            public string pw = "";
            public string namef = "";
            public string namel = "";
            public string email = "";

            public int OMSRunMode = 1;  // Motivators ON
            public int NBackThemeID = 0;  // Navy
        }


        [WebMethod]  // omsadmin
        public string OMSGenerateUserAccountsFromList(string pathUserInfo)
        {
            #region notes and tips
            // tip: The underlying stored procedure - oms_k12_insertuser - returns the tid if a user with the specified arg info
            //      already exists. Therefore, as a nice perk, OMSGenerateUserAccountsFromList can be used to look up tid's for a 
            //      given set of un/pw's, and to generate userconfig lists for existing users.
            #endregion

            string formatLogData = "";
            OMS_utils omswsLog = new OMS_utils("log_newusers");
            string wsMethodID = "OMSGenerateUserAccountsFromList";
            if (!isAuth(HttpContext.Current.Request,omswsLog,wsMethodID)) return "";

            //int arg = 4321;  // tmp for compiler
            GetWSConfig();


            #region load userinfo from unified userinfo file
            if (cwtrue)
            {
                #region log
                if (isLogEnabledForAdmin)
                {
                    //** Write to log               
                    formatLogData = wsMethodID + " Input Values:  \r\n";
                    formatLogData += "pathUserInfo = " + pathUserInfo;
                    omswsLog.log(formatLogData);
                }
                #endregion log

                //if (arg != 6831) return "operation failed";  // TODO: fix this


                // load file pathUserInfo
                // foreach dataline
                //  tokv = tokenize dataline;
                //  perform any checks, complete defaults etc
                //  create new user

                List<string> outputlogs = new List<string>();
                List<string> outputinfo = new List<string>();

                #region read in config data from external config file: user config info

                string pathNewUserInfo = "";
                string omsRunModeID = "";
                string nbackThemeID = "";

                {
                    List<string> datalines = CW.Tools.FileIO.LoadDataLines(PathNewUserConfig);
                    foreach (string dataline in datalines)
                    {
                        string[] tokv = CWTools.CWSplit(record: dataline, delims: "=", trimWhitespace: true);
                        if (tokv.Length < 2) continue;
                        string tokkey = tokv[0];
                        string tokval = tokv[1];
                        #region store token values in OMSConfig
                        // TODO: make this more robust  
                        {
                            if (false) { }

                            else if (tokkey == "pathNewUserInfo")
                            {
                                pathNewUserInfo = BaseConfig + tokval;
                            }
                            else if (tokkey == "OMSRunModeID")
                            {
                                omsRunModeID = tokval;
                            }
                            else if (tokkey == "nbackThemeID")
                            {
                                nbackThemeID = tokval;
                            }

                        }  // for i<tokc
                        #endregion
                    }  // foreach dataline
                }

                #endregion  // get config info


                #region open conn to db
                SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
                SqlCommand cmd = new SqlCommand("oms_k12_userInfoInsert", SQLConn);
                cmd.CommandType = CommandType.StoredProcedure;
                SQLConn.Open();
                #endregion
                #region iterate new users
                {
                    List<string> datalines = CW.Tools.FileIO.LoadDataLines(pathNewUserInfo);
                    foreach (string dataline in datalines)
                    {
                        string[] tokv = CWTools.CWSplit(record: dataline, delims: ",", trimWhitespace: true);
                        if (tokv.Length < 4) continue;

                        // local vars to pass to stored procedure
                        int cw_sid = 0; // 113;  // non-relevant orphan (?) from bridge to ETCO
                        string un = tokv[0];
                        string pw = tokv[1];
                        string fname = tokv[2];
                        string lname = tokv[3];
                        string email = ""; // @"lynn@curriculaworks.com";

                        #region perform stored procedure call

                        #region args/returns from stored procedure (commented)
                        //(@cw_sid Int,
                        // @un	VARCHAR(25),
                        // @pw	VARCHAR(25),
                        // @fname VARCHAR(50),
                        // @lname VARCHAR(50),
                        // @email VARCHAR(50),
                        // @trainee_id  INT OUTPUT)
                        #endregion args/returns from stored procedure (commented)
                        bool isSuccess = true;
                        #region set up SqlParameters and execute query
                        cmd.Parameters.Clear();
                        SqlParameter Param;

                        Param = cmd.Parameters.Add("@cw_sid", SqlDbType.Int);
                        Param.Value = cw_sid;  // null;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@un", SqlDbType.VarChar, 25);
                        Param.Value = un;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@pw", SqlDbType.VarChar, 25);
                        Param.Value = pw;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@fname", SqlDbType.VarChar, 50);
                        Param.Value = fname;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@lname", SqlDbType.VarChar, 50);
                        Param.Value = lname;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@email", SqlDbType.VarChar, 50);
                        Param.Value = email;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@trainee_id", SqlDbType.Int);
                        Param.Direction = ParameterDirection.Output;

                        //SQLConn.Open();
                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            isSuccess = false;
                        }
                        #endregion set up Sqlparams and execute query

                        #region collect output values
                        string trainee_id = "NULL_TRAINEE_ID";
                        if (isSuccess)
                        {
                            trainee_id = cmd.Parameters["@trainee_id"].Value.ToString();
                        }
                        #endregion collect output values

                        #endregion perform stored procedure call


                        string sep = "; ";
                        string outputlog = trainee_id + sep + un + sep + pw + sep + fname + sep + lname;
                        outputlogs.Add(outputlog);

                        sep = ",";
                        outputlog = "u" + sep + trainee_id + sep + un + sep + omsRunModeID + sep + nbackThemeID;
                        outputinfo.Add(outputlog);


                    }  // foreach dataline
                }

                #region old
                if (!cwtrue)
                //{
                //    int usernameseriesEnd = usernameseriesStart + userc;
                //    int pwidx = 0;
                //    foreach (string username in usernames)
                //    {
                //        for (int i = usernameseriesStart; i < usernameseriesEnd; i++)
                //        {
                //            // local vars to pass to stored procedure
                //            int cw_sid = 113;  // non-relevant orphan
                //            string usernumber = i.ToString("D3");
                //            string un = username + usernumber;
                //            string pw = passwords[pwidx++];
                //            string fname = username;
                //            string lname = usernumber;
                //            string email = @"lynn@curriculaworks.com";


                //            #region perform stored procedure call

                //            #region args/returns from stored procedure (commented)
                //            //(@cw_sid Int,
                //            // @un	VARCHAR(25),
                //            // @pw	VARCHAR(25),
                //            // @fname VARCHAR(50),
                //            // @lname VARCHAR(50),
                //            // @email VARCHAR(50),
                //            // @trainee_id  INT OUTPUT)
                //            #endregion args/returns from stored procedure (commented)
                //            bool isSuccess = true;
                //            #region set up SqlParameters and execute query
                //            cmd.Parameters.Clear();
                //            SqlParameter Param;

                //            Param = cmd.Parameters.Add("@cw_sid", SqlDbType.Int);
                //            Param.Value = cw_sid;
                //            Param.Direction = ParameterDirection.Input;

                //            Param = cmd.Parameters.Add("@un", SqlDbType.VarChar, 25);
                //            Param.Value = un;
                //            Param.Direction = ParameterDirection.Input;

                //            Param = cmd.Parameters.Add("@pw", SqlDbType.VarChar, 25);
                //            Param.Value = pw;
                //            Param.Direction = ParameterDirection.Input;

                //            Param = cmd.Parameters.Add("@fname", SqlDbType.VarChar, 50);
                //            Param.Value = fname;
                //            Param.Direction = ParameterDirection.Input;

                //            Param = cmd.Parameters.Add("@lname", SqlDbType.VarChar, 50);
                //            Param.Value = lname;
                //            Param.Direction = ParameterDirection.Input;

                //            Param = cmd.Parameters.Add("@email", SqlDbType.VarChar, 50);
                //            Param.Value = email;
                //            Param.Direction = ParameterDirection.Input;

                //            Param = cmd.Parameters.Add("@trainee_id", SqlDbType.Int);
                //            Param.Direction = ParameterDirection.Output;

                //            //SQLConn.Open();
                //            try
                //            {
                //                cmd.ExecuteNonQuery();
                //            }
                //            catch (Exception e)
                //            {
                //                isSuccess = false;
                //            }
                //            #endregion set up Sqlparams and execute query

                //            #region collect output values
                //            string trainee_id = "NULL_TRAINEE_ID";
                //            if (isSuccess)
                //            {
                //                trainee_id = cmd.Parameters["@trainee_id"].Value.ToString();
                //            }
                //            #endregion collect output values

                //            #endregion perform stored procedure call


                //            string sep = "; ";
                //            string outputlog = trainee_id + sep + un + sep + pw + sep + fname + sep + lname;
                //            outputlogs.Add(outputlog);
                //        }  // for i < userc
                //    } // for usernames
                //}
                #endregion old
                #endregion iterate new users
                #region close conn to db
                SQLConn.Close();
                #endregion

                #region log
                if (isLogEnabledForAdmin)
                {
                    //** Write to log               
                    formatLogData = "OMSGenerateUserAccounts Return:  \r\n";
                    foreach (string str in outputlogs)
                        formatLogData += "\r\n     " + str;
                    formatLogData += "\r\n\r\n";
                    formatLogData += "   OMSGenerateUserAccounts userconfig:  \r\n";
                    foreach (string str in outputinfo)
                        formatLogData += "\r\n     " + str;

                    omswsLog.log(formatLogData);
                }
                #endregion log

            }  // if cwtrue
            #endregion load from unified userinfo file



            #region [disabled] load userinfo from separate lists for un, pw
            if (!cwtrue)
            {
                #region log
                if (isLogEnabledForAdmin)
                {
                    //** Write to log               
                    formatLogData = "OMSGenerateUserAccountsFromList Input Values:  \r\n";
                    formatLogData += "pathUserInfo = " + pathUserInfo;
                    omswsLog.log(formatLogData);
                }
                #endregion log

                //if (arg != 6831) return "operation failed";  // TODO: fix this


                // load file pathUserInfo
                // foreach dataline
                //  tokv = tokenize dataline;
                //  perform any checks, complete defaults etc
                //  create new user

                List<string> usernames = new List<string>(); // { "lynn", "chuck" };
                //usernames.Add("test02user");
                usernames.Add("calvin");
                usernames.Add("chuck");
                usernames.Add("deedee");
                usernames.Add("lynn");
                usernames.Add("marina");
                usernames.Add("rich");
                int usernameseriesStart = 100;  // startno. for usernameset e.g. chuck100
                int userc = 0;
                List<string> outputlogs = new List<string>();

                string pathpw = "";
                List<string> passwords = new List<string>();
                //List<string> passwords = CW.Tools.FileIO.LoadDataLines("");

                #region input "generate useracct" config data from external config file: user info
                //unames = n; alpha; omega
                //# pathun = omsaccts/newusers.txt
                //userctsta = 0
                //userc = 2

                //pathpw = omsaccts/pwd.txt

                #region load config
                List<string> datalines = CW.Tools.FileIO.LoadDataLines(PathNewUserConfig);
                foreach (string dataline in datalines)
                {
                    string[] tokv = CWTools.CWSplit(record: dataline, delims: "=", trimWhitespace: true);
                    if (tokv.Length < 2) continue;
                    string tokkey = tokv[0];
                    string tokval = tokv[1];
                    #region store token values in OMSConfig
                    // TODO: make this more robust  
                    {
                        if (false) { }

                        else if (tokkey == "unames")
                        {
                            usernames = new List<string>(CWTools.Tokenize(tokval));
                            usernames.RemoveAt(0);  // remove parse key
                        }
                        else if (tokkey == "userctsta")
                        {
                            int.TryParse(tokval, out usernameseriesStart);
                        }
                        else if (tokkey == "userc")
                        {
                            int.TryParse(tokval, out userc);
                        }
                        else if (tokkey == "pathpw")
                        {
                            pathpw = BaseConfig + tokval;
                        }
                    }  // for i<tokc
                    #endregion
                }  // foreach dataline
                #endregion load config

                #region load password list
                passwords = CW.Tools.FileIO.LoadDataLines(pathpw);
                {
                    // if needed, pad passwords with default
                    if (passwords.Count == 0) passwords.Add("go");
                    int pwc = usernames.Count * userc;
                    for (int i = passwords.Count; i < pwc; i++)
                        passwords.Add(passwords[0]);
                }
                #endregion

                #endregion  // get config info



                #region open conn to db
                SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
                SqlCommand cmd = new SqlCommand("oms_k12_userInfoInsert", SQLConn);
                cmd.CommandType = CommandType.StoredProcedure;
                SQLConn.Open();
                #endregion
                #region iterate new users
                int usernameseriesEnd = usernameseriesStart + userc;
                int pwidx = 0;
                foreach (string username in usernames)
                {
                    for (int i = usernameseriesStart; i < usernameseriesEnd; i++)
                    {
                        // local vars to pass to stored procedure
                        int cw_sid = 113;  // non-relevant orphan
                        string usernumber = i.ToString("D3");
                        string un = username + usernumber;
                        string pw = passwords[pwidx++];
                        string fname = username;
                        string lname = usernumber;
                        string email = @"lynn@curriculaworks.com";


                        #region perform stored procedure call

                        #region args/returns from stored procedure (commented)
                        //(@cw_sid Int,
                        // @un	VARCHAR(25),
                        // @pw	VARCHAR(25),
                        // @fname VARCHAR(50),
                        // @lname VARCHAR(50),
                        // @email VARCHAR(50),
                        // @trainee_id  INT OUTPUT)
                        #endregion args/returns from stored procedure (commented)
                        bool isSuccess = true;
                        #region set up SqlParameters and execute query
                        cmd.Parameters.Clear();
                        SqlParameter Param;

                        Param = cmd.Parameters.Add("@cw_sid", SqlDbType.Int);
                        Param.Value = cw_sid;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@un", SqlDbType.VarChar, 25);
                        Param.Value = un;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@pw", SqlDbType.VarChar, 25);
                        Param.Value = pw;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@fname", SqlDbType.VarChar, 50);
                        Param.Value = fname;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@lname", SqlDbType.VarChar, 50);
                        Param.Value = lname;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@email", SqlDbType.VarChar, 50);
                        Param.Value = email;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@trainee_id", SqlDbType.Int);
                        Param.Direction = ParameterDirection.Output;

                        //SQLConn.Open();
                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            isSuccess = false;
                        }
                        #endregion set up Sqlparams and execute query

                        #region collect output values
                        string trainee_id = "NULL_TRAINEE_ID";
                        if (isSuccess)
                        {
                            trainee_id = cmd.Parameters["@trainee_id"].Value.ToString();
                        }
                        #endregion collect output values

                        #endregion perform stored procedure call


                        string outputlog = trainee_id + "; " + un + "; " + pw + "; " + fname + "; " + lname;
                        outputlogs.Add(outputlog);
                    }  // for i < userc
                } // for usernames
                #endregion iterate new users
                #region close conn to db
                SQLConn.Close();
                #endregion

                #region log
                if (isLogEnabledForAdmin)
                {
                    //** Write to log               
                    formatLogData = "OMSResetMission Return:  \r\n";
                    foreach (string str in outputlogs)
                        formatLogData += "\r\n  " + str;
                    omswsLog.log(formatLogData);
                }
                #endregion log

            }  // if cwtrue
            #endregion load from separate lists for un, pw


            #region [disabled] work-in-progress: generate sets of user accounts for upcoming research study
            if (!cwtrue)
            {
                //string formatLogData = "";
                //OMS_utils omswsLog = new OMS_utils();
                //#region log
                //if (isLogEnabledForAdmin)
                //{
                //    //** Write to log               
                //    formatLogData = "OMSGenerateUserAccounts Input Values:  \r\n";
                //    formatLogData += "tid = " + arg;
                //    omswsLog.log(formatLogData);
                //}
                //#endregion log

                #region algorithm
                // - read in List<string> passwords from file (e.g. generated by passwordmaker)
                // - read in config: prefixes for MOTON, MOTOFF, MOTSEL
                // - create datastructure for user-info: username, nameFirst, nameLast
                // - generate List<UserDescr> users for users to be created;
                // - foreach (UserDescr user in users)
                //   {
                //       - call stored procedure oms_k12_userInfoInsert to generate user
                //   }

                // stored procedure:
                //   ALTER PROCEDURE [dbo].[oms_k12_userInfoInsert]
                //           (@cw_sid Int,
                //            @un	VARCHAR(25),
                //            @pw	VARCHAR(25),
                //            @fname VARCHAR(50),
                //            @lname VARCHAR(50),
                //            @email VARCHAR(50),
                //            @trainee_id  INT OUTPUT)
                #endregion algorithm

                // TODO: input passwords from file
                List<string> passwords = new List<string>();
                int passwordIdx = 0;

                // TODO: input values from config file
                bool isProceedWithDBUpdates = false;  // we are disable-able from our config file
                // username-prefixes for settings-groups
                string prefMotivatorsONN = "monn"; // "omega";
                string prefMotivatorsOFF = "moff"; // "alpha";
                string prefMotivatorsSEL = "";

                int newuserc = 3;  // qty acct's per category

                // TODO: perform validity checking: is passwords.Count sufficient for req. qty new acct's?

                List<UserDescriptor> users = new List<UserDescriptor>();
                List<string> unPrefixes = new List<string>() { prefMotivatorsONN, prefMotivatorsOFF, };

                // generate batches of sequentially numbered accounts
                foreach (string pref in unPrefixes)
                {
                    for (int i = 0; i < newuserc; i++)
                    {
                        string usernmbr = i.ToString("D3");
                        users.Add(new UserDescriptor()
                        {
                            un = pref + usernmbr,
                            pw = passwords[passwordIdx++],
                            namef = pref

                        });
                    }
                }
            }  // if false
            #endregion [disabled] work-in-progress: generate sets of user accounts for upcoming research study



            return "action completed";
        }  // OMSGenerateUserAccountsFromList(pathUserInfo)


        [WebMethod]  // omsadmin
        public string OMSGenerateUserAccountSets(int arg)
        {
            if (!isAuth(HttpContext.Current.Request)) return "";

            GetWSConfig();


            if (cwtrue)
            {
                string formatLogData = "";
                OMS_utils omswsLog = new OMS_utils("log_newusers");
                #region log
                if (isLogEnabledForAdmin)
                {
                    //** Write to log               
                    formatLogData = "OMSGenerateUserAccounts Input Values:  \r\n";
                    formatLogData += "arg = " + arg;
                    omswsLog.log(formatLogData);
                }
                #endregion log

                if (arg != 6831) return "operation failed";  // TODO: fix this


                List<string> usernames = new List<string>(); // { "lynn", "chuck" };
                //usernames.Add("test02user");
                usernames.Add("calvin");
                usernames.Add("chuck");
                usernames.Add("deedee");
                usernames.Add("lynn");
                usernames.Add("marina");
                usernames.Add("rich");
                int usernameseriesStart = 100;  // startno. for usernameset e.g. chuck100
                int userc = 0;
                List<string> outputlogs = new List<string>();

                string pathpw = "";
                List<string> passwords = new List<string>();
                //List<string> passwords = CW.Tools.FileIO.LoadDataLines("");

                #region input "generate useracct" config data from external config file: user info
                //unames = n; alpha; omega
                //# pathun = omsaccts/newusers.txt
                //userctsta = 0
                //userc = 2

                //pathpw = omsaccts/pwd.txt

                #region load config
                List<string> datalines = CW.Tools.FileIO.LoadDataLines(PathNewUserConfig);
                foreach (string dataline in datalines)
                {
                    string[] tokv = CWTools.CWSplit(record: dataline, delims: "=", trimWhitespace: true);
                    if (tokv.Length < 2) continue;
                    string tokkey = tokv[0];
                    string tokval = tokv[1];
                    #region store token values in OMSConfig
                    // TODO: make this more robust  
                    {
                        if (false) { }

                        else if (tokkey == "unames")
                        {
                            usernames = new List<string>(CWTools.Tokenize(tokval));
                            usernames.RemoveAt(0);  // remove parse key
                        }
                        else if (tokkey == "userctsta")
                        {
                            int.TryParse(tokval, out usernameseriesStart);
                        }
                        else if (tokkey == "userc")
                        {
                            int.TryParse(tokval, out userc);
                        }
                        else if (tokkey == "pathpw")
                        {
                            pathpw = BaseConfig + tokval;
                        }
                    }  // for i<tokc
                    #endregion
                }  // foreach dataline
                #endregion load config

                #region load password list
                passwords = CW.Tools.FileIO.LoadDataLines(pathpw);
                {
                    // if needed, pad passwords with default
                    if (passwords.Count == 0) passwords.Add("go");
                    int pwc = usernames.Count * userc;
                    for (int i = passwords.Count; i < pwc; i++)
                        passwords.Add(passwords[0]);
                }
                #endregion

                #endregion  // get config info



                #region open conn to db
                SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
                SqlCommand cmd = new SqlCommand("oms_k12_userInfoInsert", SQLConn);
                cmd.CommandType = CommandType.StoredProcedure;
                SQLConn.Open();
                #endregion
                #region iterate new users
                int usernameseriesEnd = usernameseriesStart + userc;
                int pwidx = 0;
                foreach (string username in usernames)
                {
                    for (int i = usernameseriesStart; i < usernameseriesEnd; i++)
                    {
                        // local vars to pass to stored procedure
                        int cw_sid = 113;  // non-relevant orphan
                        string usernumber = i.ToString("D3");
                        string un = username + usernumber;
                        string pw = passwords[pwidx++];
                        string fname = username;
                        string lname = usernumber;
                        string email = @"lynn@curriculaworks.com";


                        #region perform stored procedure call

                        #region args/returns from stored procedure (commented)
                        //(@cw_sid Int,
                        // @un	VARCHAR(25),
                        // @pw	VARCHAR(25),
                        // @fname VARCHAR(50),
                        // @lname VARCHAR(50),
                        // @email VARCHAR(50),
                        // @trainee_id  INT OUTPUT)
                        #endregion args/returns from stored procedure (commented)
                        bool isSuccess = true;
                        #region set up SqlParameters and execute query
                        cmd.Parameters.Clear();
                        SqlParameter Param;

                        Param = cmd.Parameters.Add("@cw_sid", SqlDbType.Int);
                        Param.Value = cw_sid;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@un", SqlDbType.VarChar, 25);
                        Param.Value = un;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@pw", SqlDbType.VarChar, 25);
                        Param.Value = pw;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@fname", SqlDbType.VarChar, 50);
                        Param.Value = fname;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@lname", SqlDbType.VarChar, 50);
                        Param.Value = lname;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@email", SqlDbType.VarChar, 50);
                        Param.Value = email;
                        Param.Direction = ParameterDirection.Input;

                        Param = cmd.Parameters.Add("@trainee_id", SqlDbType.Int);
                        Param.Direction = ParameterDirection.Output;

                        //SQLConn.Open();
                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            isSuccess = false;
                        }
                        #endregion set up Sqlparams and execute query

                        #region collect output values
                        string trainee_id = "NULL_TRAINEE_ID";
                        if (isSuccess)
                        {
                            trainee_id = cmd.Parameters["@trainee_id"].Value.ToString();
                        }
                        #endregion collect output values

                        #endregion perform stored procedure call


                        string outputlog = trainee_id + "; " + un + "; " + pw + "; " + fname + "; " + lname;
                        outputlogs.Add(outputlog);
                    }  // for i < userc
                } // for usernames
                #endregion iterate new users
                #region close conn to db
                SQLConn.Close();
                #endregion

                #region log
                if (isLogEnabledForAdmin)
                {
                    //** Write to log               
                    formatLogData = "OMSResetMission Return:  \r\n";
                    foreach (string str in outputlogs)
                        formatLogData += "\r\n  " + str;
                    omswsLog.log(formatLogData);
                }
                #endregion log

            }  // if cwtrue


            #region [disabled] work-in-progress: generate sets of user accounts for upcoming research study
            if (!cwtrue)
            {
                string formatLogData = "";
                OMS_utils omswsLog = new OMS_utils();
                #region log
                if (isLogEnabledForAdmin)
                {
                    //** Write to log               
                    formatLogData = "OMSGenerateUserAccounts Input Values:  \r\n";
                    formatLogData += "tid = " + arg;
                    omswsLog.log(formatLogData);
                }
                #endregion log

                #region algorithm
                // - read in List<string> passwords from file (e.g. generated by passwordmaker)
                // - read in config: prefixes for MOTON, MOTOFF, MOTSEL
                // - create datastructure for user-info: username, nameFirst, nameLast
                // - generate List<UserDescr> users for users to be created;
                // - foreach (UserDescr user in users)
                //   {
                //       - call stored procedure oms_k12_userInfoInsert to generate user
                //   }

                // stored procedure:
                //   ALTER PROCEDURE [dbo].[oms_k12_userInfoInsert]
                //           (@cw_sid Int,
                //            @un	VARCHAR(25),
                //            @pw	VARCHAR(25),
                //            @fname VARCHAR(50),
                //            @lname VARCHAR(50),
                //            @email VARCHAR(50),
                //            @trainee_id  INT OUTPUT)
                #endregion algorithm

                // TODO: input passwords from file
                List<string> passwords = new List<string>();
                int passwordIdx = 0;

                // TODO: input values from config file
                bool isProceedWithDBUpdates = false;  // we are disable-able from our config file
                // username-prefixes for settings-groups
                string prefMotivatorsONN = "monn"; // "omega";
                string prefMotivatorsOFF = "moff"; // "alpha";
                string prefMotivatorsSEL = "";

                int newuserc = 3;  // qty acct's per category

                // TODO: perform validity checking: is passwords.Count sufficient for req. qty new acct's?

                List<UserDescriptor> users = new List<UserDescriptor>();
                List<string> unPrefixes = new List<string>() { prefMotivatorsONN, prefMotivatorsOFF, };

                // generate batches of sequentially numbered accounts
                foreach (string pref in unPrefixes)
                {
                    for (int i = 0; i < newuserc; i++)
                    {
                        string usernmbr = i.ToString("D3");
                        users.Add(new UserDescriptor()
                        {
                            un = pref + usernmbr,
                            pw = passwords[passwordIdx++],
                            namef = pref

                        });
                    }
                }
            }  // if false
            #endregion [disabled] work-in-progress: generate sets of user accounts for upcoming research study


            #region [disabled] leftovers for ref (delete when done)
            if (!cwtrue)
            {
                if (!isCallDB)
                {
                    return System.Web.Helpers.Json.Encode(new OMSSessionWrapper() { oms_session = new OMSSession() });
                }

                #region retrieve session data (user settings and stored state) from db
                string[] retvals = new string[26];

                SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
                SqlCommand cmd = new SqlCommand("oms_cw_auth", SQLConn);
                cmd.CommandType = CommandType.StoredProcedure;

                #region set up SqlParameters and execute query
                SqlParameter Param;
                Param = cmd.Parameters.Add("@tidinput", SqlDbType.Int);
                Param.Value = arg;
                Param.Direction = ParameterDirection.Input;
                Param = cmd.Parameters.Add("@tid", SqlDbType.Int);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@f_name", SqlDbType.VarChar, 50);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@l_name", SqlDbType.VarChar, 50);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@currentBadge_id", SqlDbType.VarChar, 50);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@qtyBadge0", SqlDbType.Int);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@qtyBadge1", SqlDbType.Int);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@qtyBadge2", SqlDbType.Int);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@qtyBadge3", SqlDbType.Int);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@level_idx", SqlDbType.Int);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@group_idx", SqlDbType.Int);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@unit_idx", SqlDbType.Int);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@unit_id", SqlDbType.VarChar, 50);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@intel_id", SqlDbType.VarChar, 50);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@top_Badge_Cnt", SqlDbType.Int);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@cur_Mission_Node_Idx", SqlDbType.Int);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@mapPoint_id", SqlDbType.Int);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@missionKey", SqlDbType.Int);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@storyline_id", SqlDbType.Int);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@timestampOfBadge", SqlDbType.DateTime);
                Param.Direction = ParameterDirection.Output;
                //
                Param = cmd.Parameters.Add("@training_power", SqlDbType.Int);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@highest_n", SqlDbType.Int);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@login_time", SqlDbType.DateTime);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@app_starttime", SqlDbType.DateTime);
                Param.Direction = ParameterDirection.Output;
                Param = cmd.Parameters.Add("@recentUnit_ts", SqlDbType.DateTime);
                Param.Direction = ParameterDirection.Output;

                SQLConn.Open();
                cmd.ExecuteNonQuery();
                #endregion set up Sqlparams

                #region [old badge stuff]
                String currBadge = cmd.Parameters["@currentBadge_id"].Value.ToString();
                String qtyB0 = cmd.Parameters["@qtyBadge0"].Value.ToString();
                String qtyB1 = cmd.Parameters["@qtyBadge1"].Value.ToString();
                String qtyB2 = cmd.Parameters["@qtyBadge2"].Value.ToString();
                String qtyB3 = cmd.Parameters["@qtyBadge3"].Value.ToString();
                #endregion [old badge stuff]

                #region [commented] test1
                //DateTime tsOfBadge = Convert.ToDateTime(cmd.Parameters["@timestampOfBadge"].Value);
                //DateTime tsOfBadgeDate = tsOfBadge.Date;   //** get only date portion of the tsOfBadge
                //DateTime tsNow = DateTime.Now.Date;        //** get only date portion of current time
                //int compareTSResult = DateTime.Compare(tsOfBadgeDate, tsNow);

                //if (compareTSResult != 0)     //** if (badges expired)
                //{
                //    currBadge = "-1";         //** Set currentBadgeID = -1 and all earned qty's to 0;
                //    qtyB0 = "0";
                //    qtyB1 = "0";
                //    qtyB2 = "0";
                //    qtyB3 = "0";
                //}
                #endregion test1


                #region [update: OMS_utils now avail.] [commented because we don't have OMS_utils available] calculate applicable param values
                //** Determine Training Level  
                int trainingLevel;

                if (cmd.Parameters["@recentUnit_ts"].Value.ToString() != "")
                {
                    DateTime l_recentUnit_ts = (DateTime)cmd.Parameters["@recentUnit_ts"].Value;
                    OMS_utils omsTrainingLevel = new OMS_utils();
                    omsTrainingLevel.determineTrainingLevel(l_recentUnit_ts);
                    trainingLevel = omsTrainingLevel.trainLevel;
                }
                else
                    trainingLevel = 0;

                //** Calculate Average Accuracy
                double avgAccuracy;
                OMS_utils omsCalAvgAccuracy = new OMS_utils();
                omsCalAvgAccuracy.calculateAvgAccuracy(arg);
                avgAccuracy = omsCalAvgAccuracy.avgAccuracy;

                //** Calculate Total Training Time
                int totalTrainingTime;
                OMS_utils omsTotalTrainingTime = new OMS_utils();
                omsTotalTrainingTime.calculateTotalTrainingTime(arg);
                totalTrainingTime = omsTotalTrainingTime.totalTrainingTime;

                //** Calculate Average Training Time
                int avgTrainingTime;
                OMS_utils omsAvgTrainingTime = new OMS_utils();
                omsAvgTrainingTime.calculateAvgTrainingTime(arg);
                avgTrainingTime = omsAvgTrainingTime.avgTrainingTime;
                //------------------
                #endregion [commented because we don't have OMS_utils available] calculate applicable param values


                #region output to intermediate datastructure dbValues
                OMS.Tools.NamedObjects dbValues = new Tools.NamedObjects();

                dbValues.Add("userdisplayname_first", cmd.Parameters["@f_name"].Value.ToString());
                dbValues.Add("userdisplayname_last", cmd.Parameters["@l_name"].Value.ToString());

                #region badges

                #region test exception
                //string strval = "";
                //int intval = 987654;

                //dbValues.Add("intval", intval);
                //strval = cmd.Parameters["@currentBadge_id"].Value.ToString();  // = "-1"
                //intval = int.Parse(cmd.Parameters["@currentBadge_id"].Value.ToString());  // works
                ////intval = (int)cmd.Parameters["@currentBadge_id"].Value;  // throws exception (although casting to int works elsewhere; does it not like neg. vals?? [update: yep, apparently not :P]

                //dbValues.Add("strcurrent_badge_id", strval);
                //dbValues.Add("intcurrent_badge_id", intval);
                #endregion test exception

                dbValues.Add("current_badge_id", int.Parse(cmd.Parameters["@currentBadge_id"].Value.ToString()));  // see above "test"

                int badgeTypec = 4;
                int[] earnedBadgeCounts = new int[badgeTypec];
                for (int i = 0; i < badgeTypec; i++)
                {
                    string badgeCounterNameDB = "@qtyBadge" + i;
                    string badgeCounterNameJSON = "badge_qty_" + i;
                    dbValues.Add(badgeCounterNameJSON, int.Parse(cmd.Parameters[badgeCounterNameDB].Value.ToString()));
                }
                #endregion

                dbValues.Add("way_point_idx", int.Parse(cmd.Parameters["@cur_Mission_Node_Idx"].Value.ToString()));

                #region user-performance to date
                dbValues.Add("perf_training_power", (int)cmd.Parameters["@training_power"].Value);
                dbValues.Add("perf_training_level", trainingLevel);  // int

                dbValues.Add("perf_total_training_time", totalTrainingTime);  // int
                dbValues.Add("perf_avg_training_time", avgTrainingTime);  // int

                dbValues.Add("perf_avg_accuracy", avgAccuracy);  // double

                dbValues.Add("perf_highest_n", (int)cmd.Parameters["@highest_n"].Value);
                dbValues.Add("perf_current_game_level", (int)cmd.Parameters["@level_idx"].Value);
                #endregion user-performance to date

                #endregion output to intermediate datastructure dbValues

                #region [obsolete] output to retvals[]: values still to-be-added to class OMSSession (see below) <-- TODO
                #region
                #endregion
                retvals[0] = cmd.Parameters["@tid"].Value.ToString();                       // omitted in dbValues
                retvals[1] = cmd.Parameters["@f_name"].Value.ToString();
                retvals[2] = cmd.Parameters["@l_name"].Value.ToString();
                retvals[3] = currBadge;
                retvals[4] = qtyB0;
                retvals[5] = qtyB1;
                retvals[6] = qtyB2;
                retvals[7] = qtyB3;
                retvals[8] = cmd.Parameters["@level_idx"].Value.ToString();
                retvals[9] = cmd.Parameters["@group_idx"].Value.ToString();                 // omitted in dbValues
                retvals[10] = cmd.Parameters["@unit_idx"].Value.ToString();                 // omitted in dbValues
                retvals[11] = cmd.Parameters["@unit_id"].Value.ToString();                  // omitted in dbValues
                retvals[12] = cmd.Parameters["@intel_id"].Value.ToString();                 // omitted in dbValues
                retvals[13] = cmd.Parameters["@top_Badge_Cnt"].Value.ToString();            // omitted in dbValues
                retvals[14] = cmd.Parameters["@cur_Mission_Node_Idx"].Value.ToString();
                retvals[15] = cmd.Parameters["@mapPoint_id"].Value.ToString();              // omitted in dbValues
                retvals[16] = cmd.Parameters["@missionKey"].Value.ToString();               // omitted in dbValues
                retvals[17] = cmd.Parameters["@storyline_id"].Value.ToString();             // omitted in dbValues
                retvals[18] = cmd.Parameters["@training_power"].Value.ToString();
                retvals[19] = Convert.ToString(trainingLevel);
                retvals[20] = cmd.Parameters["@highest_n"].Value.ToString();
                retvals[21] = Convert.ToString(avgAccuracy);
                retvals[22] = Convert.ToString(totalTrainingTime);
                retvals[23] = Convert.ToString(avgTrainingTime);
                retvals[24] = cmd.Parameters["@app_starttime"].Value.ToString();            // omitted in dbValues
                retvals[25] = cmd.Parameters["@recentUnit_ts"].Value.ToString();            // omitted in dbValues
                #endregion output to retvals[]

                SQLConn.Close();
                #endregion retrieve session data

                #region enter retrieved user data into json data structure
                OMSSession session = new OMSSession();

                session.user_name.first = dbValues["userdisplayname_first"] as string;
                session.user_name.last = dbValues["userdisplayname_last"] as string;

                #region session.earned_badges
                session.earned_badges.current_badge_id = (int)dbValues["current_badge_id"];
                {
                    int j = 0;
                    session.earned_badges.badge_qty_0 = (int)dbValues["badge_qty_" + j++];
                    session.earned_badges.badge_qty_1 = (int)dbValues["badge_qty_" + j++];
                    session.earned_badges.badge_qty_2 = (int)dbValues["badge_qty_" + j++];
                    session.earned_badges.badge_qty_3 = (int)dbValues["badge_qty_" + j++];
                }
                #endregion session.earned_badges

                #region session.performance
                session.performance.training_power = (int)dbValues["perf_training_power"];
                session.performance.training_level = (int)dbValues["perf_training_level"];

                session.performance.total_training_time = (int)dbValues["perf_total_training_time"];
                session.performance.avg_training_time = (int)dbValues["perf_avg_training_time"];

                session.performance.avg_accuracy = (double)dbValues["perf_avg_accuracy"];

                session.performance.highest_n = (int)dbValues["perf_highest_n"];
                session.performance.current_game_level = (int)dbValues["perf_current_game_level"];

                #endregion session.performance

                #endregion enter retrieved user data into json data structure

                string jsonstr = System.Web.Helpers.Json.Encode(new OMSSessionWrapper() { oms_session = session });


                string formatLogData = "";
                OMS_utils omswsLog = new OMS_utils();
                #region log
                if (isLogEnabledForAdmin)
                {
                    //** Write to log               
                    formatLogData = "OMSInit Return:  \r\n";
                    formatLogData += jsonstr;
                    omswsLog.log(formatLogData);
                }
                #endregion log

                return jsonstr;
            }
            #endregion [disabled] leftovers for ref (delete when done)

            return "action completed";
        }  // OMSGenerateUserAccounts(arg)


        #endregion OMSGenerateUserAccounts() =======================================================================================

        #region OMSResetMission() =======================================================================================
        [WebMethod]  // omswsadmin
        public string OMSResetMission(int tid)
        {
            GetWSConfig();

            string formatLogData = "";
            OMS_utils omswsLog = new OMS_utils();
            #region log
            if (isLogEnabledForAdmin)
            {
                //** Write to log               
                formatLogData = "OMSResetMission Input Values:  \r\n";
                formatLogData += "tid = " + tid;
                omswsLog.log(formatLogData);
            }
            #endregion log

            #region perform reset
            SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd = new SqlCommand("oms_reset_trainee_data", SQLConn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter Param;
            Param = cmd.Parameters.Add("@tid", SqlDbType.Int);
            Param.Value = tid;
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@retval", SqlDbType.Int);
            Param.Direction = ParameterDirection.ReturnValue;

            SQLConn.Open();

            cmd.ExecuteNonQuery();
            int retval = (int)cmd.Parameters["@retval"].Value;

            SQLConn.Close();
            #endregion perform reset

            #region log
            if (isLogEnabledForAdmin)
            {
                //** Write to log               
                formatLogData = "OMSResetMission Return:  \r\n";
                formatLogData += "retval = " + retval;
                omswsLog.log(formatLogData);
            }
            #endregion log

            return "ok";
        }  // OMSResetMission
        #endregion OMSResetMission() =======================================================================================

        #region OMSResetProgressForStudy() =======================================================================================
        /// <summary>
        /// Reset tid to beginning of Study-2013-04 by setting completed-units counter to zero.
        /// Notes: 
        ///   - this only blocks the auto-logout when end-of-study is reached; no data is cleared
        ///   - due to coding of available stored procedures, we also must overwrite all fields for user's row, so we set defaults,
        ///     de facto resetting the account (except for stored unit performance data)
        /// </summary>
        /// <param name="tid"></param>
        /// <returns></returns>
        [WebMethod]  // omswsadmin
        public string OMSResetProgressForStudy(int tid)
        {
            // TODO: retrieve and update tid's entry in current_table

            // notes:
            //  - cf OMSProcessUserPerformanceData, ./login.aspx.cs
            //  - ALERT: we missappropriate current_table field /top_Badge_Cnt/ (topBadgeCnt) as our counter for unitscompleted
            
            GetWSConfig();

            string mthdName = "OMSResetProgressForStudy";
            string formatLogData = "";
            OMS_utils omswsLog = new OMS_utils();
            #region log
            if (isLogEnabledForAdmin)
            {
                //** Write to log               
                formatLogData = mthdName + " Input Values:  \r\n";
                formatLogData += "tid = " + tid;
                omswsLog.log(formatLogData);
            }
            #endregion log

            #region perform reset

            #region prepare target values (other params set to defaults)
            int topBadgeCnt = 0;  // ALERT: misappropriated for use as completedUnitsCount
            int curMissioNode_Idx = -1;
            #endregion prep

            #region oms_updateCurrentTable

            SqlConnection SQLConn3 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd3 = new SqlCommand("oms_updateCurrentTable", SQLConn3);
            cmd3.CommandType = CommandType.StoredProcedure;

            SqlParameter Param3;
            Param3 = cmd3.Parameters.Add("@tid", SqlDbType.Int);
            Param3.Value = tid;
            Param3.Direction = ParameterDirection.Input;

            #region other params
            Param3 = cmd3.Parameters.Add("@nextLevel_idx", SqlDbType.Int);
            Param3.Value = 1;
            Param3.Direction = ParameterDirection.Input;
            Param3 = cmd3.Parameters.Add("@nextGroupIdx", SqlDbType.Int);
            Param3.Value = 1;
            Param3.Direction = ParameterDirection.Input;
            Param3 = cmd3.Parameters.Add("@nextUnitIdx", SqlDbType.Int);
            Param3.Value = 1;
            Param3.Direction = ParameterDirection.Input;
            Param3 = cmd3.Parameters.Add("@nextUnitId", SqlDbType.VarChar, 50);
            Param3.Value = "1";
            Param3.Direction = ParameterDirection.Input;
            //
            Param3 = cmd3.Parameters.Add("@currentBadge_id", SqlDbType.VarChar, 50);
            Param3.Value = -1;
            Param3.Direction = ParameterDirection.Input;
            Param3 = cmd3.Parameters.Add("@qtyBadge0", SqlDbType.Int);
            Param3.Value = 0;
            Param3.Direction = ParameterDirection.Input;
            Param3 = cmd3.Parameters.Add("@qtyBadge1", SqlDbType.Int);
            Param3.Value = 0;
            Param3.Direction = ParameterDirection.Input;
            Param3 = cmd3.Parameters.Add("@qtyBadge2", SqlDbType.Int);
            Param3.Value = 0;
            Param3.Direction = ParameterDirection.Input;
            Param3 = cmd3.Parameters.Add("@qtyBadge3", SqlDbType.Int);
            Param3.Value = 0;
            Param3.Direction = ParameterDirection.Input;
            //
            Param3 = cmd3.Parameters.Add("@top_Badge_Cnt", SqlDbType.Int);
            Param3.Value = topBadgeCnt;
            Param3.Direction = ParameterDirection.Input;
            //
            Param3 = cmd3.Parameters.Add("@cur_Mission_Node_Idx", SqlDbType.Int);
            Param3.Value = curMissioNode_Idx;
            Param3.Direction = ParameterDirection.Input;
            //
            Param3 = cmd3.Parameters.Add("@intel_id", SqlDbType.VarChar, 50);
            Param3.Value = "";
            Param3.Direction = ParameterDirection.Input;
            Param3 = cmd3.Parameters.Add("@mapPoint_id", SqlDbType.Int);
            Param3.Value = Convert.ToInt32("-1");
            Param3.Direction = ParameterDirection.Input;
            Param3 = cmd3.Parameters.Add("@missionKey", SqlDbType.Int);
            Param3.Value = 0;
            Param3.Direction = ParameterDirection.Input;
            Param3 = cmd3.Parameters.Add("@storyline_id", SqlDbType.Int);
            Param3.Value = 0;
            Param3.Direction = ParameterDirection.Input;
            //
            Param3 = cmd3.Parameters.Add("@training_power", SqlDbType.Int);
            Param3.Value = 0;
            Param3.Direction = ParameterDirection.Input;
            Param3 = cmd3.Parameters.Add("@highest_n", SqlDbType.Int);
            Param3.Value = 0;
            Param3.Direction = ParameterDirection.Input;
            Param3 = cmd3.Parameters.Add("@training_duration", SqlDbType.Int);
            Param3.Value = 0;
            Param3.Direction = ParameterDirection.Input;
            #endregion [commented] other params

            SQLConn3.Open();
            cmd3.ExecuteNonQuery();

            string retval = "SUCCESS: progress reset for tid="+tid;

            //** Excepthion handling: if update return code  <> 0, do something?

            SQLConn3.Close();
            #endregion


            #endregion perform reset

            #region log
            if (isLogEnabledForAdmin)
            {
                //** Write to log               
                formatLogData = mthdName + " Return:  \r\n";
                formatLogData += "retval = "+retval;
                omswsLog.log(formatLogData);
            }
            #endregion log

            return retval;
        }  // OMSResetProgressForStudy
        #endregion OMSResetProgressForStudy() =======================================================================================

        #endregion WebMethods > prod > oms-admin ==============================================================================


        #region WebMethods > dev > oms-admin =================================================================================

        #region OMSInit_test() =======================================================================================

        // used to test functionality of published web service w/o affecting values stored in db

        //[WebMethod]  // omsws_test
        public string OMSInit_test(int tid)
        {
            GetWSConfig();

            string mthdName = "OMSInit_test";
            string formatLogData = "";
            OMS_utils omswsLog = new OMS_utils();
            #region log
            if (isLogEnabledForAdmin)
            {
                //** Write to log               
                formatLogData = mthdName + " Input Values:  \r\n";
                formatLogData += "tid = " + tid;
                omswsLog.log(formatLogData);
            }
            #endregion log

            if (!isCallDB)
            {
                return System.Web.Helpers.Json.Encode(new OMSSessionWrapper() { oms_session = new OMSSession() });
            }

            #region retrieve session data (user settings and stored state) from db
            string[] retvals = new string[26];

            SqlConnection SQLConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            SqlCommand cmd = new SqlCommand("oms_cw_auth", SQLConn);
            cmd.CommandType = CommandType.StoredProcedure;

            #region set up SqlParameters
            SqlParameter Param;
            Param = cmd.Parameters.Add("@tidinput", SqlDbType.Int);
            Param.Value = tid;
            Param.Direction = ParameterDirection.Input;
            Param = cmd.Parameters.Add("@tid", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@f_name", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@l_name", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@currentBadge_id", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@qtyBadge0", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@qtyBadge1", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@qtyBadge2", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@qtyBadge3", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@level_idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@group_idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@unit_idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@unit_id", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@intel_id", SqlDbType.VarChar, 50);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@top_Badge_Cnt", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@cur_Mission_Node_Idx", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@mapPoint_id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@missionKey", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@storyline_id", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@timestampOfBadge", SqlDbType.DateTime);
            Param.Direction = ParameterDirection.Output;
            //
            Param = cmd.Parameters.Add("@training_power", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@highest_n", SqlDbType.Int);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@login_time", SqlDbType.DateTime);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@app_starttime", SqlDbType.DateTime);
            Param.Direction = ParameterDirection.Output;
            Param = cmd.Parameters.Add("@recentUnit_ts", SqlDbType.DateTime);
            Param.Direction = ParameterDirection.Output;
            #endregion set up Sqlparams

            #region execute query
            SQLConn.Open();
            cmd.ExecuteNonQuery();
            brkpnt = 45;
            #endregion execute query

            #region [old badge stuff]
            String currBadge = cmd.Parameters["@currentBadge_id"].Value.ToString();
            String qtyB0 = cmd.Parameters["@qtyBadge0"].Value.ToString();
            String qtyB1 = cmd.Parameters["@qtyBadge1"].Value.ToString();
            String qtyB2 = cmd.Parameters["@qtyBadge2"].Value.ToString();
            String qtyB3 = cmd.Parameters["@qtyBadge3"].Value.ToString();
            #endregion [old badge stuff]

            #region [commented] test1
            //DateTime tsOfBadge = Convert.ToDateTime(cmd.Parameters["@timestampOfBadge"].Value);
            //DateTime tsOfBadgeDate = tsOfBadge.Date;   //** get only date portion of the tsOfBadge
            //DateTime tsNow = DateTime.Now.Date;        //** get only date portion of current time
            //int compareTSResult = DateTime.Compare(tsOfBadgeDate, tsNow);

            //if (compareTSResult != 0)     //** if (badges expired)
            //{
            //    currBadge = "-1";         //** Set currentBadgeID = -1 and all earned qty's to 0;
            //    qtyB0 = "0";
            //    qtyB1 = "0";
            //    qtyB2 = "0";
            //    qtyB3 = "0";
            //}
            #endregion test1


            #region [update: OMS_utils now avail.] [commented because we don't have OMS_utils available] calculate applicable param values
            //** Determine Training Level  
            int trainingLevel;

            if (cmd.Parameters["@recentUnit_ts"].Value.ToString() != "")
            {
                DateTime l_recentUnit_ts = (DateTime)cmd.Parameters["@recentUnit_ts"].Value;
                OMS_utils omsTrainingLevel = new OMS_utils();
                omsTrainingLevel.determineTrainingLevel(l_recentUnit_ts);
                trainingLevel = omsTrainingLevel.trainLevel;
            }
            else
                trainingLevel = 0;

            //** Calculate Average Accuracy
            double avgAccuracy;
            OMS_utils omsCalAvgAccuracy = new OMS_utils();
            omsCalAvgAccuracy.calculateAvgAccuracy(tid);
            avgAccuracy = omsCalAvgAccuracy.avgAccuracy;

            //** Calculate Total Training Time
            int totalTrainingTime;
            OMS_utils omsTotalTrainingTime = new OMS_utils();
            omsTotalTrainingTime.calculateTotalTrainingTime(tid);
            totalTrainingTime = omsTotalTrainingTime.totalTrainingTime;

            //** Calculate Average Training Time
            int avgTrainingTime;
            OMS_utils omsAvgTrainingTime = new OMS_utils();
            omsAvgTrainingTime.calculateAvgTrainingTime(tid);
            avgTrainingTime = omsAvgTrainingTime.avgTrainingTime;
            //------------------
            #endregion [commented because we don't have OMS_utils available] calculate applicable param values


            #region output to intermediate datastructure dbValues
            OMS.Tools.NamedObjects dbValues = new Tools.NamedObjects();

            dbValues.Add("userdisplayname_first", cmd.Parameters["@f_name"].Value.ToString());
            dbValues.Add("userdisplayname_last", cmd.Parameters["@l_name"].Value.ToString());

            #region badges

            #region test exception
            //string strval = "";
            //int intval = 987654;

            //dbValues.Add("intval", intval);
            //strval = cmd.Parameters["@currentBadge_id"].Value.ToString();  // = "-1"
            //intval = int.Parse(cmd.Parameters["@currentBadge_id"].Value.ToString());  // works
            ////intval = (int)cmd.Parameters["@currentBadge_id"].Value;  // throws exception (although casting to int works elsewhere; does it not like neg. vals?? [update: yep, apparently not :P]

            //dbValues.Add("strcurrent_badge_id", strval);
            //dbValues.Add("intcurrent_badge_id", intval);
            #endregion test exception

            dbValues.Add("current_badge_id", int.Parse(cmd.Parameters["@currentBadge_id"].Value.ToString()));  // see above "test"

            int badgeTypec = 4;
            int[] earnedBadgeCounts = new int[badgeTypec];
            for (int i = 0; i < badgeTypec; i++)
            {
                string badgeCounterNameDB = "@qtyBadge" + i;
                string badgeCounterNameJSON = "badge_qty_" + i;
                dbValues.Add(badgeCounterNameJSON, int.Parse(cmd.Parameters[badgeCounterNameDB].Value.ToString()));
            }
            #endregion

            dbValues.Add("way_point_idx", int.Parse(cmd.Parameters["@cur_Mission_Node_Idx"].Value.ToString()));

            #region user-performance to date
            {
                string str = "";

                int level_idx = 0;
                str = cmd.Parameters["@level_idx"].Value as string;
                if (!int.TryParse(str, out level_idx)) level_idx = 0;

                dbValues.Add("perf_training_power", (int)cmd.Parameters["@training_power"].Value);
                dbValues.Add("perf_training_level", trainingLevel);  // int

                dbValues.Add("perf_total_training_time", totalTrainingTime);  // int
                dbValues.Add("perf_avg_training_time", avgTrainingTime);  // int

                dbValues.Add("perf_avg_accuracy", avgAccuracy);  // double

                dbValues.Add("perf_highest_n", (int)cmd.Parameters["@highest_n"].Value);
                dbValues.Add("perf_current_game_level", level_idx);
                brkpnt = 44;
            }
            #endregion user-performance to date

            #endregion output to intermediate datastructure dbValues

            #region [obsolete] output to retvals[]: values still to-be-added to class OMSSession (see below) <-- TODO
            #region
            #endregion
            retvals[0] = cmd.Parameters["@tid"].Value.ToString();                       // omitted in dbValues
            retvals[1] = cmd.Parameters["@f_name"].Value.ToString();
            retvals[2] = cmd.Parameters["@l_name"].Value.ToString();
            retvals[3] = currBadge;
            retvals[4] = qtyB0;
            retvals[5] = qtyB1;
            retvals[6] = qtyB2;
            retvals[7] = qtyB3;
            retvals[8] = cmd.Parameters["@level_idx"].Value.ToString();
            retvals[9] = cmd.Parameters["@group_idx"].Value.ToString();                 // omitted in dbValues
            retvals[10] = cmd.Parameters["@unit_idx"].Value.ToString();                 // omitted in dbValues
            retvals[11] = cmd.Parameters["@unit_id"].Value.ToString();                  // omitted in dbValues
            retvals[12] = cmd.Parameters["@intel_id"].Value.ToString();                 // omitted in dbValues
            retvals[13] = cmd.Parameters["@top_Badge_Cnt"].Value.ToString();            // omitted in dbValues
            retvals[14] = cmd.Parameters["@cur_Mission_Node_Idx"].Value.ToString();
            retvals[15] = cmd.Parameters["@mapPoint_id"].Value.ToString();              // omitted in dbValues
            retvals[16] = cmd.Parameters["@missionKey"].Value.ToString();               // omitted in dbValues
            retvals[17] = cmd.Parameters["@storyline_id"].Value.ToString();             // omitted in dbValues
            retvals[18] = cmd.Parameters["@training_power"].Value.ToString();
            retvals[19] = Convert.ToString(trainingLevel);
            retvals[20] = cmd.Parameters["@highest_n"].Value.ToString();
            retvals[21] = Convert.ToString(avgAccuracy);
            retvals[22] = Convert.ToString(totalTrainingTime);
            retvals[23] = Convert.ToString(avgTrainingTime);
            retvals[24] = cmd.Parameters["@app_starttime"].Value.ToString();            // omitted in dbValues
            retvals[25] = cmd.Parameters["@recentUnit_ts"].Value.ToString();            // omitted in dbValues
            #endregion output to retvals[]

            SQLConn.Close();
            #endregion retrieve session data

            #region enter retrieved user data into json data structure
            OMSSession session = new OMSSession();

            session.user_name.first = dbValues["userdisplayname_first"] as string;
            session.user_name.last = dbValues["userdisplayname_last"] as string;

            #region session.earned_badges
            session.earned_badges.current_badge_id = (int)dbValues["current_badge_id"];
            {
                int j = 0;
                session.earned_badges.badge_qty_0 = (int)dbValues["badge_qty_" + j++];
                session.earned_badges.badge_qty_1 = (int)dbValues["badge_qty_" + j++];
                session.earned_badges.badge_qty_2 = (int)dbValues["badge_qty_" + j++];
                session.earned_badges.badge_qty_3 = (int)dbValues["badge_qty_" + j++];
            }
            #endregion session.earned_badges

            #region session.performance
            session.performance.training_power = (int)dbValues["perf_training_power"];
            session.performance.training_level = (int)dbValues["perf_training_level"];

            session.performance.total_training_time = (int)dbValues["perf_total_training_time"];
            session.performance.avg_training_time = (int)dbValues["perf_avg_training_time"];

            session.performance.avg_accuracy = (double)dbValues["perf_avg_accuracy"];

            session.performance.highest_n = (int)dbValues["perf_highest_n"];
            session.performance.current_game_level = (int)dbValues["perf_current_game_level"];

            #endregion session.performance

            #endregion enter retrieved user data into json data structure

            string jsonstr = System.Web.Helpers.Json.Encode(new OMSSessionWrapper() { oms_session = session });

            #region log
            if (isLogEnabledForAdmin)
            {
                //** Write to log               
                formatLogData = mthdName + " Return:  \r\n";
                formatLogData += jsonstr;
                omswsLog.log(formatLogData);
            }
            #endregion log

            return jsonstr;
        }  // OMSInit_test()
        #endregion OMSInit_test() =======================================================================================

        #endregion WebMethods > dev > oms-admin =================================================================================

    }
}
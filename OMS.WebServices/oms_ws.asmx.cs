﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using CW.Tools;

namespace OMS.WebServices
{
    #region notes: build and publish - (i) login/out page, (ii) oms_ws web service
    //
    // - Reminder: to publish, you need to run VS as Administrator
    //
    // - To build either application, use Solution Explorer's context menu for each file to access features:
    //   - check Default.aspx.cs for status of (un)comment of redirect to login page
    //   - add/exclude file wrt Project
    //   - 'Set as Start Page'
    //   - Build project (OMS.WebServices) as usual via Build/Debug menus;
    //   - publish: Solution Explorer > rt-click project > Publish... > enter webapp name+version e.g oms_v04 AND website 
    //   - upload published folder to server; omit Web.config; do not disrupt default page for oms_ws
    //   - to build asp.net login/logout page:
    //     - add    : login.aspx logout.aspx
    //     - exclude: oms_ws.asmx omsws*.cs
    //     - Start Page: login.aspx
    //     - publish to: <website>/oms/
    //
    //   - to build web service oms_ws.asmx:
    //     - exclude: login.aspx logout.aspx
    //     - add    : oms_ws.asmx omsws*.cs (details cf notes in asmws_core.cs)
    //     - Start Page: oms_ws.asmx
    //     - publish to: <website>/omsws/
    //
    #endregion notes: build and publish




    /// <summary>
    /// Summary description for oms_ws
    /// </summary>
    [WebService(Namespace = "http://curriculaworks.com/OMSWebServices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public partial class oms_ws : System.Web.Services.WebService
    {

        #region WebMethods ===================================================================================================
        // this file is now the "anchor"; 
        // the WebMethods are located in separate files based on function - 
        //       omsws_{app,admin,dev}
        // WebMethod groups are (de)selected for Publishing by adding/excluding their .cs files to/from the project

        // NOTE: the project should always include oms_ws.asmx and omsws_core.cs
        
        #endregion WebMethods ===================================================================================================


    }  // class oms_ws
}

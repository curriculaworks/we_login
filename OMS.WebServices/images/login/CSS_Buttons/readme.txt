
The folders here came originally from the CSS Tutorial:

http://designshack.net/articles/css/four-simple-and-fun-css-button-hover-effects-for-beginners/


 - example-designshack  is what the browser (Chrome on Windows) produced using "Save page as..."

 - example-designshack is the same thing with all subfolders condensed into the parent folder

 - example-borders  is a copy of example-designshack meant to play and experiment with

 - example-borders-simplified is a copy of example-borders reduced down to one button so it's easier to see what is what


General idea for playing and experimenting:

- use example-borders-simplified
- open index.html in a web browser
- open style2.css in a text editor (probably need to do this in vi unless you can find a text editor for Mac that let's you open files with any extension)
- change values in style2.css however you want
- refresh the browser to see the effect of the style changes you made

- tip: think of the style sheet (i.e. the file style2.css) as a rudimentary style-configuration panel. Thinking of it this way might make it seem less like scary code and more like something familiar.
- Some things to try:
  - change the background color
  - change the width and/or height
  - change the border color
  - change the background color for "active" state (mousedown)
  - add a background color value for "hover" state (mouseover)
  - experiment with border values for idle (default), active and hover states
  - experiment with values for any of the other properties you feel like trying out
  - notice that background-image can be defined as a linear-gradient instead of an actual image
  - change background-image to be an actual image:
    - place an image file in the example-borders-simplified folder
    - change the background-image value to the file name of your image
  - try opening in dreamweaver (I'm not sure if you would open index.html or the containing folder) to see if it's easier to work on in dreamweaver or in the text-editor/browser
  - if you want to make a change to the button that you don't see a property for in style2.css, try googling it. For example, I was able to quickly find out how to disable text selection by googling (I put it in the css for the body element, near the top of the stylesheet)






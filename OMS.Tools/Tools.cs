﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CW.Tools;


/// <summary>
/// Tools and helpers used by backend of OMS (H5-gen1-2012) (specifically by web service)
/// </summary>
namespace OMS.Tools
{

    public class NamedStrings : Dictionary<string, string> { }
    public class NamedObjects : Dictionary<string, object> { }

    #region config

    /// <summary>
    /// store and manage global backend config data read in (by web service) from external file
    /// </summary>
    public class OMSConfig
    {
        // TODO: consolidate config mgmt

        // paths to config files
        protected static string PathOMSConfigBase = @"C:\cwconfig\";
        protected static string PathOMSConfigFile = PathOMSConfigBase + @"omsconfig.txt";
        protected static string PathWSConfigFile = PathOMSConfigBase + @"omswsconfig.txt";

        // config parameters
        public static string URLConfigSEL = @"";  // http://173.45.253.149/shell/select
        public static string URLMotivatorsOFF = @"";  // http://173.45.253.149/shell/instance/2
        public static string URLMotivatorsONN = @"";  // http://173.45.253.149/shell/instance/1
        public static string URLMissionOFF = @"";  // http://173.45.253.149/shell/instance/2

        public static List<string> ApplicationURLs = new List<string>();
        // assigned in GetConfig(); indexes correspond to OMSUserDescr.OMSRunMode, which comes from external userconfig
        //{
        //    URLConfigSEL,            [0]
        //    URLMotivatorsOFF,        [1]
        //    URLMotivatorsONN,        [2]
        //    URLMissionOFF            [3]
        //};

        public static string PathConfigUsers = @"";


        // usernames starting with these strings (';'-delim list) are redirected to URLMotivators{OFF,ONN,SEL}
        public static string userGrpMotivatorsOFF = "";
        public static string userGrpMotivatorsONN = "";
        public static string userGrpMissionOFF = "";
        public static string userGrpConfigSEL = "";

        public static List<string> MotvOFFUsers = new List<string>();
        public static List<string> MotvONNUsers = new List<string>();
        public static List<string> MssnOFFUsers = new List<string>();
        public static List<string> ConfigSELUsers = new List<string>();

        /// <summary>
        /// cf cwconfig/configUsers.txt
        /// </summary>
        public class OMSUserDescr
        {
            public int TID = 0;
            public string Username = "";  // login username
            public int OMSRunMode = 0;
            public int NBackTheme = 0;

        }


        #region config mgmt [TODO: cleanup structures (consolodate duplicate code etc)]

        public static void GetOMSConfig()
        {

            // TODO: check whether config updated since last read

            StreamReader srr = Tools.OpenRead(PathOMSConfigFile);  // TODO: errhandling
            if (srr == null) return;
            string dataLine = "";
            while (true)
            {
                #region prep next dataline
                dataLine = srr.ReadLine();
                if (dataLine == null) break;

                // preprocess: trim leading,trailing whitespace; omit meta-lines
                dataLine.Trim();
                if (
                    dataLine.Length < 2 ||
                    dataLine.StartsWith("#") ||
                    dataLine.StartsWith("//")
                    ) continue;
                #endregion prep next dataline


                string[] tokv = CWTools.CWSplit(record: dataLine, delims: "=", trimWhitespace: true);
                #region store token values
                // TODO: make this more robust  
                {
                    string key = tokv[0];
                    string val = tokv[1];
                    if (false) { }

                    else if (key == "urlConfigSEL") URLConfigSEL = val;
                    else if (key == "urlMotivatorsONN") URLMotivatorsONN = val;
                    else if (key == "urlMotivatorsOFF") URLMotivatorsOFF = val;
                    else if (key == "urlMissionOFF") URLMissionOFF = val;

                    else if (key == "pathConfigUsers") PathConfigUsers = PathOMSConfigBase + val;
                    
                        #region [obsolete] motivator-settings groups
                    else if (key == "userGrpMotivatorsOFF")
                    {
                        userGrpMotivatorsOFF = val;
                        string[] ttokv = CWTools.Tokenize(val);
                        for (int i = 1; i < ttokv.Length; i++)
                            MotvOFFUsers.Add(ttokv[i]);
                    }
                    else if (key == "userGrpMotivatorsONN")
                    {
                        userGrpMotivatorsONN = val;
                        string[] ttokv = CWTools.Tokenize(val);
                        for (int i = 1; i < ttokv.Length; i++)
                            MotvONNUsers.Add(ttokv[i]);
                    }
                    else if (key == "userGrpMotivatorsSEL")
                    {
                        userGrpConfigSEL = val;
                        string[] ttokv = CWTools.Tokenize(val);
                        for (int i = 1; i < ttokv.Length; i++)
                            ConfigSELUsers.Add(ttokv[i]);
                    }
                    #endregion motivator-settings groups

                    #region [commented] expanded else-if (spare)
                    else if
                    (
                            key == "urlMotivatorsOFF"
                    )
                        URLMotivatorsOFF = val;
                    #endregion
                }  // for i<tokc
                #endregion store tokens
            }  // while (pathConfig:dataline)

            srr.Close();

            ApplicationURLs = new List<string>()
            {
                URLConfigSEL,
                URLMotivatorsONN,
                URLMotivatorsOFF,
                URLMissionOFF
            };


        }  // GetConfig()

        public static List<OMSUserDescr> GetUsers(string pathConfigUsers)
        {
            List<string> dataLines = CW.Tools.FileIO.LoadDataLines(pathConfigUsers);

            List<OMSUserDescr> users = new List<OMSUserDescr>();
            foreach (string dataLine in dataLines)
            {
                OMSUserDescr user = new OMSUserDescr();
                string[] tokv = CWTools.Tokenize(record: dataLine);
                user.TID = int.Parse(tokv[1]);
                user.Username = tokv[2];
                user.OMSRunMode = int.Parse(tokv[3]);
                user.NBackTheme = int.Parse(tokv[4]);

                users.Add(user);
            }

            return users;
        }

        // TODO: if user not found, add new entry with default settings

        public static OMSUserDescr GetUserDescr(int tid, List<OMSUserDescr> users)
        {
            OMSUserDescr userToFind = null;
            foreach (OMSUserDescr user in users)
            {
                if (user.TID == tid)
                {
                    userToFind = user;
                    break;
                }
            }
            return userToFind;
        }

        public static OMSUserDescr GetUserDescr(string username, List<OMSUserDescr> users)
        {
            OMSUserDescr userToFind = null;
            foreach (OMSUserDescr user in users)
            {
                if (user.Username == username) break;
            }
            return userToFind;
        }


        public static OMSUserDescr GetUserDescr(int tid)
        {
            GetOMSConfig();
            List<OMSUserDescr> users = GetUsers(pathConfigUsers: PathConfigUsers);
            OMSUserDescr user = GetUserDescr(tid: tid, users: users);
            return user;
        }

        public static OMSUserDescr GetUserDescr(string username)
        {
            GetOMSConfig();
            List<OMSUserDescr> users = GetUsers(pathConfigUsers: PathConfigUsers);
            OMSUserDescr user = GetUserDescr(username: username, users: users);
            return user;
        }

        public static string GetApplicationURL(int tid)
        {
            GetOMSConfig();
            List<OMSUserDescr> users = GetUsers(pathConfigUsers: PathConfigUsers);
            OMSUserDescr user = GetUserDescr(tid: tid, users: users);
            string url = ApplicationURLs[user.OMSRunMode];

            return url;
        }



        public static bool IsUserInSet(string username, List<string> usersubsets)
        {
            bool isUserInGroup = false;

            foreach (string usersubset in usersubsets)
            {
                if(username.StartsWith(usersubset))
                {
                    isUserInGroup = true;
                    break;
                }
            }

            return isUserInGroup;
        }

        #endregion config mgmt
        public static void Toolsfoo()
        {
            #region openread
            {
                string filepathIn = @"C:\cwconfig\omswsconfig.txt";
                filepathIn = @"C:\cwconfig\omsconfig.txt"; 
                StreamReader sr = null;
                try
                {
                    //Pass the file path and file name to the StreamReader constructor
                    sr = new StreamReader(filepathIn);
                }
                catch (Exception ee)
                {
                    string s = ee.Message;
                    //Console.WriteLine("Exception: " + e.Message);
                }
                finally
                {
                    //Console.WriteLine("Executing finally block.");
                }
            }
            #endregion openread

        }  // Toolsfoo()

        public static bool GetIsEnabledLogLoginEvents()
        {
            return GetIsEnabledLogLoginEvents(PathOMSConfigFile);
        }
        public static bool GetIsEnabledLogLoginEvents(string pathConfig)
        {
            bool isEnabledLogLoginEvents = false;
            List<string> dataLines = CW.Tools.FileIO.LoadDataLines(pathConfig);

            List<OMSUserDescr> users = new List<OMSUserDescr>();
            foreach (string dataLine in dataLines)
            {
                string[] tokv = CWTools.CWSplit(record: dataLine, delims: "=", trimWhitespace: true);
                #region store token values
                // TODO: make this more robust  
                {
                    string key = tokv[0];
                    string val = tokv[1];
                    if (false) { }

                    else if (key == "isEnabledLogLoginEvents")
                    {
                        isEnabledLogLoginEvents = "true".StartsWith(val);
                        break;
                    }
                }  // for i<tokc
                #endregion store tokens
            }

            return isEnabledLogLoginEvents;
        }

    }

    /// <summary>
    /// store and manage global config data read in from external file
    /// </summary>
    public class NBackConfig
    {
    }

    #endregion config

    public class Tools
    {
        /// <summary>
        /// deprecated in favour of CW.Tools.FileIO.OpenRead()
        /// </summary>
        /// <param name="filepathIn"></param>
        /// <returns></returns>
        public static StreamReader OpenRead(string filepathIn)
        {
            StreamReader sr = null;
            try
            {
                //Pass the file path and file name to the StreamReader constructor
                sr = new StreamReader(filepathIn);
            }
            catch (Exception e)
            {
                //Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                //Console.WriteLine("Executing finally block.");
            }

            return sr;
        }
    }


}
